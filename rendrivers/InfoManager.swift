//
//  InfoManager.swift
//  rendrivers
//
//  Created by Yuna Daddy on 26/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import SwiftyJSON

enum MenuType {
    case NORMAL
    case DRIVER_REQUEST
    case DRIVER_START
    case DRIVER_CANCEL
}

enum PathType {
    case TARGET
    case SOURCE
    case REPAIR_SHOP
    case START
    case END
}

class InfoManager: NSObject {
    open var startPathInfo : TMapPOIItem? = nil
    open var endPathInfo : TMapPOIItem? = nil
    open var viaPathInfo : TMapPOIItem? = nil
    open var fcmToken : String? = nil
    open var apiToken : String? = nil
    open var appID : String? = nil
    
    open var reqID : String = "" //요청 ID
    open var expect_amt : String = "" //총 소요 금액
    open var dc_amt : String = "" //할인 금액
    open var mission_id = ""
    open var cost = ""
    open var cautions: JSON = JSON()
    
    private var menuType : MenuType = MenuType.NORMAL
    private let apiTokenStr  = "apiTokenStr"
    private let appIDStr     = "appIDStr"
    private let reqIDStr     = "reqIDStr"
    private let driverStatusStr = "driverStatusStr"
    
    private var driverCarStatus = "0" //탁송여정 안내 상태
    var requestInfo : RequestInfo? = nil
    open var curDriverWorkStatus = false //드라이버 출퇴근 상태
    
    /// Save the single instance
    static private var instance : InfoManager {
        return sharedInstance
    }
    
    private static let sharedInstance = InfoManager()
    
    /**
     Singleton pattern method
     
     - returns: FavoriteManager single instance
     */
    static func getInstance() -> InfoManager {
        return instance
    }
    
    private override init() {
        super.init()
        
        loadSettingsData()
    }
    
    func getPathResult(type : PathType) -> TMapPOIItem?{
        var item : TMapPOIItem
        
        if type == PathType.START{
            item = (startPathInfo ?? nil)!
        }else if type == PathType.END{
            item = (endPathInfo ?? nil)!
        }else{
            if viaPathInfo == nil{ return nil }
            item = (viaPathInfo ?? nil)!
        }
        
        return item
    }
    func setPathResult(item : TMapPOIItem?, type : PathType){
        if type == PathType.START{
            startPathInfo = item
        }else if type == PathType.END{
            endPathInfo = item
        }else{
            viaPathInfo = item
        }
    }
    
    func setMenuType(type : MenuType){ self.menuType = type }
    func getMenuType() -> MenuType{ return self.menuType }
    
    func setToken(token : String ){
        self.fcmToken = token
    }
    func getToken() -> String {
        return self.fcmToken ?? ""
    }
    
    func setReqID(reqID : String ){
        self.reqID = reqID
        storeData(reqIDStr, data: self.reqID)
    }
    
    func getReqID() -> String { return self.reqID }
    
    func setDriverStatus(status : String ){
        self.driverCarStatus = status
        storeData(driverCarStatus, data: self.driverCarStatus)
    }
    
    func getDriverStatus() -> String { return self.driverCarStatus }
    
    func setExpectAMT(fee : String ){ self.expect_amt = fee }
    func getExpectAMT() -> String { return self.expect_amt }
    
    func setDcAMT(dc : String ){ self.dc_amt = dc }
    func getDcAMT() -> String { return self.dc_amt }
    
    func setApiToken(token : String ){
        self.apiToken = token
        storeData(apiTokenStr, data: self.apiToken!)
    }
    func getApiToken() -> String { return self.apiToken! }
    
    func setAppID(id: String){
        self.appID = id
        storeData(appIDStr, data: self.appID!)
    }
    
    func getAppID() -> String{
        return self.appID!
    }
    
    func storeData(_ key:String, data:String){
        let sharedPref = UserDefaults.standard
        sharedPref.setValue(data, forKey: key)
        sharedPref.synchronize()
    }
    
    private func retrieveData(_ key:String) -> String{
        let sharedPref = UserDefaults.standard
        let result =  sharedPref.string(forKey: key)
        if result == nil { return "" }
        return result!
    }
    
    private func loadSettingsData(){
        //self.apiToken = "t5xFTLtcQqOUTBU3gZfqbdM4RFkVozya6EaEOUgTfvVBJO1TEVKLfcqQcHoJ31fn4VoHV21FgOmlDrbN"
        //self.appID   = "01087682057"
        
        self.apiToken = retrieveData(apiTokenStr)
        self.appID   = retrieveData(appIDStr)
        self.reqID  = retrieveData(reqIDStr)
        self.driverCarStatus = retrieveData(driverStatusStr)
    }
}
