//
//  PathInfoManager.swift
//  rendrivers
//
//  Created by Yuna Daddy on 01/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import Foundation


@objc protocol PathManagerDelegate {
    @objc optional func selectedAddrByMap(lat: Double, lng: Double)
    @objc optional func displayAddrByMap(lat: Double, lng: Double)
}

class PathItem{
    private var lat : Double
    private var lng : Double
    private var address : String? = ""
    
    init(lat: Double, lng: Double, address: String) {
        self.lat = lat
        self.lng = lng
        self.address = address
    }
    
    func getLatInfo() -> Double {return lat}
    func getLngInfo() -> Double {return lng}
    func getAddress() -> String {return address!}
}

class PathInfoManager: NSObject{
    private var startPathItem : PathItem = PathItem(lat: 37.244980, lng: 131.868397, address: "독도")
    private var sourcePathItem : PathItem? = nil
    private var targetPathItem : PathItem? = nil
    private var repairShopPathItem : PathItem? = nil
    
    private var southWestPathItem : PathItem? = nil
    private var northEastPathItem : PathItem? = nil
    
    private var bRepairShoHas = false
    private var bCompanyStart = false
    
    open var myLocationInfo : CLLocation? = nil
    
    open var start_car_no = ""
    open var start_car_model = ""
    open var start_car_photo = ""
    open var start_car_real_match = ""
    
    open var waypoint_car_no = ""
    open var waypoint_car_model = ""
    open var waypoint_car_photo = ""
    open var waypoint_car_real_match = ""
    
    open var start_tel_no = ""
    open var start_address = ""
    open var start_la = ""
    open var start_lo = ""
    
    open var waypoint_tel_no = ""
    open var waypoint_address = ""
    open var waypoint_la = ""
    open var waypoint_lo = ""
    
    open var end_tel_no = ""
    open var end_address = ""
    open var end_la = ""
    open var end_lo = ""
    
    
    open var cp_nm = ""
    open var emp_nm = ""
    
    open var insr_nm = ""
    open var insr_no = ""
    
    
    open var center_gb = ""
    open var add_req_car = ""
    open var action_gb = ""
    open var action_gb_by_app = ""
    
    open var add_req = ""
    open var req_gb = ""
    
    open var mission_id = ""
    
    open var limit_time = ""
    
    open var cur_mission_car_no = ""
    
    static let sharedInstance: PathInfoManager = {
        let instance = PathInfoManager()
        return instance
    }()
    
    private override init(){
        super.init()
        clearPathItem()
    }
    
    func isHasRepairShopInfo() -> Bool { return repairShopPathItem != nil }
    
    func setCompanyStart(_ companyStart: Bool){ bCompanyStart = companyStart }
    func isCompanyStart() -> Bool { return bCompanyStart }
    
    func getTargetInfo() -> PathItem? { return  targetPathItem }
    func getSourceInfo() -> PathItem? { return  sourcePathItem }
    func getRepariShopInfo() -> PathItem? { return  repairShopPathItem }
    
    func clearPathItem(){
        targetPathItem = nil
        sourcePathItem = nil
        repairShopPathItem = nil
        bRepairShoHas = false
        
        southWestPathItem = nil
        northEastPathItem = nil
    }
    
    func setPathInfo( item: PathItem!, pathType: PathType, isViaMode: Bool){
        if pathType == PathType.TARGET{
            targetPathItem = item
        }else if pathType == PathType.SOURCE{
            if isViaMode{
                repairShopPathItem = item
            }else{
               sourcePathItem = item
            }
            
        }else if pathType == PathType.REPAIR_SHOP{
            if isViaMode {
               sourcePathItem = item
            }else{
                if item == nil{
                    bRepairShoHas = false
                }else{
                    bRepairShoHas = true
                }
                
               repairShopPathItem = item
            }
        }
        
        if item != nil {
            setNorthEastPathInfo(item: item)
            setSouthWestPathInfo(item: item)
        }
    }
    
    private func setSouthWestPathInfo( item : PathItem ){
        if southWestPathItem == nil {
            southWestPathItem = item
        }else{
            let newDis = calcDistance(lat1: item.getLatInfo(), lon1: item.getLngInfo(), lat2: startPathItem.getLatInfo(), lon2: startPathItem.getLngInfo())
            let oldDis = calcDistance(lat1: southWestPathItem!.getLatInfo(), lon1: southWestPathItem!.getLngInfo(), lat2: startPathItem.getLatInfo(), lon2: startPathItem.getLngInfo())
            
            if newDis > oldDis {
                southWestPathItem = item
            }
        }
    }
    
    private func setNorthEastPathInfo( item : PathItem ){
        if northEastPathItem == nil {
            northEastPathItem = item
        }else{
            let newDis = calcDistance(lat1: item.getLatInfo(), lon1: item.getLngInfo(), lat2: startPathItem.getLatInfo(), lon2: startPathItem.getLngInfo())
            let oldDis = calcDistance(lat1: northEastPathItem!.getLatInfo(), lon1: northEastPathItem!.getLngInfo(), lat2: startPathItem.getLatInfo(), lon2: startPathItem.getLngInfo())
            
            if newDis < oldDis {
                northEastPathItem = item
            }
        }
    }
    
    private func calcDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double) -> Double {
        let theta : Double = lon1 - lon2;
        
        var dist : Double = sin(deg2rad(deg: lat1)) * sin(deg2rad(deg: lat2)) + cos(deg2rad(deg: lat1)) * cos(deg2rad(deg: lat2)) * cos(deg2rad(deg: theta));
        
        dist = acos(dist);
        dist = rad2deg(rad: dist);
        dist = dist * 60 * 1.1515;
        
        //        if (Unit == "kilometer") {
        //            dist = dist * 1.609344;
        //        } else if(unit == "meter"){
        //            dist = dist * 1609.344;
        //        }
        //
        return dist * 1.609344 //킬로미터
    }
    
    private func deg2rad(deg : Double) -> Double{
        return (deg * Double.pi / 180.0 )
    }
    
    private func rad2deg(rad : Double) -> Double{
        return (rad * 180 / Double.pi )
    }
    
    func getSouthWestPathInfo() -> PathItem? { return southWestPathItem }
    func getNorthEastPathInfo() -> PathItem? { return northEastPathItem }
    
    func getAddrFromTmapAPI(_ info : TMapPOIItem) -> String{
        var address : String = info.getPOIAddress()
        
        if info.firstNo.count != 0 {
            address += info.firstNo
        }
        
        if info.secondNo.count != 0 {
            address += info.secondNo
        }
        
        return address
    }
}
