//
//  BaseViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 26/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

@objc protocol ParentViewDelegate{
    func showPopupViewFromDelegate(_ title: String, _ content: String)
    @objc optional func showInfoPopUpView(_ idx: Int)
    @objc optional func showLoadingProgressBarFromDelegate(_ show: Bool)
    
    @objc optional func moveToPageFromDelegate(_ id: String)
    @objc optional func moveToPageFromDelegate(_ id: String, _ subInfo: String)
    @objc optional func moveToPageFromDelegate(_ id: String, _ subInfo: String, _ thirdInfo: String)
    @objc optional func moveToPagePresentFromDelegate(_ id: String, reqID: String)
    @objc optional func moveToPagePresentFromDelegate(_ id: String, title: String, urlInfo: String)
}

class BaseViewController: UIViewController, NetManagerDelegate, ParentViewDelegate{
    internal var dimView : UIView? = nil
    internal var loadingBgBView : UIView? = nil
    internal var loadingImage : UIImageView? = nil
    
    internal var titleInfo = ""
    internal var urlInfo = ""
    internal var pathInfoStr = ""
    internal var thirdInfo = ""
    
    weak var mainOperationDelegate : MainOperationDelegate? = nil
    
    func showPopupViewFromDelegate(_ title: String, _ content: String) {}
    
    func showInfoPopUpView(_ idx: Int) {}
    
    func showLoadingProgressBarFromDelegate(_ show: Bool) {
        showLoadingProgressBar(isShow: show)
    }
    
    deinit{
    }
    
    func moveToPageFromDelegate(_ id: String){
    }
    
    func moveToPageFromDelegate(_ id: String, _ subInfo: String){
    }
    
    func moveToPageFromDelegate(_ id: String, _ subInfo: String, _ thirdInfo: String){
        
    }
    
    func moveToPagePresentFromDelegate(_ id: String, title: String, urlInfo: String) {
    }
    
    func moveToPagePresentFromDelegate(_ id: String, reqID: String) {
    }
    
    override open var shouldAutorotate: Bool{
        return false
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .portrait
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        
        
        //Dim View
        dimView = UIView()
        dimView!.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height) // Set X and Y whatever you want
        dimView!.backgroundColor = UIColor.colorWithRGBHex(hex: 0x000000, alpha: 0.7)
        dimView!.isHidden = true
        
        //로딩 화면
        loadingBgBView = UIView()
        loadingBgBView!.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height) // Set X and Y whatever you want
        loadingBgBView!.backgroundColor = UIColor.colorWithRGBHex(hex: 0x000000, alpha: 0.7)
        loadingBgBView!.isHidden = true
        
        loadingImage = UIImageView()
        loadingImage!.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        loadingImage!.image = UIImage(named: "loading_01")
        loadingImage!.center = (loadingBgBView?.convert(loadingBgBView!.center, from:loadingBgBView?.superview))!
        
        let animationsFrames = [UIImage(named: "loading_01"), UIImage(named: "loading_02"),
                               UIImage(named: "loading_03"), UIImage(named: "loading_04")]
        loadingImage?.animationImages = (animationsFrames as! [UIImage])
        loadingImage?.animationDuration = 0.8
        //loadingImage?.startAnimating()
        loadingBgBView?.addSubview(loadingImage!)
        
        self.view.addSubview(dimView!)
        self.view.addSubview(loadingBgBView!)
    }
    
    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.view.endEditing(true)
    }
    
    //MARK: - NetManager delegate
    func request(_ name: String) {
        println(">>> requestID = \(name)")
    }
    
    func requestFinished(_ requestID: String, _ data: Any!) {
    }
    
    func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
    }
    
    func requestFailed(_ requestID: String) {
        println(">>> requestID = \(requestID)")
    }
    
    func sendCall(_ number: String){
        if let phoneCallURL = URL(string: "tel://\(number)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
            
        }
    }
    
    func showLoadingProgressBar(isShow: Bool){
        if isShow {
            loadingBgBView?.isHidden = false
            loadingImage?.startAnimating()
        }else{
            loadingBgBView?.isHidden = true
            loadingImage?.startAnimating()
        }
    }
}
