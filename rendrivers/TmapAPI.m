//
//  TmapAPI.m
//  rentalk
//
//  Created by Yuna Daddy on 09/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

#import "TmapAPI.h"
#import "TmapView.h"

@implementation TmapAPI

-(id)init{
    self = [super init];
    [self createMapView];
    return self;
}

-(NSArray*)getROIList:(NSString*) searchWord{
    NSLog(@"Objective-C Class");
    
    TMapPathData* path = [[[TMapPathData alloc] init] autorelease];
    
    //통합검색
    NSArray* result = [path requestFindAllPOI:searchWord];
//    int cnt = 0;
//    int idx = 0;
//    for(TMapPOIItem* poi in result)
//    {
//        if (cnt++ == 0) {
//            [_mapView setCenterPoint:[poi getPOIPoint]];
//        }
//
//        [_mapView addCustomObject:poi ID:[NSString stringWithFormat:@"poi%d", idx++]];
//        NSLog(@"poi:%@", poi);
//    }
    
    return result;
}

- (void)createMapView {
    _mapView = [[TMapView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [_mapView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [_mapView setSKTMapApiKey:TMAP_APPKEY];     // 발급 받은 apiKey 설정
    //[self.mapContainerView addSubview:_mapView];
}

@end
