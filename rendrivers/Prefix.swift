//
//  Prefix.swift
//  rendrivers
//
//  Created by Yuna Daddy on 26/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit


public var USE_INDEX: Bool = false                  // true - index / false - main

public let API_SERVER: String                               = "https://api.rentalk.co.kr/api/v1"
//public let API_SERVER: String                               = "http://52.79.48.82:8000/api/v1"

//public let API_SERVER: String                               = "http://tapp.minetalk.co.kr"
//public let URL_WEB_SHOP: String                             = "http://tshop.minetalk.co.kr/m/?shop_auth=%@"          //쇼핑몰 도메인

public let API_APP_VERSION                                  = "/api/app/version"            // 버전체크

public let API_USER_REGIST_REQ_NUM                          = "/auth/driver/join/auth_no"  // 회원가입 인증요청
public let API_USER_LOGIN_REQ_NUM                           = "/auth/driver/login/auth_no" // 로그인 인증요청

public let API_USER_REGIST                                  = "/auth/driver/join"  // 회원가입
public let API_USER_LOGIN                                   = "/auth/driver/login" // 로그인 인증요청

public let API_WORK_STATUS                                 = "/driver/working-status" // 드라이버 출/퇴근 상태
public let API_DRIVER_WAIT_INFO_REQUEST                       = "/driver" // 드라이버 대기 상태값 조회

public let API_DRIVER_REQUEST                               = "/driver/driver-request" // 탁송 요청건 조회
public let API_DRIVER_DECIDE_REQUEST                        = "/driver/driver-decide-request" // 탁송 요청건 수락/거절


public let API_DRIVER_INFO_REQUEST                          = "/employee/driver-request/driver" // 탁송 요청

public let API_DRIVER_INSURANCE_INFO_REQUEST                  = "http://test.simg.biz:3000/api/driver_underwriting" // 보험사 정보 요청


public let API_DRIVER_LOCATION_UPLOAD                   = "/driver/location" // 일반 상태 드라이버 위치 업로드
public let API_DRIVER_MISSION_LOCATION_UPLOAD             = "/driver/location/riding" // 운행 중 드라이버 위치 업로드
public let API_APP_STATUS_REQUEST                        = "/driver/app-status"    // 앱 상태 요청
public let API_CAR_AUTO_UP_REQUEST                   = "/driver/car/autoup" //차량 조회 API

public let API_DRIVER_MISSION_COMPLETE_REQUEST       = "/driver/driver-request/action/mission" //미션 수행



public let API_SLACK_CUSTOMER_REQUEST                       = "https://hooks.slack.com/services/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"

public let API_DRIVER_RESULT_REQUEST                        = "/driver/driver-request/list"  // 탁송 정보 요청

public let NOTI_MOVE_MENU_CONTROLLER: String                 = "noti_move_menu_controller"   //메뉴 페이지 이동

public let API_DRIVER_REQUEST_ACTION_REQUEST                = "/driver/driver-request/action" // 탁송 요청
public let API_DRIVER_ARRIVED_START_REQUEST                 = "/driver/driver-request/start" // 드라이버 픽업지 도착

public let API_DRIVER_ARRIVED_ACTION_START_REQUEST          = "/driver/driver-request/action/start" // 드라이버 픽업지 도착 정보 업로드

public let API_DRIVER_ARRIVED_ACTION_WAYPOINT_REQUEST          = "/driver/driver-request/action/waypoint-turnover" // 드라이버 경유지 도착 정보 업로드

public let API_DRIVER_TAKEOVER_ACTION_WAYPOINT_REQUEST          = "/driver/driver-request/action/waypoint-takeover" // 드라이버 경유지 출발(제2차량) 정보 업로드

public let API_DRIVER_ARRIVED_ACTION_END_REQUEST            = "/driver/driver-request/action/end" // 드라이버 도착 정보 업로드

public let API_DRIVER_MISSION_END_REQUEST            = "/driver/driver-request/end" // 탁송 종료




// MAKR:- Preference Value
public let PREF_LOGIN_TYPE: String                          = "pref_login_type"
public let PREF_REGIST_TYPE: String                         = "pref_regist_type"
public let PREF_USER_EMAIL: String                          = "pref_user_email"
public let PREF_USER_PHONE: String                          = "pref_user_phone"
public let PREF_USER_NATION_CODE: String                    = "pref_user_nation_code"
public let PREF_USER_PWD: String                            = "pref_user_pwd"
public let PREF_USER_NAME: String                           = "pref_user_name"
public let PREF_USER_PROVIDER_TYPE: String                  = "pref_user_provider_type"
public let PREF_USER_PROVIDER_ID: String                    = "pref_user_provider_id"
public let PREF_USER_PROFILE_IMAGE: String                  = "pref_user_profile_image"
public let PREF_AUTHORIZATION: String                       = "pref_authorization"
public let PREF_TOKEN_KEY: String                           = "pref_token_key"                              //deveice id
public let PREF_LANG: String                                = "pref_lang"
public let PREF_PUSH_TOKEN: String                          = "pref_push_token"
public let PREF_MY_LANG: String                             = "pref_my_lang"                                //내언어 설정
public let PREF_TRAN_LANG: String                           = "pref_tran_lang"                              //번역언어 설정
public let PREF_SET_TRAN_LANG: String                       = "pref_set_tran_lang"                          //번역언어 설정 Y/N
public let PREF_SET_CHAT_BACKGROUND_COLOR: String           = "pref_set_chat_background_color"              //채팅방내 배경색상 Y/N
public let PREF_CHAT_BACKGROUND_COLOR: String               = "pref_chat_background_color"                  //채팅방내 배경색상 값
public let PREF_CHAT_ROOM_BACKGROUND: String                = "pref_chat_room_background"                   //채팅방내 배경이미지
public let PREF_CHAT_NAMES: String                          = "pref_chat_name"                              //채팅방 리스트 이름
public let PREF_SHOP_AUTHORIZATION: String                  = "pref_shop_authorization"                     //쇼핑몰 auth
public let PREF_PERMISSION: String                          = "pref_permission"

public let PREF_VOICE_TRAN_LANGCODE: String                 = "pref_voice_tran_langCode"
public let PREF_VOICE_MY_LANGCODE: String                   = "pref_voice_my_langCode"
public let PREF_CAMERA_TRAN_LANGCODE: String                = "pref_camera_tran_langCode"
public let PREF_CAMERA_MY_LANGCODE: String                  = "pref_camera_my_langCode"
public let PREF_TRANSFER_CAMERA: String                     = "pref_transfer_camera"        //이미지 번역
public let PREF_CONTACTS_NAMES: String                      = "pref_contacts_names"         //연락처
public let PREF_FAVORITE_FRIEND: String                     = "pref_favorite_friend"        //즐겨찾기 친구
public let PREF_NOSHOW_TODAY: String                        = "pref_noshow_today"           //공지사항 오늘하루보지않기

public let NOTI_REFRESH_FRIEND_LIST: String                 = "noti_refresh_freind_list"    //친구리스트 갱신
public let NOTI_REFRESH_CHAT_LIST: String                   = "noti_refresh_chat_list"      //채팅리스트 갱신
public let NOTI_REFRESH_MYPAGE: String                      = "noti_refresh_mypage"         //마이페이지 갱신

public let NOTI_REFRESH_MENU_STATUS: String                 = "noti_refresh_menu_status"     //드라이버 상태 갱신

public let NOTI_DRIVER_REQUEST: String                    = "noti_driver_request"       //드라이버 탁송 요청 알림
public let NOTI_DRIVER_REQUEST_START: String              = "noti_driver_request_start"  //드라이버 탁송 시작 알림
public let NOTI_DRIVER_REQUEST_CANCEL: String             = "noti_driver_request_end"    //드라이버 탁송 취소 알림


public let APPLE_APP_ID = "1352912092"
public let CS_NUMBER = "16446033"

// MARK: - Devices
public func isIOS11() -> Bool{
    return CGFloat((UIDevice.current.systemVersion as NSString).floatValue) >= 11.0
}

public func isIOS8() -> Bool{
    return CGFloat((UIDevice.current.systemVersion as NSString).floatValue) >= 8.0
}

public func isIOS7() -> Bool{
    return CGFloat((UIDevice.current.systemVersion as NSString).floatValue) >= 7.0
}

public func isiPhone4() -> Bool{
    return UIScreen.main.bounds.size.height == 480.0
}

public func isiPhone5() -> Bool{
    return UIScreen.main.bounds.size.height == 568.0
}

public func isiPhone8() -> Bool{
    return UIScreen.main.bounds.size.height == 812.0
}

public func isiPhoneX() -> Bool{
    return UIScreen.main.bounds.size.height == 812.0
}

public func isiPhoneXr() -> Bool{
    return UIScreen.main.bounds.size.height == 896.0
}

public func isiPhoneXs() -> Bool{
    return UIScreen.main.bounds.size.height == 896.0
}

public func isiPhoneXsMax() -> Bool{
    return UIScreen.main.bounds.size.height == 896.0
}

public func isiPhoneXseries() -> Bool{
    if isiPhoneX() || isiPhoneXr() || isiPhoneXs() || isiPhoneXsMax() {
        return true
    }
    return false
}

public func getDeviceWidth() -> CGFloat{
    return UIScreen.main.bounds.size.width
}

public func getDeviceHeight() -> CGFloat{
    return UIScreen.main.bounds.size.height
}

public func getTargetWidth() -> CGFloat{
    if(isiPhone4()){
        return 1080
    }
    else{
        return 1080
    }
}

public func getTargetHeight() -> CGFloat{
    if(isiPhone4()){
        return 1620
    }
    else if (isiPhoneXseries()){
        return 2337
    }
    else{
        return 1920
    }
}

public func getDiffHeight() -> CGFloat{
    return 280
}

public func getWidthRatio() -> CGFloat{
    return getDeviceWidth() / getTargetWidth()
}

public func getHeightRatio() -> CGFloat{
    return getDeviceHeight() / getTargetHeight()
}

public func CGRectReMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
    return CGRect( x: (x) * getWidthRatio() , y: (y) * getHeightRatio(), width: (width) * getWidthRatio(), height: (height) * getHeightRatio())
}

public func CGPointReMake(_ x: CGFloat, _ y: CGFloat) -> CGPoint{
    return CGPoint( x: (x) * getWidthRatio() , y: (y) * getHeightRatio())
}

public func CGSizeReMake(_ width: CGFloat, _ height: CGFloat) -> CGSize{
    return CGSize( width: (width) * getWidthRatio() , height: (height) * getHeightRatio())
}

public func UIEdgeInsetsReMake(_ t: CGFloat, _ l: CGFloat, _ b: CGFloat, _ r:CGFloat) -> UIEdgeInsets{
    return UIEdgeInsets( top: (t) * getWidthRatio() , left: (l) * getHeightRatio(), bottom: (b) * getWidthRatio(), right: (r) * getHeightRatio())
}

public func getNavHeight() -> CGFloat{
    return getStatusHeight() + 143         // top is 60
}

public func getStatusHeight() -> CGFloat{
    return UIApplication.shared.statusBarFrame.size.height / getHeightRatio()
}

public func getPagerTabHeight() -> CGFloat {
    return 140
}

public func getRatioReverse(_ size: CGSize) -> CGRect {
    return CGRectReMake(0, 0, size.width / getWidthRatio(), size.height / getHeightRatio())
}

//iPhone X safeArea bottom height
public func getSafeAreaBottom() -> CGFloat {
    if #available(iOS 11.0, *) {
        return CGFloat((UIApplication.shared.keyWindow?.safeAreaInsets.bottom)!) / getHeightRatio()
    } else {
        // Fallback on earlier versions
        return 0
    }
}

public func getTargetWebViewRect() -> CGRect {
    return CGRectReMake(0, getStatusHeight(), getTargetWidth(), getTargetHeight() - getStatusHeight())
}

public func getTargetWebViewRectWithTitle() -> CGRect {
    return CGRectReMake(0, getNavHeight(), getTargetWidth(), getTargetHeight() - getNavHeight() - getSafeAreaBottom())
}

public func getNumber(_ size : CGFloat) -> CGFloat{
    return (size) * getWidthRatio()
}

public func getSafeAreaInsets() -> UIEdgeInsets {
    if isiPhoneXseries() {
        if #available(iOS 11.0, *) {
            return (UIApplication.shared.keyWindow?.safeAreaInsets)!
        }
    }
    return .zero
}

public func getContentsHeight(_ innerY: CGFloat) -> CGFloat {
    return getTargetHeight() - innerY - getSafeAreaBottom()
}

// MARK: - Font
public func getMediumFont(_ size: CGFloat) -> UIFont{
    return UIFont(name: "AppleSDGothicNeo-Medium", size: getNumber(size))!
}

public func getRegularFont(_ size: CGFloat) -> UIFont{
    return UIFont(name: "AppleSDGothicNeo-Regular", size: getNumber(size))!
}

public func getLightFont(_ size: CGFloat) -> UIFont{
    return UIFont(name: "AppleSDGothicNeo-Light", size: getNumber(size))!
}

public func getSemiBoldFont(_ size: CGFloat) -> UIFont{
    return UIFont(name: "AppleSDGothicNeo-SemiBold", size: getNumber(size))!
}

public func getBoldFont(_ size: CGFloat) -> UIFont{
    return UIFont(name: "AppleSDGothicNeo-Bold", size: getNumber(size))!
}

public func getHeavyFont(_ size: CGFloat) -> UIFont{
    return UIFont(name: "AppleSDGothicNeoH00", size: getNumber(size))!
}

// MARK: - Color
public func UIColorFromRGBA(_ netHex:Int, alpha:CGFloat) -> UIColor
{
    let red: Int = (netHex >> 16) & 0xff
    let green: Int = (netHex >> 8) & 0xff
    let blue: Int = netHex & 0xff
    
    assert(red >= 0 && red <= 255, "Invalid red component")
    assert(green >= 0 && green <= 255, "Invalid green component")
    assert(blue >= 0 && blue <= 255, "Invalid blue component")
    
    return UIColor(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
}

public func UIColorFromRGBA(_ red: Int, _ green: Int, _ blue: Int, _ alpha: CGFloat) -> UIColor
{
    return UIColor(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
}

// MARK: - UI
func getView(_ rect: CGRect = UIScreen.main.bounds, _ color: UIColor = .clear) -> UIView {
    let view = UIView()
    view.backgroundColor = color
    view.frame = rect
    return view
}

func getScrollView(_ rect: CGRect = UIScreen.main.bounds, _ backgroundColor: UIColor = .clear) -> UIScrollView {
    let scrollView = UIScrollView(frame: rect)
    scrollView.backgroundColor = backgroundColor
    scrollView.isPagingEnabled = false
    scrollView.showsHorizontalScrollIndicator = false
    scrollView.showsVerticalScrollIndicator = false
    scrollView.bounces = false
    scrollView.contentInset = .zero
    return scrollView
}

func getTableView(_ rect: CGRect, _ style: UITableView.Style) -> UITableView {
    let tableView = UITableView(frame: rect, style: style)
    tableView.decelerationRate = .fast
    tableView.showsVerticalScrollIndicator = true
    tableView.showsHorizontalScrollIndicator = false
    tableView.separatorColor = UIColor.clear
    tableView.separatorStyle = .none
    tableView.scrollsToTop = true
    tableView.bounces = true
    tableView.contentInset = .zero
    tableView.estimatedRowHeight = 0
    tableView.estimatedSectionFooterHeight = 0
    tableView.estimatedSectionHeaderHeight = 0
    return tableView
}

func getLabel(_ title: String, _ rect: CGRect, _ font: UIFont, _ align: NSTextAlignment, _ color: UIColor) -> UILabel {
    let label: UILabel = UILabel()
    label.frame = rect
    label.backgroundColor = UIColor.clear
    label.textAlignment = align
    label.textColor = color
    label.font = font
    label.text = title
    label.numberOfLines = 2
    label.lineBreakMode = .byTruncatingTail
    
    return label
}

func getImageView(_ name: String, _ rect: CGRect) -> UIImageView {
    let imageView = UIImageView()
    imageView.backgroundColor = UIColor.clear
    imageView.frame = rect
    if name != "" {
        imageView.image = UIImage(named: name)
    }
    return imageView
}

func getButton(_ rect: CGRect, _ tag: Int) -> UIButton {
    let button = UIButton(type: .custom)
    button.backgroundColor = UIColor.clear
    button.frame = rect
    button.tag = tag
    button.isExclusiveTouch = true
    return button
}

func getSwitchButton(_ rect: CGRect, _ tag: Int) -> UISwitch {
    let switchButton = UISwitch(frame:rect)
    switchButton.isOn = true
    switchButton.setOn(true, animated: true);
    switchButton.tag = tag
    return switchButton
}

func getImageButton(_ rect: CGRect, _ tag: Int, _ normalImage: String, _ selectImage: String) -> UIButton {
    let button = UIButton(type: .custom)
    button.backgroundColor = UIColor.clear
    button.frame = rect
    button.tag = tag
    button.imageEdgeInsets = UIEdgeInsetsReMake(30, 30, 30, 30)
    button.setImage(UIImage(named: normalImage), for: .normal)
    button.setImage(UIImage(named: selectImage), for: .selected)
    return button
}

func getTextView(_ rect: CGRect, _ font: UIFont, _ align: NSTextAlignment, _ color: UIColor) -> UITextView {
    let textView = UITextView(frame: rect)
    textView.backgroundColor = UIColor.clear
    textView.textAlignment = NSTextAlignment.left
    textView.font = font
    textView.textColor = color
    return textView
}

func getTextView(_ rect: CGRect, _ font: UIFont, _ color: UIColor) -> UITextView {
    let textView = UITextView(frame: rect)
    textView.textColor = color
    textView.font = font
    textView.isScrollEnabled = true
    textView.isUserInteractionEnabled = true
    textView.isEditable = true
    textView.contentInset = .zero
    return textView
}

//func getDottedLineView(_ rect: CGRect, _ lineColor: UIColor, _ lineWidth: CGFloat) -> UIView {
//    let view = DottedLineView()
//    view.frame = rect
//    view.lineColor = lineColor
//    view.lineWidth = lineWidth
//    return view
//}

// MARK: - Preference
func getPreferenceString(_ preference: String) -> String
{
    let sharedUserDefaults: UserDefaults = UserDefaults.standard
    if let preferen = sharedUserDefaults.value(forKey: preference) as? String{
        return preferen
    }
    return ""
}

func getPreferenceBool(_ preference: String) -> Bool
{
    if let preferen = UserDefaults.standard.value(forKey: preference) as? Bool{
        return preferen
    }
    return false
}

func getPreferenceDouble(_ preference: String) -> Double {
    
    if let preferen = UserDefaults.standard.value(forKey: preference) as? Double{
        return preferen
    }
    return 0.0
}

func getPreferenceFloat(_ preference: String) -> Float {
    
    if let preferen = UserDefaults.standard.value(forKey: preference) as? Float{
        return preferen
    }
    return 0.0
}

func getPreferenceInt(_ preference: String) -> Int {
    
    if let preferen = UserDefaults.standard.value(forKey: preference) as? Int{
        return preferen
    }
    return 0
}

// MARK: - Log
public func println<T>(_ object: T, _ file: String = #file, _ function: String = #function, _ line: Int = #line){
    #if DEBUG
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss:SSS"
    let process = ProcessInfo.processInfo
    
    var tid:UInt64 = 0;
    pthread_threadid_np(nil, &tid);
    let threadId = tid
    
    Swift.print("\(dateFormatter.string(from: NSDate() as Date)) \(process.processName))[\(process.processIdentifier):\(threadId)] \(file.lastPathComponent)(\(line)) \(function):\t\(object)")
    
    //        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    //        let path = documentsPath.stringByAppendingPathComponent("dump.txt")
    //        var dump = ""
    //        if FileManager.default.fileExists(atPath: path) {
    //            dump =  try! String(contentsOfFile: path, encoding: String.Encoding.utf8)
    //        }
    //        do {
    //
    //            dump += "\n\(dateFormatter.string(from: NSDate() as Date)) \(process.processName))[\(process.processIdentifier):\(threadId)] \(file.lastPathComponent)(\(line)) \(function):\t\(object)"
    //            // Write to the file
    //            try  "\(dump)".write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
    //
    //        } catch let error as NSError {
    //            print("Failed writing to log file: \(path), Error: " + error.localizedDescription)
    //        }
    
    #else
    
    #endif
}

public func printlnLog<T>(_ type: Int, _ object: T, _ file: String = #file, _ function: String = #function, _ line: Int = #line){
    #if DEBUG
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss:SSS"
    let process = ProcessInfo.processInfo
    
    var tid:UInt64 = 0;
    pthread_threadid_np(nil, &tid);
    let threadId = tid
    
    switch type {
    case 0:
        Swift.print("\(dateFormatter.string(from: NSDate() as Date)) \(process.processName))[\(process.processIdentifier):\(threadId)] \(file.lastPathComponent)(\(line)) \(function):\t \n=======================paramters=======================\n\(object)\n=======================paramters=======================")
        
        break
    case 1:
        Swift.print("\(dateFormatter.string(from: NSDate() as Date)) \(process.processName))[\(process.processIdentifier):\(threadId)] \(file.lastPathComponent)(\(line)) \(function):\t \n=======================response json=======================\n\(object)\n=======================response json=======================")
        break
    default:
        break
    }
    #else
    
    #endif
}

