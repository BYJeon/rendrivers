//
//  CommonUI.swift
//  rendrivers
//
//  Created by Yuna Daddy on 20/09/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit


class CommonUI {
    class func makeLine(_ rect: CGRect) -> UIView {
        return getView(rect, UIColorFromRGBA(0xc9cfce, alpha: 1))
    }
}
