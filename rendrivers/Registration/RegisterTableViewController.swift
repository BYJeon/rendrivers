//
//  RegisterTableViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 26/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CryptoSwift


class RegisterTableViewController: BaseTableViewController {
    //이미지 등록
    var imagePicker = UIImagePickerController()
    
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var secureTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var numPreTextField: UITextField!
    @IBOutlet var numTailTextField: UITextField!
    
    @IBOutlet var liceseYearTextField: UITextField!
    @IBOutlet var licenseMonthTextField: UITextField!
    @IBOutlet var licenseDayTextField: UITextField!
    
    //운전면허 번호 앞자리
    @IBOutlet var licenseRegionTextField: UITextField!
    //운전면허 번호
    @IBOutlet var licenseNumberTextField: UITextField!
    //운전면허 종류
    @IBOutlet var licenseTypeTextField: UITextField!
    
    @IBOutlet var licenseCatagoryGuideImg: UIImageView!
    @IBOutlet var licenseNumGuideImg: UIImageView!
    
    @IBOutlet var licenseCategoryBtn: UIButton!
    @IBOutlet var licenseNumberBtn: UIButton!
    
    @IBOutlet var stickGearCheckBtn: CheckBox!
    
    //운전면허증 사진
    @IBOutlet var licenseBtn: UIButton!
    @IBOutlet var profileBtn: UIButton!
    @IBOutlet var authorityBtn: UIButton!
    
    private var isLicenseImgDone = false
    private var isProfileImgDone = false
    private var isAuthoImgDone = false
    
    private var popupType = 0
    private var selImgType = 0 // 0 : 면허증, 1 : 프로필. 2 : 인증
    
    //timer
    private var mTimer : Timer?
    private var remainTime = 600
    @IBOutlet var secureRemainTimeLabel: UILabel!
    
    //이용약관
    @IBOutlet var serviceAgreeLabel: UILabel!
    @IBOutlet var privacyAgreeLabel: UILabel!
    @IBOutlet var identityAgreeLabel: UILabel!
    @IBOutlet var insuaranceAgreeLabel: UILabel!
    @IBOutlet var positionAgreeLabel: UILabel!
    
    @IBOutlet var serviceAgreeCheck: CheckBox!
    @IBOutlet var privacyAgreeCheck: CheckBox!
    @IBOutlet var identityAgreeCheck: CheckBox!
    @IBOutlet var insuranceAgreeCheck: CheckBox!
    @IBOutlet var positionAgreeCheck: CheckBox!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        licenseBtn.tag = 0
        profileBtn.tag = 1
        authorityBtn.tag = 2
        
        licenseBtn.addTarget(self, action:#selector(openCameraDidClicked) , for: .touchUpInside)
        profileBtn.addTarget(self, action:#selector(openCameraDidClicked) , for: .touchUpInside)
        authorityBtn.addTarget(self, action:#selector(openCameraDidClicked) , for: .touchUpInside)
        
        licenseCategoryBtn.setImage(UIImage(named: "ico_phone_guide_p"), for: .selected)
        licenseNumberBtn.setImage(UIImage(named: "ico_phone_guide_p"), for: .selected)
        
        let guideGesture = UITapGestureRecognizer(target: self, action: #selector(self.categoryGuideImgDidClicked(_:)))
        licenseCatagoryGuideImg.isUserInteractionEnabled = true
        licenseCatagoryGuideImg.addGestureRecognizer(guideGesture)
        
        let guideNumGesture = UITapGestureRecognizer(target: self, action: #selector(self.numGuideImgDidClicked(_:)))
        licenseNumGuideImg.isUserInteractionEnabled = true
        licenseNumGuideImg.addGestureRecognizer(guideNumGesture)
        
        licenseNumberTextField.delegate = self
        phoneTextField.keyboardType = .numberPad
        secureTextField.keyboardType = .numberPad
        numPreTextField.keyboardType = .numberPad
        numTailTextField.keyboardType = .numberPad

        licenseNumberTextField.keyboardType = .numberPad
        liceseYearTextField.keyboardType = .numberPad
        licenseMonthTextField.keyboardType = .numberPad
        licenseDayTextField.keyboardType = .numberPad

        
        licenseRegionTextField.text = "11"
        licenseTypeTextField.text = "1종 보통"
        
        
        let serviceGesture = UITapGestureRecognizer(target: self, action: #selector(self.serviceAgrddDidClicked(_:)))
        serviceAgreeLabel.isUserInteractionEnabled = true
        serviceAgreeLabel.addGestureRecognizer(serviceGesture)
        
        let privacyGesture = UITapGestureRecognizer(target: self, action: #selector(self.privacyAgrddDidClicked(_:)))
        privacyAgreeLabel.isUserInteractionEnabled = true
        privacyAgreeLabel.addGestureRecognizer(privacyGesture)
        
        let identityGesture = UITapGestureRecognizer(target: self, action: #selector(self.identityAgrddDidClicked(_:)))
        identityAgreeLabel.isUserInteractionEnabled = true
        identityAgreeLabel.addGestureRecognizer(identityGesture)
        
        let insuranceGesture = UITapGestureRecognizer(target: self, action: #selector(self.insuaranceAgrddDidClicked(_:)))
        insuaranceAgreeLabel.isUserInteractionEnabled = true
        insuaranceAgreeLabel.addGestureRecognizer(insuranceGesture)
        
        let positionGesture = UITapGestureRecognizer(target: self, action: #selector(self.positionAgrddDidClicked(_:)))
        positionAgreeLabel.isUserInteractionEnabled = true
        positionAgreeLabel.addGestureRecognizer(positionGesture)
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 9
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
    
    @objc func categoryGuideImgDidClicked(_ sender: UIGestureRecognizer){
        self.view.endEditing(true)
       licenseCatagoryGuideImg.isHidden = true
       licenseCategoryBtn.isSelected = false
    }
    
    @objc func numGuideImgDidClicked(_ sender: UIGestureRecognizer){
        self.view.endEditing(true)
        licenseNumGuideImg.isHidden = true
        licenseNumberBtn.isSelected = false
    }
    
 
    //이용약관
    @objc func serviceAgrddDidClicked(_ sender: UIGestureRecognizer){
        openUrl(url: "https://www.rentalk.co.kr/storage/rendriver_clause.html")
    }
    
    @objc func privacyAgrddDidClicked(_ sender: UIGestureRecognizer){
        openUrl(url: "https://www.rentalk.co.kr/storage/personal_data.html")
    }
    
    @objc func identityAgrddDidClicked(_ sender: UIGestureRecognizer){
        openUrl(url: "https://www.notion.so/avara/016f76e8acca446599b4efc1e7293037")
    }
    
    @objc func insuaranceAgrddDidClicked(_ sender: UIGestureRecognizer){
        openUrl(url: "https://www.notion.so/avara/5803b414ae3f424b88be79c1f2151b41")
    }
    
    @objc func positionAgrddDidClicked(_ sender: UIGestureRecognizer){
        openUrl(url: "https://www.rentalk.co.kr/storage/location_service_clause.html")
    }
    
    private func openUrl(url: String){
        guard let url = URL(string: url), UIApplication.shared.canOpenURL(url) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    //타이머가 호출하는 콜백함수
    @objc func timerCallback(){
        if remainTime > 0 {
            remainTime -= 1
            secureRemainTimeLabel.text = String(format: "%02d:%02d", remainTime/60, remainTime%60)
        }else{
            secureRemainTimeLabel.isHidden = true
            mTimer?.invalidate()
        }
        
    }
    
    //운전면허 종류
    @IBAction func licenseTypeDidClicked(_ sender: Any) {
        self.view.endEditing(true)
        popupType = 0
        if parentDelegate != nil {
            parentDelegate?.showInfoPopUpView?(popupType)
        }
    }
    
    //운전면허 지역
    @IBAction func licenseRegionDidClicked(_ sender: Any) {
        self.view.endEditing(true)
        popupType = 1
        if parentDelegate != nil {
            parentDelegate?.showInfoPopUpView?(popupType)
        }
    }
    //인증번호 요청
    @IBAction func reqSecureDidClicked(_ sender: Any) {
        self.view.endEditing(true)
        var isCheckResult: Bool = false
        
        let phone : String = phoneTextField.trimmingCharacters()
        if phone != "" {
            isCheckResult = true
        }
        
        if parentDelegate != nil && !isCheckResult{
            parentDelegate?.showPopupViewFromDelegate("회원가입", "핸드폰 번호를 입력해 주세요.")
            return
        }
        
        request(API_USER_REGIST_REQ_NUM)
        
        remainTime = 600
        secureRemainTimeLabel.isHidden = false
        if let timer = mTimer {
            //timer 객체가 nil 이 아닌경우에는 invalid 상태에만 시작한다
            if !timer.isValid {
                /** 1초마다 timerCallback함수를 호출하는 타이머 */
                mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
            }
        }else{
            //timer 객체가 nil 인 경우에 객체를 생성하고 타이머를 시작한다
            /** 1초마다 timerCallback함수를 호출하는 타이머 */
            mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
        }
    }
    
    @IBAction func registDidClicked(_ sender: Any) {
        self.view.endEditing(true)
        var isCheckName: Bool = false
        var isCheckPhone: Bool = false
        var isCheckSecureNum: Bool = false
        var isCheckEmail: Bool = false
        var isCheckLicenseNum: Bool = false
        var isCheckYear: Bool = false
        var isCheckAuthNum: Bool = false
        
        let name : String = nameTextField.trimmingCharacters()
        if name != "" {
            isCheckName = true
        }
        
        let phone : String = phoneTextField.trimmingCharacters()
        if phone != "" {
            isCheckPhone = true
        }
        
        let secure : String = secureTextField.trimmingCharacters()
        if secure != "" {
            isCheckSecureNum = true
        }
        
        let email : String = emailTextField.trimmingCharacters()
        if email != "" {
            isCheckEmail = true
        }
        
        let licenseNum : String = secureTextField.trimmingCharacters()
        if licenseNum != "" && licenseNum.count != 12 {
            isCheckLicenseNum = true
        }
        
        isCheckLicenseNum = true
        
        let licenseYear : String = liceseYearTextField.trimmingCharacters()
        let licenseMonth : String = licenseMonthTextField.trimmingCharacters()
        let licenseDay : String = licenseDayTextField.trimmingCharacters()

        
        if licenseYear != "" && licenseMonth != ""  && licenseDay != ""{
            isCheckYear = true
        }
        
        let userNumPre : String = numPreTextField.trimmingCharacters()
        let userNumTail : String = numTailTextField.trimmingCharacters()
        if userNumPre != "" && userNumTail != ""{
            isCheckAuthNum = true
        }
        
        if !isProfileImgDone || !isLicenseImgDone || !isAuthoImgDone ||
           !isCheckName || !isCheckPhone || !isCheckSecureNum || !isCheckEmail ||
           !isCheckLicenseNum || !isCheckYear || !isCheckAuthNum {
            
            if parentDelegate != nil {
                parentDelegate?.showPopupViewFromDelegate("회원가입", "모든 정보를 입력해 주세요.")
            }
            
            return
        }
        
        if !serviceAgreeCheck.isChecked || !privacyAgreeCheck.isChecked ||
            !identityAgreeCheck.isChecked || !insuranceAgreeCheck.isChecked || !positionAgreeCheck.isChecked {
            
            if parentDelegate != nil {
                parentDelegate?.showPopupViewFromDelegate("회원가입", "모든 약관에 동의해 주세요.")
            }
            
            return
        }
        
        
        secureRemainTimeLabel.isHidden = true
        mTimer?.invalidate()
        
        if parentDelegate != nil {
            parentDelegate?.showLoadingProgressBarFromDelegate?(true)
        }
        //디버그 모드 일때는 바로 회원 가입 요청
        #if DEBUG
           request(API_USER_REGIST)
        #else
            //릴리즈 모드 일떄는 보험사 확인 요청
            request(API_DRIVER_INSURANCE_INFO_REQUEST)
        #endif
    }
    
    //운전면허 종류 선택 가이드
    @IBAction func licenseCategoryGuideDidClicked(_ sender: Any) {
        licenseCategoryBtn.isSelected = !licenseCategoryBtn.isSelected
        licenseCatagoryGuideImg.isHidden = !licenseCategoryBtn.isSelected
    }
    
    //운전면허 번호 선택 가이드
    @IBAction func licenseNumGuideDidClicked(_ sender: Any) {
        licenseNumberBtn.isSelected = !licenseNumberBtn.isSelected
        licenseNumGuideImg.isHidden = !licenseNumberBtn.isSelected
    }
    
    //사진 등록 이벤트
    @objc open  func openCameraDidClicked(_ sender: UIButton!) {
        selImgType = sender.tag
        self.openCamera()
    }
    
    // MARK:- camera picker
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: {
                action in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - network
    override func request(_ name: String) {
        if parentDelegate != nil {
            //parentDelegate?.showLoadingProgressBarFromDelegate?(true)
        }
        
        switch name {
        case API_DRIVER_INSURANCE_INFO_REQUEST:
            let API_KEY : String = "B440B73D-3022-4C52-BDA9-AB07B1F6C9D7"
            let API_URL : String = "http://m.simg.biz/api/driver_underwriting"
//            #if DEBUG
//                API_KEY = "8D491C88-5EF4-4AC8-B17B-BFE75E765699"
//                API_URL  = "http://test.simg.biz:3000/api/driver_underwriting"
//            #endif
            
            let nm : String = nameTextField.trimmingCharacters()
            let cell = phoneTextField.trimmingCharacters()
            let cellEnc = try! cell.aesEncrypt()
            let socialNum = numPreTextField.trimmingCharacters()+numTailTextField.trimmingCharacters()
            let socialNumber = try! socialNum.aesEncrypt()
            
            let params: Parameters = [
                "apiKey" : API_KEY,
                "request_data" : [[
                    "company_number" : "7287600248"
                    ,"biz_driver_id" :  cell
                    ,"cell" : cellEnc
                    ,"cell2"  : ""
                    ,"name" : nm
                    ,"social_number" : socialNumber
                    ,"license_type" : ""
                    ,"license_number" : ""
                    ,"email" : ""
                    ,"memo" : ""]]
            ]
            _ = NetworkManager.sharedInstance.request(API_URL, name, params, HTTPMethod.post, target: self)
            break
            
        case API_USER_REGIST:
            let nm : String = nameTextField.trimmingCharacters()
            let phone : String = phoneTextField.trimmingCharacters()
            let secure : String = secureTextField.trimmingCharacters()
            let email : String  = emailTextField.trimmingCharacters()
            let driverType : String = licenseTypeTextField.trimmingCharacters()
            let driverRgn : String  = licenseRegionTextField.trimmingCharacters()
            let driverNum : String  = licenseNumberTextField.trimmingCharacters()
            let licenseYear : String = liceseYearTextField.trimmingCharacters()
            let licenseMonth : String = licenseMonthTextField.trimmingCharacters()
            let licenseDay : String = licenseDayTextField.trimmingCharacters()
            
            let authPreNum : String = numPreTextField.trimmingCharacters()
            let authTailNum : String = numTailTextField.trimmingCharacters()
            
            var driverTypeNum = "0"
            let token : String  = InfoManager.getInstance().getToken()

            
            if driverType == "1종대형"  {
                driverTypeNum = "0"
            }else if driverType == "1종보통"  {
                driverTypeNum = "1"
            }else if driverType == "1종특수"  {
                driverTypeNum = "3"
            }else if driverType == "2종보통자동"  {
                driverTypeNum = "4"
            }else if driverType == "2종보통수동"  {
                driverTypeNum = "5"
            }
            
            let params: Parameters = [
                "tel_no" : phone
                ,"nm" : nm
                ,"auth_no" : secure
                ,"email" : email
                ,"drvlcns_type" : driverTypeNum
                ,"drvlcns_no" : driverRgn + "-" + driverNum
                ,"drvlcns_expire_date" : licenseYear + licenseMonth + licenseDay
                ,"jumin_no" : authPreNum + authTailNum
                ,"auto_only" : stickGearCheckBtn.isChecked ? "0" : "1"
                ,"fcm_token" : token
            ]
            
            var licenseData : Data! = nil
            var profileData : Data! = nil
            var authData : Data! = nil
            
            if let setLicenseImage = licenseBtn.image(for: .normal) {
                licenseData = setLicenseImage.pngData()
                licenseData = setLicenseImage.jpegData(compressionQuality: 1.0)
            }
            
            if let setProfileImage = profileBtn.image(for: .normal) {
                profileData = setProfileImage.pngData()
                profileData = setProfileImage.jpegData(compressionQuality: 1.0)
            }
            
            if let setImage = authorityBtn.image(for: .normal) {
                authData = setImage.pngData()
                authData = setImage.jpegData(compressionQuality: 1.0)
            }
            
            if licenseData != nil && licenseData != nil && licenseData != nil {
                _ = NetworkManager.sharedInstance.requestMultipartFormdata(name, params, licenseData, profileData, authData,  target: self)
            }else{
                if parentDelegate != nil {
                    parentDelegate?.showLoadingProgressBarFromDelegate?(true)
                    parentDelegate?.showPopupViewFromDelegate("회원가입", "정보를 다시 입력해 주세요.")
                }
            }
            break
            
        case API_USER_REGIST_REQ_NUM:
            let phone : String = phoneTextField.trimmingCharacters()
            let params: Parameters = ["tel_no" : phone]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.post, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        if parentDelegate != nil {
            parentDelegate?.showLoadingProgressBarFromDelegate?(false)
        }
        
        switch requestID {
        case API_DRIVER_INSURANCE_INFO_REQUEST:
            //let json = JSON(data!)
            request(API_USER_REGIST)
            break
        case API_USER_REGIST:
            let json = JSON(data!)
            var content = ""
            if json["success"].stringValue == "0000" {
                content = "회원 가입이 완료되었습니다.\n승인이 처리 될떄까지 기다려 주세요."
                //self.dismiss(animated: true, completion: nil)
            }else{
                content = "관리자에게 문의해 주세요."
            }
            
            if parentDelegate != nil {
                parentDelegate?.showPopupViewFromDelegate("회원가입", content)
            }
            break
            
        case API_USER_REGIST_REQ_NUM:
            let json = JSON(data!)
            var content = ""
            if json["success"].stringValue == "0000" {
                content = "인증 요청이 완료되었습니다.\n카카오톡을 확인해 주세요."
            }else{
                content = "관리자에게 문의해 주세요."
            }
            
            if parentDelegate != nil {
                parentDelegate?.showPopupViewFromDelegate("회원가입", content)
            }
            break
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        secureRemainTimeLabel.isHidden = true
        mTimer?.invalidate()
        
        if parentDelegate != nil {
            parentDelegate?.showLoadingProgressBarFromDelegate?(false)
        }
        
        if(requestID == API_DRIVER_INSURANCE_INFO_REQUEST){
            if parentDelegate != nil {
                parentDelegate?.showPopupViewFromDelegate("회원가입", "정보를 다시 입력하세요.")
            }
        }else{
            let json = JSON(data!)
            print(json["error"])
            
            if parentDelegate != nil {
                parentDelegate?.showPopupViewFromDelegate("회원가입", "관리자에게 문의하세요.")
            }
        }
//        let json = JSON(data!)
//        print(json["error"])
//
//        let dialog = UIAlertController(title: "가입하기 오류", message: "오류 번호 : " + json["success"].stringValue, preferredStyle: .alert)
//        let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
//        dialog.addAction(action)
//        self.present(dialog, animated: true, completion: nil)
    }
    
    open func updateInfoFromPopup(_ title: String, _ idx: Int ){
        if popupType == 0 { // 운전면허 종류
            licenseTypeTextField.text = title
        }else{
            licenseRegionTextField.text = title
        }
    }
}

extension RegisterTableViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImage = info[.editedImage] as? UIImage {
            if selImgType == 0 {
                licenseBtn.setImage(editedImage, for: .normal)
                licenseBtn.imageView?.contentMode = .scaleAspectFit
                isLicenseImgDone = true
            }else if selImgType == 1 {
                profileBtn.setImage(editedImage, for: .normal)
                profileBtn.imageView?.contentMode = .scaleAspectFit
                isProfileImgDone = true
            }else{
                authorityBtn.setImage(editedImage, for: .normal)
                authorityBtn.imageView?.contentMode = .scaleAspectFit
                isAuthoImgDone = true
            }
        }
        dismiss(animated: true, completion: {})
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        if selImgType == 0 {
            licenseBtn.setImage(UIImage(named: "btn_ico_contract"), for: .normal)
            isLicenseImgDone = false
        }else if selImgType == 1 {
            profileBtn.setImage(UIImage(named: "btn_ico_contract"), for: .normal)
            isProfileImgDone = false
        }else{
            authorityBtn.setImage(UIImage(named: "btn_ico_contract"), for: .normal)
            isAuthoImgDone = false
        }
        
        dismiss(animated: true, completion: nil)
    }
}

extension RegisterTableViewController: UINavigationControllerDelegate {
    
}


extension String {
    func aesEncrypt() throws -> String{
        let DEFINE_KEY : String = "42885382FEFD4429A855D97B4C88F40E"
        
        //#if DEBUG
        //    DEFINE_KEY = "5F4D4D0F6C2D46A3ACAC34A9A4BAAEDB"
        //#endif
        
        let DEFINE_IV: [UInt8] = [ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 ]

        do {
            let enc = try AES(key: DEFINE_KEY.bytes, blockMode: CBC(iv: DEFINE_IV), padding: .pkcs5).encrypt(self.bytes)
            let encData = NSData(bytes: enc, length: Int(enc.count))
            let base64String: String = encData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0));
            return base64String
        } catch { print(error) }
        
        return ""
    }
    
//    func aesDecrypt(key: String, iv: String) throws -> String {
//        let data = NSData(base64Encoded: self, options: NSData.Base64DecodingOptions(rawValue: 0))
//        let dec = try AES(key: key, iv: iv, blockMode:.CBC).decrypt(data!.arrayOfBytes(), padding: PKCS7())
//        let decData = NSData(bytes: dec, length: Int(dec.count))
//        let result = NSString(data: decData, encoding: NSUTF8StringEncoding)
//        return String(result!)
//    }
}

extension RegisterTableViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var strText: String? = textField.text
        if strText == nil {
            strText = ""
        }
        
        if strText!.count >= 12 && string.count != 0 {return false}
        //strText = strText!.replacingOccurrences(of: "b", with: "a")
        if strText!.count > 1 && strText!.count == 2 && string != "" || strText!.count > 1 && strText!.count == 9 && string != "" {
            textField.text = "\(textField.text!)-\(string)"
            return false
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
