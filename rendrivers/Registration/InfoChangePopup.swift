//
//  InfoChangePopup.swift
//  rendrivers
//
//  Created by Yuna Daddy on 20/09/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

protocol InfoChangePopupDelegate {
    func infoChangePopupChange(title: String, idx: Int)
}

class InfoChangePopup: UIView {
    private var contentsView: UIView!
    //
    private var TB_CELL_HEIGHT: CGFloat = 150.0
    //
    private var searchTableView: UITableView!
    //
    var infoArr: [String] = []
    //
    var delegate: InfoChangePopupDelegate?
    //
    var titleName = ""
    
    deinit{
        println("")
    }
    
    required public init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColorFromRGBA(0x000000, alpha: 0.6)
    }
    
    func setData(_ arr: [String]){
        self.infoArr = arr
        searchTableView.reloadData()
    }
    
    func initView() {
        var inner_y: CGFloat = 0
        contentsView = getView(CGRectReMake(96, 650, 888, 620), UIColorFromRGBA(0xffffff, alpha: 1))
        contentsView.layer.cornerRadius = getNumber(12)
        contentsView.clipsToBounds = true
        self.addSubview(contentsView)
        
        //타이틀
        contentsView.addSubview(makeTitleBox(CGRectReMake(0, inner_y, 792, 140), titleName))
        
        inner_y += 140
        let table_h: CGFloat = TB_CELL_HEIGHT * 3
        contentsView.addSubview(makeSearchTableView(CGRectReMake(0, inner_y, 888, table_h)))
        
        inner_y += table_h
        contentsView.frame.size.height = getNumber(inner_y)
        contentsView.frame.origin.y = getNumber((getTargetHeight() - inner_y) / 2)
    }
    
    func makeTitleBox(_ rect: CGRect,_ title: String) -> UIView {
        let view = getView(rect, UIColor.clear)
        
        let titleLabel = getLabel(title, CGRectReMake(48, 0, 792, 140), getMediumFont(51), .left, UIColorFromRGBA(0x004179, alpha: 1))
        view.addSubview(titleLabel)
        
        view.addSubview(CommonUI.makeLine(CGRectReMake(0, 138, 888, 2)))
        
        return view
    }
    
    func makeSearchTableView(_ rect: CGRect) -> UIView {
        let view = getView(rect, UIColorFromRGBA(0xffffff, alpha: 1))
        
        searchTableView = getTableView(getRatioReverse(rect.size), .plain)
        searchTableView.backgroundColor = UIColor.clear
        searchTableView.delegate = self
        searchTableView.dataSource = self
        view.addSubview(searchTableView)
        
        return view
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch:UITouch = touches.first!
        let touchpoint:CGPoint = touch.location(in: self)
        if contentsView.frame.contains(touchpoint)  {
            println("true")
        } else {
            println("false")
            self.removeFromSuperview()
        }
    }
    
    // MARK: - onButton
    @objc func onMenuButton(_ sender: UIButton!) {
        println("onMenuButton")
        switch sender.tag {
        case 1:
            break
        case 2:
            break
        default: break
        }
    }
}

extension InfoChangePopup: UITableViewDataSource {
    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoChangeCell") as! InfoChangeCell? ?? InfoChangeCell(style: .default, reuseIdentifier: "InfoChangeCell")
        cell.selectionStyle = .none
        
        let themeName = infoArr[indexPath.row]
        cell.titleName.text = themeName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return getNumber(TB_CELL_HEIGHT)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let themeTitle = infoArr[indexPath.row]
        self.delegate?.infoChangePopupChange(title: themeTitle, idx: indexPath.row)
        
        self.removeFromSuperview()
    }
}

extension InfoChangePopup: UITableViewDelegate {
    
}
