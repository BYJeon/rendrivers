//
//  InfoChangeCell.swift
//  rendrivers
//
//  Created by Yuna Daddy on 20/09/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

class InfoChangeCell: UITableViewCell {
    
    var titleName: UILabel!
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        titleName = getLabel("", CGRectReMake(60, 0, 772, 150), getRegularFont(46), .left, UIColorFromRGBA(0x4a4a4a, alpha: 1))
        self.addSubview(titleName)
        
        let cellineview = getView(CGRectReMake(0, 148, 888, 2), UIColorFromRGBA(0xd3d3d3, alpha: 1))
        self.addSubview(cellineview)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }

}
