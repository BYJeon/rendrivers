//
//  RegisterViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 20/09/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

class RegisterViewController: BaseViewController {
    @IBOutlet var popupView: UIView!
    
    @IBOutlet var popupTitleLabel: UILabel!
    @IBOutlet var popupContentLabel: UITextView!
    
    private var infoPopupView: InfoChangePopup!
    private var childTableView: RegisterTableViewController!
    
    let KEY: String = "5F4D4D0F6C2D46A3ACAC34A9A4BAAEDB"
    
    let licenseTypeArr: [String] = [
        "1종 대형"
        , "1종 보통"
        , "1종 특수"
        , "2종 보통 자동"
        , "2종 보통 수동"
    ]
    
    let regionTypeArr: [String] = [ "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26"
                                  ,"서울", "부산", "경기", "강원", "충북", "충남", "전북", "전남", "경북", "경남", "제주", "대구", "인천", "광주", "대전", "울산" ]
    
    var popupType = 0 // 0 : 운전면허 종류, 1 : 운전면허 지역
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Touch Event 등록 후 함수를 연동한다. (getPosByMap)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dimDidClicked(_:)))
        self.dimView!.addGestureRecognizer(gesture)
        
        childTableView = (self.children[0] as! RegisterTableViewController)
        childTableView.parentDelegate = self
        
    }
    
    @IBAction func closeDidClicked(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    //DimView Clicked
    @objc func dimDidClicked(_ sender: UIGestureRecognizer){
        hidePopupView()
    }
    
    @IBAction func confirmDidClicked(_ sender: Any) {
        hidePopupView()
        
        if popupContentLabel.text.contains("회원 가입이 완료되었습니다."){
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func showInfoPopupView(_ idx: Int){
        popupType = idx
        if infoPopupView != nil {
            infoPopupView.removeFromSuperview()
        }
        infoPopupView = InfoChangePopup(frame: CGRectReMake(0, 0, getTargetWidth(), getTargetHeight()))
        infoPopupView.titleName = "정보 선택"
        infoPopupView.initView()
        infoPopupView.delegate = self
        infoPopupView.setData(idx == 0 ? licenseTypeArr : regionTypeArr)
        self.view.addSubview(infoPopupView)
    }
    
    private func showPopupView(){
        self.view.bringSubviewToFront(popupView)
        
        popupView.isHidden = false
        dimView!.isHidden = false
    }
    private func hidePopupView(){
        popupView.isHidden = true
        dimView!.isHidden = true
       
    }
    
    /// Delegate
    override func showPopupViewFromDelegate(_ title: String, _ content: String) {
        self.view.endEditing(true)
        popupTitleLabel.text = title
        popupContentLabel.text = content
        popupContentLabel.textColor = UIColorFromRGBA(0x78849E, alpha: 1)
        showPopupView()
    }
    
    override func showInfoPopUpView(_ idx: Int) {
        showInfoPopupView(idx)
    }
    
    override func showLoadingProgressBarFromDelegate(_ show: Bool) {
       self.showLoadingProgressBar(isShow: show)
    }
}

extension RegisterViewController: InfoChangePopupDelegate{
    func infoChangePopupChange(title: String, idx: Int) {
        print(title)
        childTableView.updateInfoFromPopup(title, idx)
    }
}
