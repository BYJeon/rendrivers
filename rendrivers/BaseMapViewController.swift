//
//  BaseMapViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 26/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import NMapsMap

class BaseMapViewController: UIViewController, NMapViewDelegate, NMFMapViewDelegate, NetManagerDelegate {
    enum state {
        case disabled
        case tracking
        case trackingWithHeading
    }
    
    internal var mapView: NMFMapView?
    internal var changeStateButton: UIButton?
    internal var currentState: state = .disabled
    
    override open var shouldAutorotate: Bool{
        return false
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //custom initialization
        mapView = NMFMapView(frame: self.view.frame)
        mapView?.delegate = self
        self.view.insertSubview(mapView!, at: 0)
        
//        mapView = NMapView(frame: self.view.frame)
//
//        if let mapView = mapView {
//            // set the delegate for map view
//            mapView.delegate = self
//            // set the application api key for Open MapViewer Library
//            mapView.setClientId("2zdxd3x2qv")
//            mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//            view.insertSubview(mapView, at: 0)
//        }
        
        // Add Controls.
//        changeStateButton = createButton()
//
//        if let button = changeStateButton {
//            //view.addSubview(button)
//        }
        
        // Do any additional setup after loading the view.
    }
    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        mapView?.didReceiveMemoryWarning()
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        mapView?.viewWillAppear()
//    }
//
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        mapView?.viewDidDisappear()
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        mapView?.viewDidAppear()
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//
//        mapView?.viewWillDisappear()
//        stopLocationUpdating()
//    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - NMapViewDelegate Methods
    
    open func onMapView(_ mapView: NMapView!, initHandler error: NMapError!) {
        if (error == nil) { // success
            // set map center and level
            mapView.setMapCenter(NGeoPoint(longitude:126.978371, latitude:37.5666091), atLevel:11)
            // set for retina display
            mapView.setMapEnlarged(true, mapHD: true)
            // set map mode : vector/satelite/hybrid
            mapView.mapViewMode = .vector
        } else { // fail
            print("onMapView:initHandler: \(error.description)")
        }
    }
    
    // MARK: - NMapPOIdataOverlayDelegate Methods
    open func onMapOverlay(_ poiDataOverlay: NMapPOIdataOverlay!, imageForOverlayItem poiItem: NMapPOIitem!, selected: Bool) -> UIImage! {
        return NMapViewResources.imageWithType(poiItem.poiFlagType, selected: selected)
    }
    
    open func onMapOverlay(_ poiDataOverlay: NMapPOIdataOverlay!, anchorPointWithType poiFlagType: NMapPOIflagType) -> CGPoint {
        return NMapViewResources.anchorPoint(withType: poiFlagType)
    }
    
    open func onMapOverlay(_ poiDataOverlay: NMapPOIdataOverlay!, calloutOffsetWithType poiFlagType: NMapPOIflagType) -> CGPoint {
        return CGPoint.zero
    }
    
    open func onMapOverlay(_ poiDataOverlay: NMapPOIdataOverlay!, imageForCalloutOverlayItem poiItem: NMapPOIitem!, constraintSize: CGSize, selected: Bool, imageForCalloutRightAccessory: UIImage!, calloutPosition: UnsafeMutablePointer<CGPoint>!, calloutHit calloutHitRect: UnsafeMutablePointer<CGRect>!) -> UIImage! {
        return nil
    }
    
    // MARK: - NMapLocationManagerDelegate Methods
    
//    open func locationManager(_ locationManager: NMapLocationManager!, didUpdateTo location: CLLocation!) {
//
//        let coordinate = location.coordinate
//
//        let myLocation = NGeoPoint(longitude: coordinate.longitude, latitude: coordinate.latitude)
//        let locationAccuracy = Float(location.horizontalAccuracy)
//
//        mapView?.mapOverlayManager.setMyLocation(myLocation, locationAccuracy: locationAccuracy)
//        mapView?.setMapCenter(myLocation)
//    }
//
//    open func locationManager(_ locationManager: NMapLocationManager!, didFailWithError errorType: NMapLocationManagerErrorType) {
//
//        var message: String = ""
//
//        switch errorType {
//        case .unknown: fallthrough
//        case .canceled: fallthrough
//        case .timeout:
//            message = "일시적으로 내위치를 확인 할 수 없습니다."
//        case .denied:
//            message = "위치 정보를 확인 할 수 없습니다.\n사용자의 위치 정보를 확인하도록 허용하시려면 위치서비스를 켜십시오."
//        case .unavailableArea:
//            message = "현재 위치는 지도내에 표시할 수 없습니다."
//        case .heading:
//            message = "나침반 정보를 확인 할 수 없습니다."
//        }
//
//        if (!message.isEmpty) {
//            let alert = UIAlertController(title:"NMapViewer", message: message, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title:"OK", style:.default, handler: nil))
//            present(alert, animated: true, completion: nil)
//        }
//
//        if let mapView = mapView, mapView.isAutoRotateEnabled {
//            mapView.setAutoRotateEnabled(false, animate: true)
//        }
//    }
//
//    func locationManager(_ locationManager: NMapLocationManager!, didUpdate heading: CLHeading!) {
//        let headingValue = heading.trueHeading < 0.0 ? heading.magneticHeading : heading.trueHeading
//        setCompassHeadingValue(headingValue)
//    }
//
//    func onMapViewIsGPSTracking(_ mapView: NMapView!) -> Bool {
//        return NMapLocationManager.getSharedInstance().isTrackingEnabled()
//    }
//
//    func findCurrentLocation() {
//        enableLocationUpdate()
//    }
//
//    func setCompassHeadingValue(_ headingValue: Double) {
//
//        if let mapView = mapView, mapView.isAutoRotateEnabled {
//            mapView.setRotateAngle(Float(headingValue), animate: true)
//        }
//    }
//
//    func stopLocationUpdating() {
//
//        disableHeading()
//        disableLocationUpdate()
//    }
    
    // MARK: - My Location
    
//    func enableLocationUpdate() {
//
//        if let lm = NMapLocationManager.getSharedInstance() {
//
//            if lm.locationServiceEnabled() == false {
//                locationManager(lm, didFailWithError: .denied)
//                return
//            }
//
//            if lm.isUpdateLocationStarted() == false {
//                // set delegate
//                lm.setDelegate(self)
//                // start updating location
//                lm.startContinuousLocationInfo()
//            }
//        }
//    }
//
//    func disableLocationUpdate() {
//
//        if let lm = NMapLocationManager.getSharedInstance() {
//
//            if lm.isUpdateLocationStarted() {
//                // start updating location
//                lm.stopUpdateLocationInfo()
//                // set delegate
//                lm.setDelegate(nil)
//            }
//        }
//
//        mapView?.mapOverlayManager.clearMyLocationOverlay()
//    }
//
//    // MARK: - Compass
//
//    func enableHeading() -> Bool {
//
//        if let lm = NMapLocationManager.getSharedInstance() {
//
//            let isAvailableCompass = lm.headingAvailable()
//
//            if isAvailableCompass {
//
//                mapView?.setAutoRotateEnabled(true, animate: true)
//
//                lm.startUpdatingHeading()
//            } else {
//                return false
//            }
//        }
//
//        return true;
//    }
//
//    func disableHeading() {
//        if let lm = NMapLocationManager.getSharedInstance() {
//
//            let isAvailableCompass = lm.headingAvailable()
//
//            if isAvailableCompass {
//                lm.stopUpdatingHeading()
//            }
//        }
//
//        mapView?.setAutoRotateEnabled(false, animate: true)
//    }
    
    // MARK: - Button Control
    
//    func createButton() -> UIButton? {
//
//        let button = UIButton(type: .custom)
//
//        button.frame = CGRect(x: 15, y: 30, width: 36, height: 36)
//        button.setImage(#imageLiteral(resourceName: "v4_btn_navi_location_normal"), for: .normal)
//
//        button.addTarget(self, action: #selector(buttonClicked(_:)), for: .touchUpInside)
//
//        return button
//    }
    
//    @objc func buttonClicked(_ sender: UIButton!) {
//
//        if let lm = NMapLocationManager.getSharedInstance() {
//
//            switch currentState {
//            case .disabled:
//                enableLocationUpdate()
//                updateState(.tracking)
//            case .tracking:
//                let isAvailableCompass = lm.headingAvailable()
//
//                if isAvailableCompass {
//                    enableLocationUpdate()
//                    if enableHeading() {
//                        updateState(.trackingWithHeading)
//                    }
//                } else {
//                    stopLocationUpdating()
//                    updateState(.disabled)
//                }
//            case .trackingWithHeading:
//                stopLocationUpdating()
//                updateState(.disabled)
//            }
//        }
//    }
    
    func updateState(_ newState: state) {
        
        currentState = newState
        
        switch currentState {
        case .disabled:
            changeStateButton?.setImage(#imageLiteral(resourceName: "v4_btn_navi_location_normal"), for: .normal)
        case .tracking:
            changeStateButton?.setImage(#imageLiteral(resourceName: "v4_btn_navi_location_selected"), for: .normal)
        case .trackingWithHeading:
            changeStateButton?.setImage(#imageLiteral(resourceName: "v4_btn_navi_location_my"), for: .normal)
        }
    }
    
    
    //MARK: - NetManager delegate
    func request(_ name: String) {
        println(">>> requestID = \(name)")
    }
    
    func requestFinished(_ requestID: String, _ data: Any!) {
    }
    
    func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
    }
    
    func requestFailed(_ requestID: String) {
        println(">>> requestID = \(requestID)")
    }
}
