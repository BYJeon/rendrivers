//
//  StringEx.swift
//  rendrivers
//
//  Created by Yuna Daddy on 26/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import Foundation

extension String {
    var lastPathComponent: String {
        
        get {
            return (self as NSString).lastPathComponent
        }
    }
    
    func getCurrencyString() -> String {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .decimal
        currencyFormatter.groupingSize = 3
        currencyFormatter.alwaysShowsDecimalSeparator = false
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.locale = NSLocale.current
        
        if let number = Double(self) {
            //            println(">>> number = \(number)")
            let transNumber = NSNumber(value:number)
            //            println(">>> number = \(transNumber)")
            
            let priceString: String = currencyFormatter.string(for: transNumber)!
            //            println(">>> priceString = \(priceString)")
            return priceString
        }
        else {
            return self
        }
    }
    
    func getArrayAfterRegex(regex: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: self,
                                        range: NSRange(self.startIndex..., in: self))
            return results.map {
                String(self[Range($0.range, in: self)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
}

extension UITextField {
    func setTextFieldPaddingAndBorder(_ amount: CGFloat) { //왼쪽에 여백 주기
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
        self.layer.cornerRadius = 3
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func trimmingCharacters() -> String {
        if let textField = self.text {
            return textField.trimmingCharacters(in: .whitespaces)
        }
        return ""
    }
}
