//
//  ContractViewController.swift
//  rendrivers
//
//  Created by Yuna on 07/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AWSS3
import NYTPhotoViewer

class ContractViewController: BaseViewController {
    
    @IBOutlet var contractImg: UIImageView!
    @IBOutlet var contract2Img: UIImageView!
    @IBOutlet var contractExamImg: UIImageView!
    
    @IBOutlet var carNoLabel: UILabel!
    
    //이미지 등록
    var imagePicker = UIImagePickerController()
    private var curSelIdx = 0
    private let infoManager = InfoManager.getInstance()
    private let pathInfoManager = PathInfoManager.sharedInstance
    
    private var hasContractImg = false
    private var hasContract2Img = false
    private var contractImgPath = ""
    private var contractImg2Path = ""
    
    private var uploadProgress : Float = 0
    private var uploadCnt = 0
    private var contractCnt = 0
    
    @IBOutlet var contractDesc: FixedTextView!
    
    @objc var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    @objc var progressBlock: AWSS3TransferUtilityProgressBlock?
    @objc lazy var transferUtility = {
        AWSS3TransferUtility.default()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        contractDesc.text = "임대차(차량렌트)계약서 작성 미션을 수행해주세요. 작성 후에 아래 첨부하기 버튼을 이용하여 계약서를 촬영하여주시고, 원본은 타고오신 차량 조수석 수납함에 넣어주세요."
        contractDesc.textColor = UIColorFromRGBA(0x959DAD, alpha: 1)
        
        let gestureExamRequest = UITapGestureRecognizer(target: self, action: #selector(self.contractExamClicked(_:)))
        self.contractExamImg.addGestureRecognizer(gestureExamRequest)
        
        let gestureRequest = UITapGestureRecognizer(target: self, action: #selector(self.contractClicked(_:)))
        self.contractImg.addGestureRecognizer(gestureRequest)
        
        let gesture2Request = UITapGestureRecognizer(target: self, action: #selector(self.contract2Clicked(_:)))
        self.contract2Img.addGestureRecognizer(gesture2Request)
        
        if infoManager.getDriverStatus() == "4"{
            carNoLabel.text = PathInfoManager.sharedInstance.start_car_no
        }else if infoManager.getDriverStatus() == "8"{ //경유지 없는 미션 수행
            carNoLabel.text = PathInfoManager.sharedInstance.start_car_no
        }
        
        //S3 파일 업로드
        self.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                if (self.uploadProgress < Float(progress.fractionCompleted)) {
                    self.uploadProgress = Float(progress.fractionCompleted)
                }
            })
        }
        
        self.completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                if let error = error {
                    print("Failed with error: \(error)")
                    //self.statusLabel.text = "Failed"
                }
                else if(self.uploadProgress != 1.0) {
                    //self.statusLabel.text = "Failed"
                    NSLog("Error: Failed - Likely due to invalid region / filename")
                }
                else{
                    self.uploadCnt += 1
                    if self.contractCnt == self.uploadCnt {
                        self.request(API_DRIVER_MISSION_COMPLETE_REQUEST)
                    }
                    print("Complete")
                }
            })
        }
        
    }
    
    //S3 이미지 업로드
    func uploadImage(with data: Data, keyName: String) {
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = progressBlock
        expression.setValue("public-read", forRequestHeader: "x-amz-acl")
        
        DispatchQueue.main.async(execute: {
            self.uploadProgress = 0
        })
        
        transferUtility.uploadData(
            data,
            key: keyName,
            contentType: "image/png",
            expression: expression,
            completionHandler: completionHandler).continueWith { (task) -> AnyObject? in
                if let error = task.error {
                    print("Error: \(error.localizedDescription)")
                    
                    DispatchQueue.main.async {
                        //self.statusLabel.text = "Failed"
                    }
                }
                
                if let _ = task.result {
                    
                    DispatchQueue.main.async {
                        //self.statusLabel.text = "Uploading..."
                        print("Upload Starting!")
                    }
                    
                    // Do something with uploadTask.
                }
                
                return nil;
        }
    }
    
    
    @IBAction func contractRegistrationDidClicked(_ sender: Any) {
        if hasContractImg {
            contractCnt += 1
        }
        
        if hasContract2Img {
            contractCnt += 1
        }
        
        if contractCnt == 0 {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        if hasContractImg {
            contractImgPath = makeUploadImageName("0")
            uploadImage(with: (contractImg.image?.pngData()!)!, keyName: contractImgPath)
        }
        
        if hasContract2Img {
            contractImg2Path = makeUploadImageName("1")
            uploadImage(with: (contract2Img.image?.pngData()!)!, keyName: contractImg2Path)
        }
        
        showLoadingProgressBar(isShow: true)
    }
    // 계약서 예제 클릭
    @objc func contractExamClicked(_ sender: UIGestureRecognizer){
        let photo = ContractImage()
        photo.image = UIImage(named: "contract_example")
        let photosViewController = NYTPhotosViewController(photos: [photo])
        photosViewController.overlayView?.rightBarButtonItem = nil
        self.present(photosViewController, animated: true, completion: nil)
    }
    
    @IBAction func cancelDidClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // 계약서 조회 클릭
    @objc func contractClicked(_ sender: UIGestureRecognizer){
        curSelIdx = 0
        openCamera()
    }
    
    @objc func contract2Clicked(_ sender: UIGestureRecognizer){
        curSelIdx = 1
        openCamera()
    }
    
    func makeUploadImageName(_ index: String)-> String{
        let now=NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        
        //let img = mapView?.takeScreenshot(pathInfoView.bounds.height)
        let imgPath = "contract/" + infoManager.getAppID() + dateFormatter.string(from: now as Date)+"_"+index+".png"
        
        return imgPath
        //uploadImage(with: (img?.pngData()!)!, keyName: imgPath)
    }
    
    // MARK:- camera picker
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: {
                action in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - network
    override func request(_ name: String) {
        let infoManager = InfoManager.getInstance()
        switch name {
            
        case API_DRIVER_MISSION_COMPLETE_REQUEST:
            let req_id = infoManager.getReqID()
            let mission_id = pathInfoManager.mission_id
            let token : String  = infoManager.getApiToken()
            
            
            let photos: Parameters = [
                "photo1": contractImgPath
                ,"photo2": contractImg2Path
                ,"photo3": ""
                ,"photo4": ""
                ,"photo5": ""
            ]
            
            let params: Parameters = [
                "req_id" : req_id
                ,"misstion_id" : mission_id
                ,"api_token" : token
                ,"photos" : photos
            ]
            
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.post, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        
        showLoadingProgressBar(isShow: false)
        switch requestID {
        case API_DRIVER_MISSION_COMPLETE_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                if infoManager.getDriverStatus() == "4" {//경유지 있는 미션
                    pathInfoManager.action_gb_by_app = "5"
                }else if infoManager.getDriverStatus() == "8" {//경유지 없는  미션
                    pathInfoManager.action_gb_by_app = "8"
                }
            }
            
            self.dismiss(animated: false, completion: nil)
            break
            
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
    }
    
}

extension ContractViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImage = info[.editedImage] as? UIImage {
            if curSelIdx == 0 {
                contractImg.image = editedImage
                hasContractImg = true
            }else{
                contract2Img.image = editedImage
                hasContract2Img = true
            }
        }
        dismiss(animated: true, completion: {})
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //carImgBtnArr[curImgSelIdx].setImage(UIImage(named: "box_area"), for: .normal)
        //carImgPathArr[curImgSelIdx] = ""
        //bCarImgDone[curImgSelIdx] = false
        
        if curSelIdx == 0 {
            hasContractImg = false
            contractImg.image = UIImage(named: "box_area")
        }else{
            hasContract2Img = false
            contract2Img.image = UIImage(named: "box_area")
        }
        dismiss(animated: true, completion: nil)
    }
}

extension ContractViewController: UINavigationControllerDelegate {
    
}
