//
//  CarInfoTableViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 01/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AWSS3

class CarInfoTableViewController: BaseTableViewController {
    private let infoManager = InfoManager.getInstance()
    private let pathInfoManager = PathInfoManager.sharedInstance
    
    @IBOutlet var carNoTextField: UITextField!
    
    @IBOutlet var carSearchView: UIView!
    @IBOutlet var carInfoView: UIView!
    
    @IBOutlet var carInfoNoLabel: UILabel!
    @IBOutlet var carInfoNameLabel: UILabel!
    
    @IBOutlet var insranceNameTextField: UITextField!
    @IBOutlet var insranceNumTextField: UITextField!
    
    @IBOutlet var realMatchCheckBox: CheckBox!
    
    @IBOutlet var insranceCell: UITableViewCell!
    //이미지 등록
    var imagePicker = UIImagePickerController()
    
    private final let CAR_IMG_CNT = 11
    private var curImgSelIdx = 1
    
    @IBOutlet var carImg1Btn: UIButton!
    @IBOutlet var carImg2Btn: UIButton!
    @IBOutlet var carImg3Btn: UIButton!
    @IBOutlet var carImg4Btn: UIButton!
    @IBOutlet var carImg5Btn: UIButton!
    @IBOutlet var carImg6Btn: UIButton!
    @IBOutlet var carImg7Btn: UIButton!
    @IBOutlet var carImg8Btn: UIButton!
    @IBOutlet var carImg9Btn: UIButton!
    @IBOutlet var carImg10Btn: UIButton!
    @IBOutlet var carImg11Btn: UIButton!
    
    private var carImgBtnArr: [UIButton] = []
    private var carImgPathArr: [String] = []
    private var bCarImgDone : [Bool] = []
    
    
    private var lastOffset = 0
    
    private var uploadProgress : Float = 0
    private var uploadCnt = 0
    
    @objc var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    @objc var progressBlock: AWSS3TransferUtilityProgressBlock?
    @objc lazy var transferUtility = {
        AWSS3TransferUtility.default()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        carImgBtnArr.append(carImg1Btn)
        carImgBtnArr.append(carImg2Btn)
        carImgBtnArr.append(carImg3Btn)
        carImgBtnArr.append(carImg4Btn)
        carImgBtnArr.append(carImg5Btn)
        carImgBtnArr.append(carImg6Btn)
        carImgBtnArr.append(carImg7Btn)
        carImgBtnArr.append(carImg8Btn)
        carImgBtnArr.append(carImg9Btn)
        carImgBtnArr.append(carImg10Btn)
        carImgBtnArr.append(carImg11Btn)


        carInfoNoLabel.text = ""
        insranceCell.frame = CGRect(x: 0, y: 0, width: 0 ,height: 0)
        
        insranceNameTextField.text = pathInfoManager.insr_nm
        insranceNumTextField.text = pathInfoManager.insr_no
        
        //목적지 도착(6) / 경유지 도착(3)일 경우 차량 조회 필요 없음
        if infoManager.getDriverStatus() == "6" {
            insranceNameTextField.isUserInteractionEnabled = false
            insranceNumTextField.isUserInteractionEnabled = false
            carSearchView.isHidden = true
            carInfoView.isHidden = false
            
            if pathInfoManager.waypoint_address.count == 0{
                carInfoNoLabel.text = pathInfoManager.start_car_no
                carInfoNameLabel.text = pathInfoManager.start_car_model
                realMatchCheckBox.isChecked = pathInfoManager.start_car_real_match.count == 0 ? false : true
                if pathInfoManager.start_car_real_match == "1" || pathInfoManager.start_car_real_match == "2" {
                    realMatchCheckBox.isChecked = false
                }else{
                    realMatchCheckBox.isChecked = true
                }
                
            }else{
                carInfoNoLabel.text = pathInfoManager.waypoint_car_no
                carInfoNameLabel.text = pathInfoManager.waypoint_car_model
                realMatchCheckBox.isChecked = pathInfoManager.waypoint_car_real_match.count == 0 ? false : true
                if pathInfoManager.waypoint_car_real_match == "1" || pathInfoManager.waypoint_car_real_match == "2" {
                    realMatchCheckBox.isChecked = false
                }else{
                    realMatchCheckBox.isChecked = true
                }
                
            }
            
            
        }else if infoManager.getDriverStatus() == "3"{ //경유지 정보
            carSearchView.isHidden = true
            carInfoView.isHidden = false
            carInfoNoLabel.text = pathInfoManager.start_car_no
            carInfoNameLabel.text = pathInfoManager.start_car_model
            if pathInfoManager.start_car_real_match == "1" || pathInfoManager.start_car_real_match == "2" {
                realMatchCheckBox.isChecked = false
            }else{
                realMatchCheckBox.isChecked = true
            }
        }else{
            carSearchView.isHidden = false
            carInfoView.isHidden = true
        }

        for num in 0..<CAR_IMG_CNT{
            carImgPathArr.append("")
            bCarImgDone.append(false)
            carImgBtnArr[num].tag = num
            carImgBtnArr[num].addTarget(self, action:#selector(openCameraDidClicked) , for: .touchUpInside)
        }
        
        //S3 파일 업로드
        self.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                if (self.uploadProgress < Float(progress.fractionCompleted)) {
                    self.uploadProgress = Float(progress.fractionCompleted)
                }
            })
        }
        
        self.completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                if let error = error {
                    print("Failed with error: \(error)")
                    //self.statusLabel.text = "Failed"
                }
                else if(self.uploadProgress != 1.0) {
                    //self.statusLabel.text = "Failed"
                    NSLog("Error: Failed - Likely due to invalid region / filename")
                }
                else{
                    if self.uploadCnt == self.CAR_IMG_CNT {
                        if self.infoManager.getDriverStatus() == "6"{ //목적지 도착 정보 업로드
                            self.request(API_DRIVER_ARRIVED_ACTION_END_REQUEST)
                        }else if self.infoManager.getDriverStatus() == "3"{ //경유지 도착 정보 업로드
                            self.request(API_DRIVER_ARRIVED_ACTION_WAYPOINT_REQUEST)
                        }else if self.infoManager.getDriverStatus() == "5"{ //경유지 출발(제2차량 픽업) 정보 업로드
                            self.request(API_DRIVER_TAKEOVER_ACTION_WAYPOINT_REQUEST)
                        }else if self.infoManager.getDriverStatus() == "1"{
                            self.request(API_DRIVER_ARRIVED_ACTION_START_REQUEST)  //출발지 도착 정보 업로드
                        }
                        
                        return
                    }
                    
                    self.uploadCnt += 1
                    print(self.uploadCnt)
                    self.uploadImage(with: (self.carImgBtnArr[self.uploadCnt-1].imageView?.image?.pngData())!, keyName: self.carImgPathArr[self.uploadCnt-1])
                    print("Complete")
                }
            })
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2 {
            return pathInfoManager.center_gb != "1"  ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        }else{
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    @IBAction func carSearchDidClicked(_ sender: Any) {
        request(API_CAR_AUTO_UP_REQUEST)
        self.view.endEditing(true)
    }
    
    
    func makeUploadImageName(_ index: String)-> String{
        let now=NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        
        //let img = mapView?.takeScreenshot(pathInfoView.bounds.height)
        let imgPath = "confirmation/" + infoManager.getAppID() + dateFormatter.string(from: now as Date)+"_"+index+".png"
        
        return imgPath
        //uploadImage(with: (img?.pngData()!)!, keyName: imgPath)
    }
    
    //S3 이미지 업로드
    func uploadImage(with data: Data, keyName: String) {
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = progressBlock
        expression.setValue("public-read", forRequestHeader: "x-amz-acl")
        
        DispatchQueue.main.async(execute: {
            self.uploadProgress = 0
        })
        
        transferUtility.uploadData(
            data,
            key: keyName,
            contentType: "image/png",
            expression: expression,
            completionHandler: completionHandler).continueWith { (task) -> AnyObject? in
                if let error = task.error {
                    print("Error: \(error.localizedDescription)")
                    
                    DispatchQueue.main.async {
                        //self.statusLabel.text = "Failed"
                    }
                }
                
                if let _ = task.result {
                    
                    DispatchQueue.main.async {
                        //self.statusLabel.text = "Uploading..."
                        print("Upload Starting!")
                    }
                    
                    // Do something with uploadTask.
                }
                
                return nil;
        }
    }
    
    @IBAction func completeDidClicked(_ sender: Any) {
        let carNum = carInfoNoLabel.text
        if carNum != nil && carNum!.count == 0 && infoManager.getDriverStatus() != "3"{
            if parentDelegate != nil {
                parentDelegate?.showPopupViewFromDelegate("차량정보 등록", "차량 번호를 조회해 주세요.")
                return
            }
        }
        
        let imgResult = isAllImgDone()
        if !imgResult{
            if parentDelegate != nil {
                parentDelegate?.showPopupViewFromDelegate("차량정보 등록", "차량의 모든 이미지를 촬영해 주세요")
                return
            }
        }
        
        
        //제2차량 픽업
        if infoManager.getDriverStatus() == "5" && pathInfoManager.center_gb == "1" {
            if insranceNameTextField.trimmingCharacters().count == 0 || insranceNumTextField.trimmingCharacters().count == 0 {
                if parentDelegate != nil {
                    parentDelegate?.showPopupViewFromDelegate("차량정보 등록", "보험 정보를 정확히 입력해 주세요.")
                    return
                }
            }
            
        }
        uploadCnt = 1
        uploadImage(with: (carImgBtnArr[uploadCnt-1].imageView?.image?.pngData())!, keyName: carImgPathArr[uploadCnt-1])
        
        if parentDelegate != nil {
            parentDelegate?.showLoadingProgressBarFromDelegate?(true)
        }
    }
    
    //MARK: - network
    override func request(_ name: String) {
        switch name {
        case API_DRIVER_ARRIVED_ACTION_END_REQUEST:
            let req_id = infoManager.getReqID()
            let token : String  = infoManager.getApiToken()
            
            
            let phptos_info_params: Parameters = [
                "front": carImgPathArr[0]
                ,"front_left": carImgPathArr[1]
                ,"front_right": carImgPathArr[9]
                
                ,"left_front": carImgPathArr[2]
                ,"dashboard": carImgPathArr[10]
                ,"right_front": carImgPathArr[8]
                
                ,"left_back": carImgPathArr[3]
                ,"right_back": carImgPathArr[7]
                
                ,"back_left": carImgPathArr[4]
                ,"back": carImgPathArr[5]
                ,"back_right": carImgPathArr[6]
                
            ]
            
            let params: Parameters = [
                "req_id" : req_id
                ,"api_token" : token
                ,"photos" : phptos_info_params
            ]
            
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.post, target: self)
            break
        
        case API_DRIVER_ARRIVED_ACTION_WAYPOINT_REQUEST:
            let req_id = infoManager.getReqID()
            let token : String  = infoManager.getApiToken()
            
            
            let phptos_info_params: Parameters = [
                "front": carImgPathArr[0]
                ,"front_left": carImgPathArr[1]
                ,"front_right": carImgPathArr[9]
                
                ,"left_front": carImgPathArr[2]
                ,"dashboard": carImgPathArr[10]
                ,"right_front": carImgPathArr[8]
                
                ,"left_back": carImgPathArr[3]
                ,"right_back": carImgPathArr[7]
                
                ,"back_left": carImgPathArr[4]
                ,"back": carImgPathArr[5]
                ,"back_right": carImgPathArr[6]
                
            ]
            
            let params: Parameters = [
                "req_id" : req_id
                ,"api_token" : token
                ,"photos" : phptos_info_params
            ]
            
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.post, target: self)
            break
            
        case API_DRIVER_TAKEOVER_ACTION_WAYPOINT_REQUEST:
            let req_id = infoManager.getReqID()
            let token : String  = infoManager.getApiToken()
            let car_num : String = carInfoNoLabel.text!
            
            pathInfoManager.cur_mission_car_no = car_num
            
            let insr_nm : String = insranceNameTextField.trimmingCharacters()
            let insr_no : String = insranceNumTextField.trimmingCharacters()
            let real_match : String = realMatchCheckBox.isChecked ? "0" : "1"
            
            let center_info_params: Parameters = [
                "center_address" : ""
                ,"center_nm": ""
                ,"center_tel_no": ""
                ,"center_la": ""
                ,"center_lo": ""
            ]
            
            let phptos_info_params: Parameters = [
                "front": carImgPathArr[0]
                ,"front_left": carImgPathArr[1]
                ,"front_right": carImgPathArr[9]
                
                ,"left_front": carImgPathArr[2]
                ,"dashboard": carImgPathArr[10]
                ,"right_front": carImgPathArr[8]
                
                ,"left_back": carImgPathArr[3]
                ,"right_back": carImgPathArr[7]
                
                ,"back_left": carImgPathArr[4]
                ,"back": carImgPathArr[5]
                ,"back_right": carImgPathArr[6]
                
            ]
            
            let params: Parameters = [
                "req_id" : req_id
                ,"api_token" : token
                ,"insr_nm" : insr_nm
                ,"car_no" : car_num
                ,"real_match" : real_match
                ,"insr_no" : insr_no
                ,"center_info" : center_info_params
                ,"photos" : phptos_info_params
            ]
            
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.post, target: self)
            break
            
        case API_DRIVER_ARRIVED_ACTION_START_REQUEST:
            let req_id = infoManager.getReqID()
            let token : String  = infoManager.getApiToken()
            let car_num : String = carInfoNoLabel.text!
            
            pathInfoManager.cur_mission_car_no = car_num
            
            let insr_nm : String = insranceNameTextField.trimmingCharacters()
            let insr_no : String = insranceNumTextField.trimmingCharacters()
            let real_match : String = realMatchCheckBox.isChecked ? "0" : "1"
            
            let center_info_params: Parameters = [
                "center_address" : ""
                ,"center_nm": ""
                ,"center_tel_no": ""
                ,"center_la": ""
                ,"center_lo": ""
            ]
            
            let phptos_info_params: Parameters = [
                "front": carImgPathArr[0]
                ,"front_left": carImgPathArr[1]
                ,"front_right": carImgPathArr[9]
                
                ,"left_front": carImgPathArr[2]
                ,"dashboard": carImgPathArr[10]
                ,"right_front": carImgPathArr[8]
                
                ,"left_back": carImgPathArr[3]
                ,"right_back": carImgPathArr[7]
                
                ,"back_left": carImgPathArr[4]
                ,"back": carImgPathArr[5]
                ,"back_right": carImgPathArr[6]
                
            ]
            
            let params: Parameters = [
                "req_id" : req_id
                ,"api_token" : token
                ,"insr_nm" : insr_nm
                ,"car_no" : car_num
                ,"real_match" : real_match
                ,"insr_no" : insr_no
                ,"center_info" : center_info_params
                ,"photos" : phptos_info_params
            ]
            
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.post, target: self)
            break
        case API_CAR_AUTO_UP_REQUEST: //차량 정보 조회
            var carNo : String = carNoTextField.trimmingCharacters()
            carNo = carNo.replacingOccurrences(of: " ", with: "")
            let newCarNo  = carNo.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            
            let extraParam : String = "?car_no=" + newCarNo! + "&api_token="+infoManager.getApiToken()
             _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
            
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        switch requestID {
        case API_DRIVER_ARRIVED_ACTION_END_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                if parentDelegate != nil {
                    pathInfoManager.action_gb_by_app = "7"
                    parentDelegate?.showLoadingProgressBarFromDelegate?(false)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            break
        case API_DRIVER_ARRIVED_ACTION_WAYPOINT_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                if parentDelegate != nil {
                    pathInfoManager.action_gb_by_app = "4"
                    parentDelegate?.showLoadingProgressBarFromDelegate?(false)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            break
        case API_DRIVER_TAKEOVER_ACTION_WAYPOINT_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                if parentDelegate != nil {
                    pathInfoManager.action_gb_by_app = "5"
                    parentDelegate?.showLoadingProgressBarFromDelegate?(false)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            break
        case API_DRIVER_ARRIVED_ACTION_START_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                if parentDelegate != nil {
                    pathInfoManager.action_gb_by_app = "2"
                    parentDelegate?.showLoadingProgressBarFromDelegate?(false)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            break
        case API_CAR_AUTO_UP_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                carSearchView.isHidden = true
                carInfoView.isHidden = false
                
                carInfoNoLabel.text = json["car_no"].stringValue
                carInfoNameLabel.text =  json["car_mdl"].stringValue
            }
            break
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        var carNo : String = carNoTextField.trimmingCharacters()
        carNo = carNo.replacingOccurrences(of: " ", with: "")
        let newCarNo  = carNo.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        
        let result = isCarNumCorrect(newCarNo!)
        
        if !result{
            if parentDelegate != nil {
                parentDelegate?.showPopupViewFromDelegate("차량 번호 검색", "차량 번호를 정확히 입력해 주세요.")
                return
            }
        }else{
            carSearchView.isHidden = true
            carInfoView.isHidden = false
            
            carInfoNoLabel.text = carNo
            carInfoNameLabel.text =  ""
            return
        }
        
        let json = JSON(data!)
        let dialog = UIAlertController(title: "통신 오류", message: "오류 번호 : " + json["error"].stringValue, preferredStyle: .alert)
        let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
        dialog.addAction(action)
        self.present(dialog, animated: true, completion: nil)
    }
    
    private func isCarNumCorrect(_ carNo: String) -> Bool{
        if carNo.count != 15 && carNo.count != 16 {
            return false
        }
        
        if carNo.count == 15 {
            let start = carNo.index(carNo.startIndex, offsetBy: 0)
            let end = carNo.index(carNo.startIndex, offsetBy: 2)
            let range = start..<end
            
            let preNo = Int(carNo[range])
            let rstart = carNo.index(carNo.startIndex, offsetBy: 11)
            let rend = carNo.index(carNo.startIndex, offsetBy: 15)
            let rrange = rstart..<rend
            let tailNo = Int(carNo[rrange])
            
            if preNo! > 100 || tailNo! > 100000 { return false }
        }else if carNo.count == 16 {
            let start = carNo.index(carNo.startIndex, offsetBy: 0)
            let end = carNo.index(carNo.startIndex, offsetBy: 3)
            let range = start..<end
            
            let preNo = Int(carNo[range])
            let rstart = carNo.index(carNo.startIndex, offsetBy: 12)
            let rend = carNo.index(carNo.startIndex, offsetBy: 16)
            let rrange = rstart..<rend
            let tailNo = Int(carNo[rrange])
            
            if preNo! > 10000 || tailNo! > 100000 { return false }
        }
        return true
    }
    //사진 등록 이벤트
    @objc open  func openCameraDidClicked(_ sender: UIButton!) {
        curImgSelIdx = sender.tag
        self.openCamera()
    }
    
    // MARK:- camera picker
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: {
                action in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func isAllImgDone() -> Bool {
        var result = true
        
//        #if DEBUG
//            result = false
//        #endif
        
        for num in 0..<CAR_IMG_CNT{
//            #if DEBUG
//                if bCarImgDone[num] == true{
//                    return true
//                }
//            #endif
            
//            #if RELEASE
//            if bCarImgDone[num] == false{
//                return false
//            }
//            #endif
            
            if bCarImgDone[num] == false{
                return false
            }
        }
        
        return result
    }
    
}

extension CarInfoTableViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImage = info[.editedImage] as? UIImage {
            
            carImgBtnArr[curImgSelIdx].setImage(editedImage, for: .normal)
            carImgBtnArr[curImgSelIdx].imageView?.contentMode = .scaleAspectFit
            carImgPathArr[curImgSelIdx] = makeUploadImageName(String(curImgSelIdx))
            bCarImgDone[curImgSelIdx] = true
            
            #if DEBUG
            for num in 0..<CAR_IMG_CNT{
                carImgBtnArr[num].setImage(editedImage, for: .normal)
                carImgPathArr[num] = makeUploadImageName(String(num))
                bCarImgDone[num] = true
            }
            #endif
        }
        dismiss(animated: true, completion: {})
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        carImgBtnArr[curImgSelIdx].setImage(UIImage(named: "box_area"), for: .normal)
        carImgPathArr[curImgSelIdx] = ""
        bCarImgDone[curImgSelIdx] = false
        
        dismiss(animated: true, completion: nil)
    }
}

extension CarInfoTableViewController: UINavigationControllerDelegate {
    
}
