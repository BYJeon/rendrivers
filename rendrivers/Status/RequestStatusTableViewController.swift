//
//  RequestStatusTableViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 01/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class RequestStatusTableViewController: BaseTableViewController {
    
    //픽업지 도착 완료
    @IBOutlet var pickUpArrivedView: UIView!
    @IBOutlet var pickUpTitleLabel: UILabel!
    @IBOutlet var pickUpTimedLabel: UILabel!
    @IBOutlet var pickUpRewardedLabel: UILabel!
    @IBOutlet var pickUpArrivedCallBtn: UIButton!
    @IBOutlet var pickUpArrivedCheckBox: CheckBox!
    
    //차량 픽업 완료
    @IBOutlet var pickupCompleteView: UIView!
    @IBOutlet var pickUpCompleteLabel: UILabel!
    @IBOutlet var pickUpCompleteSubLabel: UILabel!
    @IBOutlet var pickUpCompleteCheckBox: CheckBox!
    
    //경유지 도착
    @IBOutlet var waypointArrivedView: UIView!
    @IBOutlet var wayPointTitleLabel: UILabel!
    @IBOutlet var wayPointAddrLabel: UILabel!
    @IBOutlet var wayPointTimeLabel: UILabel!
    @IBOutlet var wayPointRewardLabel: UILabel!
    @IBOutlet var wayPointCallBtn: UIButton!

    @IBOutlet var waypointTimeTitleLabel: UILabel!
    @IBOutlet var waypointTimeLabel: UILabel!
    
    @IBOutlet var waypointRewardTitleLabel: UILabel!
    @IBOutlet var wayPointCheckBox: CheckBox!
    
    //계약서 수행 View
    @IBOutlet var contractCompleteView: UIView!
    @IBOutlet var contractTitleLabel: UILabel!
    @IBOutlet var contractRewardLabel: UILabel!
    @IBOutlet var contractImgView: UIImageView!
    @IBOutlet var contractRewardTitleLabel: UILabel!
    
    //제2차량 픽업
    @IBOutlet var secondPickupView: UIView!
    @IBOutlet var secondPickUpTitleLabel: UILabel!
    @IBOutlet var secondPickUpContentLabel: UILabel!
    @IBOutlet var secondPickUpCheckBox: CheckBox!
    
    //목적지 도착
    @IBOutlet var targetArrivedView: UIView!
    @IBOutlet var targetArrivedTitleLabel: UILabel!
    @IBOutlet var targetArrivedAddrLabel: UITextView!
    @IBOutlet var targetArrivedTimeLabel: UILabel!
    @IBOutlet var targetArrivedRewardLabel: UILabel!
    @IBOutlet var targetArrivedCheckBox: CheckBox!
    @IBOutlet var targetCallBtn: UIButton!
    @IBOutlet var targetTimeTitleLabel: UILabel!
    @IBOutlet var targetRewardTitleLabel: UILabel!
    
    
    //하단 계약서 수행 View
    @IBOutlet var contractBottomView: UIView!
    @IBOutlet var contractBottomTitleLabel: UILabel!
    @IBOutlet var contractBottomRewardLabel: UILabel!
    @IBOutlet var contractBottomImgView: UIImageView!
    @IBOutlet var contractBottomRewardTitleLabel: UILabel!
    
    //마지막 확인하기
    @IBOutlet var lastCheckView: UIView!
    @IBOutlet var lastCheckTitleLabel: UILabel!
    @IBOutlet var lastCheckContentLabel: UILabel!
    @IBOutlet var lastCheckBox: CheckBox!
    
    private let infoManager = InfoManager.getInstance()
    private let pathInfoManager = PathInfoManager.sharedInstance
    
    
    @IBOutlet var cpNameLabel: UILabel!
    @IBOutlet var empNameLabel: UILabel!
    
    @IBOutlet var add_req_car: UILabel!
    @IBOutlet var add_req: UILabel!
    
    
    private var hasWayPoint = false
    private var hasMission = false
    private var hasBottomMission = false
    
    private var limitTimeInfo = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Touch Event 등록 후 함수를 연동한다. (getPosByMap)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.pickUpArridveDidClicked(_:)))
        self.pickUpArrivedView.addGestureRecognizer(gesture)
        
        // Touch Event 등록 후 함수를 연동한다. (getPosByMap)
        let carGesture = UITapGestureRecognizer(target: self, action: #selector(self.pickUpCarDidClicked))
        self.pickupCompleteView.addGestureRecognizer(carGesture)
        
        // Touch Event 등록 후 함수를 연동한다. (getPosByMap)
        let wayPointGesture = UITapGestureRecognizer(target: self, action: #selector(self.wayPointArrivedDidClicked))
        self.waypointArrivedView.addGestureRecognizer(wayPointGesture)
        
        
        let contractGesture = UITapGestureRecognizer(target: self, action: #selector(self.contractComleteDidClicked))
        self.contractCompleteView.addGestureRecognizer(contractGesture)
        
        let secondPickUpGesture = UITapGestureRecognizer(target: self, action: #selector(self.secondPickUpComleteDidClicked))
        self.secondPickupView.addGestureRecognizer(secondPickUpGesture)
        
        let targetArrivedUpGesture = UITapGestureRecognizer(target: self, action: #selector(self.targetArrivedComleteDidClicked))
        self.targetArrivedView.addGestureRecognizer(targetArrivedUpGesture)
        
        let lastMisstionGesture = UITapGestureRecognizer(target: self, action: #selector(self.lastMisstionDidClicked))
        self.lastCheckView.addGestureRecognizer(lastMisstionGesture)
        
        let contractBottomGesture = UITapGestureRecognizer(target: self, action: #selector(self.contractComleteDidClicked))
        self.contractBottomView.addGestureRecognizer(contractBottomGesture)
        
        //initDefaultView()
        
        //request(API_DRIVER_REQUEST_ACTION_REQUEST)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        initDefaultView()
        request(API_DRIVER_REQUEST_ACTION_REQUEST)
    }
    @IBAction func startSendCallDidClicked(_ sender: Any) {
        sendCall(pathInfoManager.start_tel_no)
    }
    
    @IBAction func waypointSendCallDidClicked(_ sender: Any) {
        sendCall(pathInfoManager.waypoint_tel_no)
    }
    
    @IBAction func endSendCallDidClicked(_ sender: Any) {
        sendCall(pathInfoManager.end_tel_no)
    }
    
    //픽업지 도착 View 클릭
    @objc func pickUpArridveDidClicked(_ sender: UIGestureRecognizer){
        initStatusView("1")
        if parentDelegate != nil {
            var contents = ""
            if pathInfoManager.req_gb == "0"{ //세미배차
                contents = "① 발주처에게 탁송을 수행할 드라이버입니다 라고 말씀해주세요.\n ② 발주처에게 임무 내용에 대해 자세히 설명을 듣고 숙지해주세요."
            }else if pathInfoManager.req_gb == "1"{ //풀 배차
                contents = "① 발주처에게 탁송을 수행할 드라이버입니다 라고 말씀해주세요.\n ② 발주처에게 임무 내용에 대해 자세히 설명을 듣고 숙지해주세요."
            }else if pathInfoManager.req_gb == "2"{ //세미 반차
                if pathInfoManager.add_req_car.count == 0 {
                   contents = "픽업지에 도착하시면 \(pathInfoManager.cp_nm) 에서 차량을 찾으러 왔습니다 라고 말씀해주세요"
                }else{
                   contents = "픽업지에 도착하시면 \(pathInfoManager.cp_nm) 에서 차량번호 \(pathInfoManager.add_req_car) 를 찾으러 왔습니다 라고 말씀해주세요"
                }
                
            }else if pathInfoManager.req_gb == "3"{ //풀 반차
                contents = "공업사 직원에게 \(pathInfoManager.cp_nm) 에서\n차량번호 ᄋᄋ가ᄋᄋᄋᄋ(임차인 차번호 노출)  ” 를 찾으러 왔습니다 라고 말씀해주세"
            }
            
            parentDelegate?.showPopupViewFromDelegate("탁송 여정 안내", contents)
        }
    }
    
    //차량 픽업 하기
    @objc func pickUpCarDidClicked(_ sender: UIGestureRecognizer){
       //initStatusView("3")
        println(infoManager.getDriverStatus())
        if parentDelegate != nil {
            parentDelegate?.moveToPageFromDelegate?("CarInfoViewController")
        }
    }
    
    //경유지 도착
    @objc func wayPointArrivedDidClicked(_ sender: UIGestureRecognizer){
        println(infoManager.getDriverStatus())
        if parentDelegate != nil {
            parentDelegate?.moveToPageFromDelegate?("CarInfoViewController")
        }
    }
    
    //계약서 미션 수행 차례
    @objc func contractComleteDidClicked(_ sender: UIGestureRecognizer){
        println(infoManager.getDriverStatus())
        if parentDelegate != nil {
            parentDelegate?.moveToPagePresentFromDelegate?("ContractViewController", reqID: "")
        }
    }
    
    //하단 계약서 미션 수행 차례
    @objc func contractBottomComleteDidClicked(_ sender: UIGestureRecognizer){
        println(infoManager.getDriverStatus())
        if parentDelegate != nil {
            parentDelegate?.moveToPagePresentFromDelegate?("ContractViewController", reqID: "")
        }
    }
    
    //제2차량 픽업 완료
    @objc func secondPickUpComleteDidClicked(_ sender: UIGestureRecognizer){
       if parentDelegate != nil {
           parentDelegate?.moveToPageFromDelegate?("CarInfoViewController")
       }
    }
    
    //목적지 도착 완료
    @objc func targetArrivedComleteDidClicked(_ sender: UIGestureRecognizer){
        if parentDelegate != nil {
            parentDelegate?.moveToPageFromDelegate?("CarInfoViewController")
        }
    }
    
    //마지막 확인
    @objc func lastMisstionDidClicked(_ sender: UIGestureRecognizer){
        if parentDelegate != nil {
            parentDelegate?.moveToPagePresentFromDelegate?("EndMissionViewController", reqID: "")
        }
    }
    
    open func driverArrivedStartPos(){
        request(API_DRIVER_ARRIVED_START_REQUEST)
    }
    
    open func refreshViewStatus(){
        request(API_DRIVER_REQUEST_ACTION_REQUEST)
    }
    
    open func initStatusView(_ status: String){
        initDefaultView()
        
        println(status)
        
        infoManager.setDriverStatus(status: status)
        if status == "0"{ //출발지 도착 시작
            pickUpArrivedCheckBox.isChecked = true
            pickUpArrivedCheckBox.layer.borderWidth = 2
            pickUpArrivedCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            pickUpArrivedView.isUserInteractionEnabled = true
            pickUpArrivedView.layer.borderWidth = 2
            pickUpArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickUpArrivedView.layer.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            
            pickUpArrivedCallBtn.setImage(UIImage(named: "btn_call32_white"), for: .normal)
            
            if parentDelegate != nil {
               parentDelegate?.showPopupViewFromDelegate("확인해주세요.", "① 픽업지 도착 란에 기재된 픽업지로 출발해주세요.\n② 도착 하시면 픽업지 도착 버튼을 눌러주세요.")
            }
        }else if status == "1"{ //출발지 도착 완료
            
            ///픽업지 도착
            pickUpArrivedView.isUserInteractionEnabled = false
            
            pickUpArrivedView.layer.borderWidth = 2
            pickUpArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickUpArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpTitleLabel.text = "픽업지 도착 완료"
            pickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpArrivedCheckBox.isChecked = true
            pickUpArrivedCheckBox.layer.borderWidth = 2
            pickUpArrivedCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            //차량 픽업 하기
            pickUpCompleteCheckBox.isChecked = true
            pickUpCompleteCheckBox.layer.borderWidth = 2
            pickUpCompleteCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            pickupCompleteView.isUserInteractionEnabled = true
            
            pickupCompleteView.layer.borderWidth = 2
            pickupCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickupCompleteView.layer.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickUpCompleteLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            
        }else if status == "2"{ //출발지 인수 완료
            ///픽업지 도착
            pickUpArrivedView.isUserInteractionEnabled = false
            
            pickUpArrivedView.layer.borderWidth = 2
            pickUpArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickUpArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpTitleLabel.text = "픽업지 도착 완료"
            pickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpArrivedCheckBox.isChecked = true
            pickUpArrivedCheckBox.layer.borderWidth = 2
            pickUpArrivedCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            ///차량 픽업하기 View
            pickupCompleteView.isUserInteractionEnabled = false
            
            pickupCompleteView.layer.borderWidth = 2
            pickupCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickupCompleteView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpCompleteLabel.text = "차량 픽업 완료"
            pickUpCompleteLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpCompleteCheckBox.isChecked = true
            pickUpCompleteCheckBox.layer.borderWidth = 2
            pickUpCompleteCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            //경유지 도착 View
            waypointArrivedView.isUserInteractionEnabled = true
            
            wayPointCallBtn.setImage(UIImage(named: "btn_call32_white"), for: .normal)
            waypointArrivedView.layer.borderWidth = 2
            waypointArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            waypointArrivedView.layer.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            
            wayPointCheckBox.isChecked = true
            wayPointCheckBox.layer.borderWidth = 2
            wayPointCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            wayPointTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            wayPointAddrLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            wayPointTimeLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            wayPointRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xFFDB00, alpha: 1)
            
            waypointTimeTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            waypointRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            
            
            if hasWayPoint {
                //경유지 도착으로 이동
                initStatusView("3")
            }else{
                //목적지 도착으로 이동
                initStatusView("6")
            }
        }else if status == "3"{ //경유지 도착
            ///픽업지 도착
            pickUpArrivedView.isUserInteractionEnabled = false
            
            pickUpArrivedView.layer.borderWidth = 2
            pickUpArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickUpArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpTitleLabel.text = "픽업지 도착 완료"
            pickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpArrivedCheckBox.isChecked = true
            pickUpArrivedCheckBox.layer.borderWidth = 2
            pickUpArrivedCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            
            ///차량 픽업하기 View
            pickupCompleteView.isUserInteractionEnabled = false
            
            pickupCompleteView.layer.borderWidth = 2
            pickupCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickupCompleteView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpCompleteLabel.text = "차량 픽업 완료"
            pickUpCompleteLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpCompleteCheckBox.isChecked = true
            pickUpCompleteCheckBox.layer.borderWidth = 2
            pickUpCompleteCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            //경유지 도착 View
            waypointArrivedView.isUserInteractionEnabled = true
            
            wayPointCallBtn.setImage(UIImage(named: "btn_call32_white"), for: .normal)
            waypointArrivedView.layer.borderWidth = 2
            waypointArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            waypointArrivedView.layer.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            
            wayPointCheckBox.isChecked = true
            wayPointCheckBox.layer.borderWidth = 2
            wayPointCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            wayPointTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            wayPointAddrLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            wayPointTimeLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            wayPointRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xFFDB00, alpha: 1)
            
            waypointTimeTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            waypointTimeLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            wayPointTimeLabel.isHidden = false
            wayPointTimeLabel.text = limitTimeInfo
            waypointRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            
        }else if status == "4"{ //계약서 미션 수행
            ///픽업지 도착
            pickUpArrivedView.isUserInteractionEnabled = false
            
            pickUpArrivedView.layer.borderWidth = 2
            pickUpArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickUpArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpTitleLabel.text = "픽업지 도착 완료"
            pickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpArrivedCheckBox.isChecked = true
            pickUpArrivedCheckBox.layer.borderWidth = 2
            pickUpArrivedCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            
            ///차량 픽업하기 View
            pickupCompleteView.isUserInteractionEnabled = true
            
            pickupCompleteView.layer.borderWidth = 2
            pickupCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickupCompleteView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpCompleteLabel.text = "차량 픽업 완료"
            pickUpCompleteLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpCompleteCheckBox.isChecked = true
            pickUpCompleteCheckBox.layer.borderWidth = 2
            pickUpCompleteCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            //경유지 도착 View
            waypointArrivedView.isUserInteractionEnabled = false
            
            wayPointTitleLabel.text = "경유지 도착 완료"
            wayPointCheckBox.isChecked = true
            wayPointCallBtn.setImage(UIImage(named: "btn_call32"), for: .normal)
            waypointArrivedView.layer.borderWidth = 2
            waypointArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            waypointArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            
            wayPointTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            wayPointAddrLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            wayPointTimeLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            wayPointRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            waypointTimeTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            waypointRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            
            //계약서 View
            contractCompleteView.isUserInteractionEnabled = true
            
            contractCompleteView.layer.borderWidth = 2
            contractCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            contractCompleteView.layer.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            
            contractTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            contractRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            contractRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            
            if hasMission {
                initStatusView("5")
            }else if hasBottomMission{
                initStatusView("7")
            }
        }else if status == "5"{ //경유지 출발
           
            ///픽업지 도착
            pickUpArrivedView.isUserInteractionEnabled = false
            
            pickUpArrivedView.layer.borderWidth = 2
            pickUpArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickUpArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpTitleLabel.text = "픽업지 도착 완료"
            pickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpArrivedCheckBox.isChecked = true
            pickUpArrivedCheckBox.layer.borderWidth = 2
            pickUpArrivedCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            
            ///차량 픽업하기 View
            pickupCompleteView.isUserInteractionEnabled = true
            
            pickupCompleteView.layer.borderWidth = 2
            pickupCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickupCompleteView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpCompleteLabel.text = "차량 픽업 완료"
            pickUpCompleteLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpCompleteCheckBox.isChecked = true
            pickUpCompleteCheckBox.layer.borderWidth = 2
            pickUpCompleteCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            //경유지 도착 View
            waypointArrivedView.isUserInteractionEnabled = false
            
            wayPointTitleLabel.text = "경유지 도착 완료"
            wayPointCallBtn.setImage(UIImage(named: "btn_call32"), for: .normal)
            waypointArrivedView.layer.borderWidth = 2
            waypointArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            waypointArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            
            wayPointTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            wayPointAddrLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            wayPointTimeLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            wayPointRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            waypointTimeTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            waypointRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            wayPointCheckBox.isChecked = true
            //계약서 View
            contractCompleteView.isUserInteractionEnabled = false
            
            contractCompleteView.layer.borderWidth = 2
            contractCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            contractCompleteView.layer.backgroundColor = UIColor.clear.cgColor
            
            
            contractImgView.image = UIImage(named: "ico_do_contract_p")
            contractTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            contractRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            contractRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            
            //제2차량 픽업
            secondPickupView.isUserInteractionEnabled = true
            
            secondPickupView.layer.borderWidth = 2
            secondPickupView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            secondPickupView.layer.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            
            secondPickUpCheckBox.isChecked = true
            secondPickUpCheckBox.layer.borderWidth = 2
            secondPickUpCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            secondPickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            secondPickUpContentLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            
        }else if status == "6"{ //도착지 도착
            ///픽업지 도착
            pickUpArrivedView.isUserInteractionEnabled = false
            
            pickUpArrivedView.layer.borderWidth = 2
            pickUpArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickUpArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpTitleLabel.text = "픽업지 도착 완료"
            pickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpArrivedCheckBox.isChecked = true
            pickUpArrivedCheckBox.layer.borderWidth = 2
            pickUpArrivedCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            
            ///차량 픽업하기 View
            pickupCompleteView.isUserInteractionEnabled = false
            
            pickupCompleteView.layer.borderWidth = 2
            pickupCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickupCompleteView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpCompleteLabel.text = "차량 픽업 완료"
            pickUpCompleteLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpCompleteCheckBox.isChecked = true
            pickUpCompleteCheckBox.layer.borderWidth = 2
            pickUpCompleteCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            //경유지 도착 View
            waypointArrivedView.isUserInteractionEnabled = false
            
            wayPointCallBtn.setImage(UIImage(named: "btn_call32"), for: .normal)
            waypointArrivedView.layer.borderWidth = 2
            waypointArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            waypointArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            
            wayPointTitleLabel.text = "경유지 도착 완료"
            wayPointTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            wayPointAddrLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            wayPointTimeLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            wayPointRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            waypointTimeTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            waypointRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            wayPointCheckBox.isChecked = true
            //계약서 View
            contractCompleteView.isUserInteractionEnabled = false
            
            contractCompleteView.layer.borderWidth = 2
            contractCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            contractCompleteView.layer.backgroundColor = UIColor.clear.cgColor
            
            
            contractImgView.image = UIImage(named: "ico_do_contract_p")
            contractTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            contractRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            contractRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            
            //제2차량 픽업
            secondPickupView.isUserInteractionEnabled = false
            
            secondPickUpCheckBox.isChecked = true
            secondPickupView.layer.borderWidth = 2
            secondPickupView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            secondPickupView.layer.backgroundColor = UIColor.clear.cgColor
            
            
            secondPickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            secondPickUpContentLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            
            //목적지 도착
            targetArrivedTitleLabel.text = "목적지 도착"
            targetArrivedView.isUserInteractionEnabled = true
            
            targetArrivedCheckBox.isChecked = true
            targetArrivedCheckBox.layer.borderWidth = 2
            targetArrivedCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            targetArrivedView.layer.borderWidth = 2
            targetArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            targetArrivedView.layer.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            
            targetArrivedTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            targetArrivedAddrLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            targetArrivedTimeLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            targetArrivedRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            
            targetCallBtn.setImage(UIImage(named: "btn_call32_white"), for: .normal)
            targetTimeTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            targetArrivedTimeLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            targetArrivedTimeLabel.text = limitTimeInfo
            targetRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
        }else if status == "7"{ //도착지 도착 완료
            ///픽업지 도착
            pickUpArrivedView.isUserInteractionEnabled = false
            
            pickUpArrivedView.layer.borderWidth = 2
            pickUpArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickUpArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpTitleLabel.text = "픽업지 도착 완료"
            pickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpArrivedCheckBox.isChecked = true
            pickUpArrivedCheckBox.layer.borderWidth = 2
            pickUpArrivedCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            ///차량 픽업하기 View
            pickupCompleteView.isUserInteractionEnabled = true
            
            pickupCompleteView.layer.borderWidth = 2
            pickupCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickupCompleteView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpCompleteLabel.text = "차량 픽업 완료"
            pickUpCompleteLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpCompleteCheckBox.isChecked = true
            pickUpCompleteCheckBox.layer.borderWidth = 2
            pickUpCompleteCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            //경유지 도착 View
            waypointArrivedView.isUserInteractionEnabled = false
            
            wayPointCallBtn.setImage(UIImage(named: "btn_call32"), for: .normal)
            waypointArrivedView.layer.borderWidth = 2
            waypointArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            waypointArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            
            wayPointTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            wayPointAddrLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            wayPointTimeLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            wayPointRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            waypointTimeTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            waypointRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            
            //계약서 View
            contractCompleteView.isUserInteractionEnabled = false
            
            contractCompleteView.layer.borderWidth = 2
            contractCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            contractCompleteView.layer.backgroundColor = UIColor.clear.cgColor
            
            
            contractImgView.image = UIImage(named: "ico_do_contract_p")
            contractTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            contractRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            contractRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            
            //제2차량 픽업
            secondPickupView.isUserInteractionEnabled = false
            
            secondPickUpCheckBox.isChecked = true
            secondPickupView.layer.borderWidth = 2
            secondPickupView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            secondPickupView.layer.backgroundColor = UIColor.clear.cgColor
            
            
            secondPickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            secondPickUpContentLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            
            //목적지 도착
            targetArrivedTitleLabel.text = "목적지 도착 완료"
            targetArrivedView.isUserInteractionEnabled = false
            
            targetArrivedView.layer.borderWidth = 2
            targetArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            targetArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            
            targetArrivedTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            targetArrivedAddrLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            targetArrivedTimeLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            targetArrivedRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            
            targetCallBtn.setImage(UIImage(named: "btn_call32_white"), for: .normal)
            targetArrivedCheckBox.isChecked = true
            targetTimeTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            targetRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            
            //계약서 하단 View
            contractBottomView.isUserInteractionEnabled = false
            
            contractBottomTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            contractBottomView.layer.borderWidth = 2
            contractBottomView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            contractBottomView.layer.backgroundColor = UIColor.clear.cgColor
            
            //마지막 확인하기
            lastCheckView.isUserInteractionEnabled = true
            
            lastCheckView.layer.borderWidth = 2
            lastCheckView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            lastCheckView.layer.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            
            
            lastCheckTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            lastCheckContentLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            
            lastCheckBox.isChecked = true
            lastCheckBox.layer.borderWidth = 2
            lastCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
        }else if status == "8"{ //도착지 완료 후 미션이 있을 경우
            ///픽업지 도착
            pickUpArrivedView.isUserInteractionEnabled = false
            
            pickUpArrivedView.layer.borderWidth = 2
            pickUpArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickUpArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpTitleLabel.text = "픽업지 도착 완료"
            pickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpArrivedCheckBox.isChecked = true
            pickUpArrivedCheckBox.layer.borderWidth = 2
            pickUpArrivedCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            ///차량 픽업하기 View
            pickupCompleteView.isUserInteractionEnabled = true
            
            pickupCompleteView.layer.borderWidth = 2
            pickupCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            pickupCompleteView.layer.backgroundColor = UIColor.clear.cgColor
            pickUpCompleteLabel.text = "차량 픽업 완료"
            pickUpCompleteLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            
            pickUpCompleteCheckBox.isChecked = true
            pickUpCompleteCheckBox.layer.borderWidth = 2
            pickUpCompleteCheckBox.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1).cgColor
            
            //경유지 도착 View
            waypointArrivedView.isUserInteractionEnabled = false
            
            wayPointCallBtn.setImage(UIImage(named: "btn_call32"), for: .normal)
            waypointArrivedView.layer.borderWidth = 2
            waypointArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            waypointArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            
            wayPointTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            wayPointAddrLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            wayPointTimeLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            wayPointRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            waypointTimeTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            waypointRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            
            //계약서 View
            contractCompleteView.isUserInteractionEnabled = false
            
            contractCompleteView.layer.borderWidth = 2
            contractCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            contractCompleteView.layer.backgroundColor = UIColor.clear.cgColor
            
            
            contractImgView.image = UIImage(named: "ico_do_contract_p")
            contractTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            contractRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            contractRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            
            //제2차량 픽업
            secondPickupView.isUserInteractionEnabled = false
            
            secondPickUpCheckBox.isChecked = true
            secondPickupView.layer.borderWidth = 2
            secondPickupView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            secondPickupView.layer.backgroundColor = UIColor.clear.cgColor
            
            
            secondPickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            secondPickUpContentLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            
            //목적지 도착
            targetArrivedTitleLabel.text = "목적지 도착 완료"
            targetArrivedView.isUserInteractionEnabled = false
            
            targetArrivedView.layer.borderWidth = 2
            targetArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            targetArrivedView.layer.backgroundColor = UIColor.clear.cgColor
            
            targetArrivedTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
            targetArrivedAddrLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            targetArrivedTimeLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            targetArrivedRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            
            targetCallBtn.setImage(UIImage(named: "btn_call32_white"), for: .normal)
            targetArrivedCheckBox.isChecked = true
            targetTimeTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            targetRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xc4C515C, alpha: 1)
            
            
            //하단 계약서 View
            //계약서 View
            contractBottomView.isUserInteractionEnabled = true
            
            contractBottomView.layer.borderWidth = 2
            contractBottomView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            contractBottomView.layer.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1).cgColor
            
            contractBottomTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            contractBottomRewardLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
            contractBottomRewardTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0xffffff, alpha: 1)
        }
    }
    
    private func initDefaultView(){
        
        pickUpArrivedView.isUserInteractionEnabled = false
        pickUpArrivedView.layer.borderWidth = 2
        pickUpArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xF4F4F6, alpha: 1).cgColor
        pickUpArrivedView.layer.backgroundColor = UIColor.clear.cgColor
        pickUpArrivedCheckBox.isChecked = false
        
        pickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x99A2B4, alpha: 1)
        pickUpTimedLabel.textColor = UIColor.colorWithRGBHex(hex: 0x99A2B4, alpha: 1)
        pickUpRewardedLabel.textColor = UIColor.colorWithRGBHex(hex: 0x99A2B4, alpha: 1)
        
        pickupCompleteView.isUserInteractionEnabled = false
        pickUpCompleteLabel.text = "차량 픽업 하기"
        pickUpCompleteLabel.textColor = UIColor.colorWithRGBHex(hex: 0x99A2B4, alpha: 1)
        pickupCompleteView.layer.borderWidth = 2
        pickupCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xF4F4F6, alpha: 1).cgColor
        pickupCompleteView.layer.backgroundColor = UIColor.clear.cgColor
        
        waypointArrivedView.isUserInteractionEnabled = false
        wayPointCheckBox.isChecked = false
        wayPointTitleLabel.text = "경유지 도착"
        wayPointTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x99A2B4, alpha: 1)
        waypointArrivedView.layer.borderWidth = 2
        waypointArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xF4F4F6, alpha: 1).cgColor
        waypointArrivedView.layer.backgroundColor = UIColor.clear.cgColor
        
        contractCompleteView.isUserInteractionEnabled = false
        contractTitleLabel.text = "계약서 미션 수행"
        contractTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x99A2B4, alpha: 1)
        contractCompleteView.layer.borderWidth = 2
        contractCompleteView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xF4F4F6, alpha: 1).cgColor
        contractCompleteView.layer.backgroundColor = UIColor.clear.cgColor
        
        secondPickupView.isUserInteractionEnabled = false
        secondPickUpCheckBox.isChecked = false
        secondPickUpTitleLabel.text = "제2차량 픽업"
        secondPickUpTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x99A2B4, alpha: 1)
        secondPickupView.layer.borderWidth = 2
        secondPickupView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xF4F4F6, alpha: 1).cgColor
        secondPickupView.layer.backgroundColor = UIColor.clear.cgColor
        
        targetArrivedView.isUserInteractionEnabled = false
        targetArrivedTitleLabel.text = "목적지 도착"
        targetArrivedTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x99A2B4, alpha: 1)
        targetArrivedView.layer.borderWidth = 2
        targetArrivedView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xF4F4F6, alpha: 1).cgColor
        targetArrivedView.layer.backgroundColor = UIColor.clear.cgColor
        targetArrivedCheckBox.isChecked = false
        
        contractBottomView.isUserInteractionEnabled = false
        contractBottomTitleLabel.text = "계약서 미션 수행"
        contractBottomTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x99A2B4, alpha: 1)
        contractBottomView.layer.borderWidth = 2
        contractBottomView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xF4F4F6, alpha: 1).cgColor
        contractBottomView.layer.backgroundColor = UIColor.clear.cgColor
        
        
        lastCheckView.isUserInteractionEnabled = false
        lastCheckTitleLabel.text = "마지막 확인하기"
        lastCheckTitleLabel.textColor = UIColor.colorWithRGBHex(hex: 0x99A2B4, alpha: 1)
        lastCheckView.layer.borderWidth = 2
        lastCheckView.layer.borderColor = UIColor.colorWithRGBHex(hex: 0xF4F4F6, alpha: 1).cgColor
        lastCheckView.layer.backgroundColor = UIColor.clear.cgColor
        lastCheckBox.isChecked = false
    }
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 5 || indexPath.row == 7 {
            print(hasWayPoint)
            return !hasWayPoint  ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        }else if indexPath.row ==  6 {
            return !hasMission  ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        }else if indexPath.row ==  9 {
            return !hasBottomMission  ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        }else{
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 12
    }
    
    //MARK: - network
    override func request(_ name: String) {
        let infoManager = InfoManager.getInstance()
        switch name {
        case API_DRIVER_ARRIVED_START_REQUEST:
            let apiToken : String  = infoManager.getApiToken()
            let reqID : String = infoManager.getReqID()
            
            let params: Parameters = [
                "api_token" : apiToken
                ,"req_id" : reqID
            ]
            _ = NetworkManager.sharedInstance.request(API_DRIVER_ARRIVED_START_REQUEST, params, HTTPMethod.put, target: self)
            
            break
        case API_DRIVER_REQUEST_ACTION_REQUEST://탁송 드라이버 요청 건 검색
            //let extraParam : String = "/" + infoManager.getReqID() + "?api_token="+infoManager.getApiToken()
            let extraParam : String = "/" + infoManager.getReqID() + "?api_token="+infoManager.getApiToken()
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        
        switch requestID {
        case API_DRIVER_ARRIVED_START_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
            }
            
            break
        case API_DRIVER_REQUEST_ACTION_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                pathInfoManager.start_car_no =  json["start_point_car_info"]["car_no"].stringValue
                pathInfoManager.start_car_model =  json["start_point_car_info"]["model"].stringValue
                pathInfoManager.start_car_photo =  json["start_point_car_info"]["car_photo"].stringValue
                pathInfoManager.start_car_real_match =  json["start_point_car_info"]["real_match"].stringValue
                
                pathInfoManager.waypoint_car_no =  json["waypoint_car_info"]["car_no"].stringValue
                pathInfoManager.waypoint_car_model =  json["waypoint_car_info"]["model"].stringValue
                pathInfoManager.waypoint_car_photo =  json["waypoint_car_info"]["car_photo"].stringValue
                pathInfoManager.waypoint_car_real_match =  json["waypoint_car_info"]["real_match"].stringValue
                
                
                pathInfoManager.start_tel_no =  json["start_point_info"]["tel_no"].stringValue
                pathInfoManager.start_address =  json["start_point_info"]["address"].stringValue
                pathInfoManager.start_la =  json["start_point_info"]["la"].stringValue
                pathInfoManager.start_lo =  json["start_point_info"]["lo"].stringValue
                
                pathInfoManager.waypoint_tel_no =  json["waypoint_info"]["tel_no"].stringValue
                pathInfoManager.waypoint_address =  json["waypoint_info"]["address"].stringValue
                pathInfoManager.waypoint_la =  json["waypoint_info"]["la"].stringValue
                pathInfoManager.waypoint_lo =  json["waypoint_info"]["lo"].stringValue
                
                //경유지가 없는 경우
                if pathInfoManager.waypoint_address.count == 0 {
                }
                
                hasWayPoint = pathInfoManager.waypoint_address.count == 0 ? false : true
                
                pathInfoManager.end_tel_no =  json["end_point_info"]["tel_no"].stringValue
                pathInfoManager.end_address =  json["end_point_info"]["address"].stringValue
                pathInfoManager.end_la =  json["end_point_info"]["la"].stringValue
                pathInfoManager.end_lo =  json["end_point_info"]["lo"].stringValue
                
                targetArrivedAddrLabel.text = pathInfoManager.end_address
                
                pathInfoManager.cp_nm = json["cp_nm"].stringValue
                cpNameLabel.text = pathInfoManager.cp_nm
                pathInfoManager.emp_nm = json["emp_nm"].stringValue
                empNameLabel.text = pathInfoManager.emp_nm
                
                pathInfoManager.insr_nm = json["insr_info"]["insr_nm"].stringValue
                pathInfoManager.insr_no = json["insr_info"]["insr_no"].stringValue
                
                pathInfoManager.center_gb = json["center_gb"].stringValue
                pathInfoManager.mission_id = json["mission_id"].stringValue
                
                if pathInfoManager.mission_id.count == 0 {
                    //contractCompleteView.isHidden = true
                }
                
                if hasWayPoint {
                    hasBottomMission = false
                    hasMission = pathInfoManager.mission_id.count == 0 ? false : true
                }else{
                    hasBottomMission = pathInfoManager.mission_id.count == 0 ? false : true
                }
                
                
                pathInfoManager.add_req_car = json["add_req_car"].stringValue
                pathInfoManager.action_gb = json["action_gb"].stringValue
                
                println(pathInfoManager.action_gb)
                
                if pathInfoManager.action_gb == "3"{ //경유지 도착 후 사진 업로드 완료
                    if hasMission {
                        pathInfoManager.action_gb = "4"
                    }else {
                        pathInfoManager.action_gb = "5"
                    }
                }else if pathInfoManager.action_gb == "4"{ //계약서 미션 사진 업로드 후 완료
                    if hasMission{
                        pathInfoManager.action_gb = "5"
                    }
                }else if pathInfoManager.action_gb == "5"{ //제2차량 픽업 완료하여 경유지 출발
                    pathInfoManager.action_gb = "6"
                }else if pathInfoManager.action_gb_by_app == "7" && pathInfoManager.action_gb == "6"{
                    if hasBottomMission {
                        //도착지 도착 후 사진까지 업로드 완료 후 미션이 있을 경우
                        pathInfoManager.action_gb = "8"
                    }else{
                        //도착지 도착 후 사진까지 업로드 완료
                        pathInfoManager.action_gb = "7"
                    }
                    
                }else if pathInfoManager.action_gb_by_app == "8" && pathInfoManager.action_gb == "6"{
                    //도착지 도착 후 계약서 미션 완료 후
                    pathInfoManager.action_gb = "7"
                }
                
                pathInfoManager.add_req = json["add_req"].stringValue
                pathInfoManager.req_gb = json["req_gb"].stringValue
                
                pathInfoManager.limit_time = json["limit_time"].stringValue
                
                add_req_car.text = json["add_req_car"].stringValue
                add_req.text = json["add_req"].stringValue
                
                if pathInfoManager.limit_time.count > 0{
                    limitTimeInfo = String.init(format: "%d분",  Int(pathInfoManager.limit_time)!/60)
                }else{
                    limitTimeInfo = String.init(format: "%d분",  0)
                }
                
                self.tableView.reloadData()
                initStatusView(pathInfoManager.action_gb)
            }
            break
            
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
    }
    
    func sendCall(_ number: String){
        if let phoneCallURL = URL(string: "tel://\(number)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
            
        }
    }
}
