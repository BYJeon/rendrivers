//
//  EndMissionViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 04/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

class EndMissionViewController: BaseViewController {
    @IBOutlet var popupView: UIView!
    
    @IBOutlet var popupTitleLabel: UILabel!
    @IBOutlet var popupContentLabel: UITextView!
    private var childTableView: EndMissionTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Touch Event 등록 후 함수를 연동한다. (getPosByMap)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dimDidClicked(_:)))
        self.dimView!.addGestureRecognizer(gesture)
        
        childTableView = (self.children[0] as! EndMissionTableViewController)
        childTableView.parentDelegate = self
    }
    
    @IBAction func backDidClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func showPopupView(){
        self.view.bringSubviewToFront(popupView)
        popupView.isHidden = false
        dimView!.isHidden = false
    }
    
    private func hidePopupView(){
        popupView.isHidden = true
        dimView!.isHidden = true
    }
    
    @IBAction func popUpConfirmDidClicked(_ sender: Any) {
        hidePopupView()
    }
    
    //DimView Clicked
    @objc func dimDidClicked(_ sender: UIGestureRecognizer){
        hidePopupView()
    }
    
    /// Delegate
    override func showPopupViewFromDelegate(_ title: String, _ content: String) {
        self.view.endEditing(true)
        popupTitleLabel.text = title
        popupContentLabel.text = content
        popupContentLabel.textColor = UIColorFromRGBA(0x78849E, alpha: 1)
        showPopupView()
    }
    
    override func showLoadingProgressBarFromDelegate(_ show: Bool) {
        self.showLoadingProgressBar(isShow: show)
    }
    
    override func moveToPagePresentFromDelegate(_ id: String, reqID: String) {
//        if mainOperationDelegate != nil {
//            UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: false, completion: nil)
//            mainOperationDelegate?.moveToPageByPresent(id: id)
//        }
        
        self.navigationController?.popViewController(animated: false)
        //        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseViewController
        //        getTopMostViewController()!.present(pathVC, animated: true, completion: nil)
        //self.navigationController?.popToRootViewController(animated: true)
        
        //self.dismiss(animated: true, completion: {});
        self.navigationController?.popViewController(animated: true);
        
        
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseViewController
        getTopMostViewController()!.present(pathVC, animated: true, completion: nil)
    }
    
    func getTopMostViewController() -> UIViewController? {
        var topMostViewController = UIApplication.shared.keyWindow?.rootViewController
        
        while let presentedViewController = topMostViewController?.presentedViewController {
            topMostViewController = presentedViewController
        }
        
        return topMostViewController
    }
}
