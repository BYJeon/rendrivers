//
//  SummaryMissionViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 04/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

class SummaryMissionViewController: BaseViewController {
    private var childTableView: SummaryMissionTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        childTableView = (self.children[0] as! SummaryMissionTableViewController)
        childTableView.parentDelegate = self
    }
    
    override func showLoadingProgressBarFromDelegate(_ show: Bool) {
        self.showLoadingProgressBar(isShow: show)
    }
    
    override func moveToPagePresentFromDelegate(_ id: String, reqID: String) {
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: false, completion: nil)
        self.navigationController?.popViewController(animated: false)
//        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseViewController
//        getTopMostViewController()!.present(pathVC, animated: true, completion: nil)
//        self.navigationController?.popToRootViewController(animated: true)
        
//        self.dismiss(animated: true, completion: {});
//        self.navigationController?.popViewController(animated: true);
    }
    
    func getTopMostViewController() -> UIViewController? {
        var topMostViewController = UIApplication.shared.keyWindow?.rootViewController
        
        while let presentedViewController = topMostViewController?.presentedViewController {
            topMostViewController = presentedViewController
        }
        
        return topMostViewController
    }
}
