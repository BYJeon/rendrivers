//
//  RequestStatusViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 01/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class RequestStatusViewController: BaseViewController {
    @IBOutlet var popupView: UIView!
    
    @IBOutlet var popupTitleLabel: UILabel!
    @IBOutlet var popupContentLabel: UITextView!
    
    private let locationManager = CLLocationManager()
    private let pathInfoManager : PathInfoManager = PathInfoManager.sharedInstance
    private let infoManager = InfoManager.getInstance()
    
    private var childTableView: RequestStatusTableViewController!
    
    private var curStatus : Int = 0
    
    open var cpTelNo:String = ""
    
    //timer
    private var mTimer : Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Touch Event 등록 후 함수를 연동한다. (getPosByMap)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dimDidClicked(_:)))
        self.dimView!.addGestureRecognizer(gesture)
        
        dimView!.frame = CGRect(x: 0, y: 0, width: getDeviceWidth(), height: getDeviceHeight())
        childTableView = (self.children[0] as! RequestStatusTableViewController)
        childTableView.parentDelegate = self
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization() //권한 요청
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        if let timer = mTimer {
            //timer 객체가 nil 이 아닌경우에는 invalid 상태에만 시작한다
            if !timer.isValid {
                /** 1초마다 timerCallback함수를 호출하는 타이머 */
                mTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
            }
        }else{
            //timer 객체가 nil 인 경우에 객체를 생성하고 타이머를 시작한다
            /** 1초마다 timerCallback함수를 호출하는 타이머 */
            mTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
        }
        
    }

    override func viewDidDisappear(_ animated: Bool) {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    override func viewDidAppear(_ animated: Bool) {
        let driverStatus = infoManager.getDriverStatus()
        if driverStatus == "-1" { //임무 완료
            navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            navigationController?.popViewController(animated: true)
            return
        }
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        childTableView.initStatusView(infoManager.getDriverStatus())
    }
    
    //타이머가 호출하는 콜백함수
    @objc func timerCallback(){
        request(API_DRIVER_MISSION_LOCATION_UPLOAD)
    }
    
    
    private func showPopupView(){
        self.view.bringSubviewToFront(popupView)
        popupView.isHidden = false
        dimView!.isHidden = false
    }
    private func hidePopupView(){
        popupView.isHidden = true
        dimView!.isHidden = true
        
        print(infoManager.getDriverStatus())
        //픽업지 도착 뷰 클릭했을 경우
        if infoManager.getDriverStatus() == "1" {
            
            childTableView.driverArrivedStartPos()
        }
    }
    
    //CS 문의하기
    @IBAction func callCenterDidClicked(_ sender: Any) {
        sendCall(CS_NUMBER)
    }
    
    //refresh
    @IBAction func refreshDidClicked(_ sender: Any) {

        childTableView.refreshViewStatus()
        //curStatus = (curStatus + 8 + 1) % 8
        //childTableView.initStatusView(String(curStatus))
    }
    
    @IBAction func popUpConfirmDidClicked(_ sender: Any) {
        hidePopupView()
    }
    
    //DimView Clicked
    @objc func dimDidClicked(_ sender: UIGestureRecognizer){
        hidePopupView()
        
    }
    
    /// Delegate
    override func showPopupViewFromDelegate(_ title: String, _ content: String) {
        self.view.endEditing(true)
        popupTitleLabel.text = title
        popupContentLabel.text = content
        popupContentLabel.textColor = UIColorFromRGBA(0x78849E, alpha: 1)
        showPopupView()
    }
    

    override func moveToPageFromDelegate(_ id: String) {
        if mainOperationDelegate != nil {
            mainOperationDelegate?.moveToPage(id: id)
        }
    }
    
    override func moveToPagePresentFromDelegate(_ id: String, reqID: String) {
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseViewController
        self.present(pathVC, animated: true, completion: nil)
    }
    
    
    //MARK: - network
    override func request(_ name: String) {
        let infoManager = InfoManager.getInstance()
        switch name {
            
        case API_DRIVER_MISSION_LOCATION_UPLOAD:
            let la : Double = (pathInfoManager.myLocationInfo?.coordinate.latitude)!
            let lo : Double = (pathInfoManager.myLocationInfo?.coordinate.longitude)!
            let req_id = infoManager.getReqID()
            let driver_id : String = infoManager.appID!
            let token : String  = infoManager.getApiToken()
            
            let params: Parameters = [
                "la" : String(format: "%f", la)
                ,"lo" : String(format: "%f", lo)
                ,"req_id" : req_id
                ,"driver_id" : driver_id
                ,"api_token" : token
            ]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.post, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        
        switch requestID {
        case API_DRIVER_LOCATION_UPLOAD:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
            }
            break
            
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
    }
    
}

extension RequestStatusViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let firstLocation = locations.first else {
            return
        }
        
        if pathInfoManager.myLocationInfo == nil {
            pathInfoManager.myLocationInfo = firstLocation
            println(pathInfoManager.myLocationInfo?.coordinate.latitude)
            println(pathInfoManager.myLocationInfo?.coordinate.longitude)
            println("*****************************************************************")
        }
    }
}
