//
//  CarInfoViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 01/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

class CarInfoViewController: BaseViewController {
    @IBOutlet var popupView: UIView!
    
    @IBOutlet var popupTitleLabel: UILabel!
    @IBOutlet var popupContentLabel: UITextView!
    
    private var childTableView: CarInfoTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Touch Event 등록 후 함수를 연동한다. (getPosByMap)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dimDidClicked(_:)))
        self.dimView!.addGestureRecognizer(gesture)
        
        childTableView = (self.children[0] as! CarInfoTableViewController)
        childTableView.parentDelegate = self
    }
    
    
    private func showPopupView(){
        self.view.bringSubviewToFront(popupView)
        popupView.isHidden = false
        dimView!.isHidden = false
    }
    
    private func hidePopupView(){
        popupView.isHidden = true
        dimView!.isHidden = true
    }
    
    @IBAction func backDidClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func popUpConfirmDidClicked(_ sender: Any) {
        hidePopupView()
    }
    
    //DimView Clicked
    @objc func dimDidClicked(_ sender: UIGestureRecognizer){
        hidePopupView()
    }
    
    /// Delegate
    override func showPopupViewFromDelegate(_ title: String, _ content: String) {
        self.view.endEditing(true)
        popupTitleLabel.text = title
        popupContentLabel.text = content
        popupContentLabel.textColor = UIColorFromRGBA(0x78849E, alpha: 1)
        showPopupView()
    }
    
    override func showLoadingProgressBarFromDelegate(_ show: Bool) {
       self.showLoadingProgressBar(isShow: show)
    }
}
