//
//  SummaryMissionTableViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 04/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class SummaryMissionTableViewController: BaseTableViewController {
    
    @IBOutlet var telNoLabel: UILabel!
    @IBOutlet var cpNmLabel: UILabel!
    
    @IBOutlet var pathInfoView: UIView!
    @IBOutlet var pathViaView: UIView!
    
    //경유지가 없는 경로
    @IBOutlet var targetPath: UITextView!
    @IBOutlet var sourcePath: UITextView!
    @IBOutlet var missionView: UIView!
    @IBOutlet var missionCompleteTimeLabel: UILabel!
    
    @IBOutlet var sourceTimeLabel: UILabel!
    @IBOutlet var targetTimeLabel: UILabel!
    
    //경유지가 있는 경로
    @IBOutlet var targetViaPath: UITextView!
    @IBOutlet var viaPath: UITextView!
    @IBOutlet var sourceViaPath: UITextView!
    
    @IBOutlet var missionViaView: UIView!
    @IBOutlet var missionViaCompleteTimeLabel: UILabel!
    
    @IBOutlet var sourceViaTimeLabel: UILabel!
    @IBOutlet var viaTimeLabel: UILabel!
    @IBOutlet var targetViaTimeLabel: UILabel!
    
    @IBOutlet var serviceFeeLabel: UILabel!
    @IBOutlet var totalFeeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if parentDelegate != nil {
            parentDelegate?.showLoadingProgressBarFromDelegate?(true)
        }
        
        missionView.isHidden = InfoManager.getInstance().mission_id.count == 0
        missionViaView.isHidden = InfoManager.getInstance().mission_id.count == 0
        
        request(API_DRIVER_MISSION_END_REQUEST)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 7
    }
    
    @IBAction func closeDidClicked(_ sender: Any) {
        if parentDelegate != nil {
            self.navigationController?.popViewController(animated: false)
            parentDelegate?.moveToPagePresentFromDelegate?("driverMainViewController", reqID: "")
        }
    }
    @IBAction func confirmDidClicked(_ sender: Any) {
        if parentDelegate != nil {
            InfoManager.getInstance().setDriverStatus(status: "-1")
            self.navigationController?.popViewController(animated: false)
            parentDelegate?.moveToPagePresentFromDelegate?("driverMainViewController", reqID: "")
        }
    }
    //MARK: - network
    override func request(_ name: String) {
        let infoManager = InfoManager.getInstance()
        switch name {

        case API_DRIVER_MISSION_END_REQUEST://탁송 드라이버 요청 건 검색
            //let extraParam : String = "/" + infoManager.getReqID() + "?api_token="+infoManager.getApiToken()
            let extraParam : String = "/" + infoManager.getReqID() + "?api_token="+infoManager.getApiToken()
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        if parentDelegate != nil {
            parentDelegate?.showLoadingProgressBarFromDelegate?(false)
        }
        
        switch requestID {
        case API_DRIVER_MISSION_END_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                let start_tel_no  =  json["start_point_info"]["tel_no"].stringValue
                let start_address =  json["start_point_info"]["address"].stringValue
                let start_arrived_time =  json["start_point_info"]["arrive_time"].stringValue
                
                let waypoint_tel_no  =  json["waypoint_info"]["tel_no"].stringValue
                let waypoint_address =  json["waypoint_info"]["address"].stringValue
                let waypoint_arrived_time =  json["waypoint_info"]["arrive_time"].stringValue
               
                let end_tel_no  =  json["end_point_info"]["tel_no"].stringValue
                let end_address =  json["end_point_info"]["address"].stringValue
                let end_arrived_time =  json["end_point_info"]["arrive_time"].stringValue
                
                let mission_nm = json["mission_info"][0]["mission_nm"].stringValue
                let mission_complete_time = json["mission_info"][0]["completed_time"].stringValue
                
                let service_info = json["fee_info"]["service_info"].stringValue
                let total_dc = json["fee_info"]["total_dc"].stringValue
                let charge_fee = json["fee_info"]["charge_fee"].stringValue
                
                let emp_nm = json["emp_nm"].stringValue
                let emp_company = json["emp_company"].stringValue
                let emp_id = json["emp_id"].stringValue
                 
                cpNmLabel.text = "발주처 : " + emp_company
                //경유지가 있을 경우
                if waypoint_address.count != 0 && waypoint_arrived_time.count != 0 {
                    pathInfoView.isHidden = true
                    pathViaView.isHidden = false
                    
                    sourceViaPath.text = start_address
                    sourceViaTimeLabel.text = start_arrived_time
                    
                    viaPath.text = waypoint_address
                    viaTimeLabel.text = waypoint_arrived_time
                    
                    targetViaPath.text = end_address
                    targetViaTimeLabel.text = end_arrived_time
                    
                    if mission_nm != nil && mission_nm.count != 0 {
                        missionViaView.isHidden = false
                        missionViaCompleteTimeLabel.text = mission_complete_time
                    }else{
                        missionViaView.isHidden = true
                    }
                    
                }else{
                    pathInfoView.isHidden = false
                    pathViaView.isHidden = true
                    
                    sourcePath.text = start_address
                    sourceTimeLabel.text = start_arrived_time
                    
                    targetPath.text = end_address
                    targetTimeLabel.text = end_arrived_time
                    
                    if mission_nm != nil && mission_nm.count != 0 {
                        missionView.isHidden = false
                        missionCompleteTimeLabel.text = mission_complete_time
                    }else{
                        missionView.isHidden = true
                    }
                }
                
                serviceFeeLabel.text = charge_fee.getCurrencyString() + "원"
                totalFeeLabel.text = charge_fee.getCurrencyString() + "원"
            }
            break
            
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        if parentDelegate != nil {
            parentDelegate?.showLoadingProgressBarFromDelegate?(false)
        }
        println(">>> requestID = \(requestID)")
        
    }

}
