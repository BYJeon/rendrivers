//
//  EndMissionTableViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 04/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class EndMissionTableViewController: BaseTableViewController {

    
    @IBOutlet var addrTitleLabel: UILabel!
    @IBOutlet var addrSubTitleLabel: UILabel!
    
    @IBOutlet var rewardLabel: UILabel!
    @IBOutlet var agreeCheckBox: CheckBox!
    
    @IBOutlet var insranceNmLabel: UILabel!
    @IBOutlet var insranceNumLabel: UILabel!
    
    private let infoManager = InfoManager.getInstance()
    private let pathInfoManager = PathInfoManager.sharedInstance
    
    @IBOutlet var endMissionDesc1: UITextView!
    @IBOutlet var endMissionDesc2: UITextView!
    @IBOutlet var endMissionDesc3: UITextView!
    @IBOutlet var endMissionDesc4: UITextView!
    @IBOutlet var endMissionDesc5: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        request(API_DRIVER_REQUEST)
        //addrTitleLabel.text = pathInfoManager.end_address
        //rewardLabel.text = infoManager.expect_amt.getCurrencyString() + "원"
        
        insranceNmLabel.text = pathInfoManager.insr_nm
        insranceNumLabel.text = pathInfoManager.insr_no
        
        endMissionDesc1.text = "도착지 주차장에 제대로 주차 후 시동OFF 하셨나요."
        endMissionDesc1.textColor = UIColorFromRGBA(0x393939, alpha: 1)
        
        endMissionDesc2.text = "공업사에 입고할 때 참고하세요."
        endMissionDesc2.textColor =  UIColorFromRGBA(0x393939, alpha: 1)
        
        endMissionDesc3.text = "임무수행간 발생한 과태료, 통행료는 드라이버의 책임이며 추후에라도 책임지고 지불해야 합니다."
        endMissionDesc3.textColor =  UIColorFromRGBA(0x393939, alpha: 1)
        
        endMissionDesc4.text = "차량이 지정된 장소에 없거나, 임무수행이 적절히 이뤄지지 않은 경우 패널티와 함께 견인비, 탁송비, 주차비, 과태료 등 실비가 부과될 수 있습니다."
        endMissionDesc4.textColor =  UIColorFromRGBA(0x393939, alpha: 1)
        
        endMissionDesc5.text = "탁송요금을 발주처로부터 직접 수취하세요."
        endMissionDesc5.textColor =  UIColorFromRGBA(0x393939, alpha: 1)
    }

    
    @IBAction func confirmDidClicked(_ sender: Any) {
        if !agreeCheckBox.isChecked && parentDelegate != nil {
            parentDelegate?.showPopupViewFromDelegate("임무수행 완료하기","주의사항 확인 여부를 체크해 주세요.")
            return
        }

        if parentDelegate != nil {
            parentDelegate?.showLoadingProgressBarFromDelegate?(true)
        }
        request(API_DRIVER_MISSION_END_REQUEST)
        
//        if parentDelegate != nil {
//            parentDelegate?.moveToPagePresentFromDelegate?("SummaryMissionViewController", reqID: "")
//        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 6
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2 {
            let  bCenterInfo = pathInfoManager.insr_nm.count != 0 && pathInfoManager.insr_no.count != 0
            return !bCenterInfo  ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
        }else{
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    @IBAction func closeDidClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - network
    override func request(_ name: String) {
        if parentDelegate != nil {
            parentDelegate?.showLoadingProgressBarFromDelegate?(true)
        }
        
        switch name {
        case API_DRIVER_REQUEST://완료 된 요청 건 검색
            //let extraParam : String = "/" + infoManager.getReqID() + "?api_token="+infoManager.getApiToken()
            let extraParam : String = "/" + infoManager.getReqID() + "?api_token="+infoManager.getApiToken()
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
        case API_DRIVER_MISSION_END_REQUEST:
            let apiToken : String  = infoManager.getApiToken()
            let req_id : String  = infoManager.getReqID()
            
            let params: Parameters = [
                "api_token" : apiToken
                ,"req_id" : req_id
                
            ]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.put, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        if parentDelegate != nil {
            parentDelegate?.showLoadingProgressBarFromDelegate?(false)
        }
        
        switch requestID {
        case API_DRIVER_REQUEST:
            
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                let expect_amt = json["expect_amt"].stringValue
                let way_point_addr   = json["addresses"]["waypoint"]["address"].stringValue
                let start_point_addr = json["addresses"]["start_point"]["address"].stringValue
                let end_point_addr   = json["addresses"]["end_point"]["address"].stringValue
                
                addrTitleLabel.text = end_point_addr
                if expect_amt.count != 0{
                    rewardLabel.text = expect_amt.getCurrencyString() + "원"
                }else{
                    rewardLabel.text = "0원"
                }
                
                //insranceNmLabel.text = pathInfoManager.insr_nm
                //insranceNumLabel.text = pathInfoManager.insr_no
            }
            break
            
        case API_DRIVER_MISSION_END_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                if parentDelegate != nil {
                    parentDelegate?.moveToPagePresentFromDelegate?("SummaryMissionViewController", reqID: "")
                }
            }
            break
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        if parentDelegate != nil {
            parentDelegate?.showLoadingProgressBarFromDelegate?(false)
        }
        
        let json = JSON(data!)
        
        let dialog = UIAlertController(title: "통신 오류", message: "오류 번호 : " + json["error"].stringValue, preferredStyle: .alert)
        let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
        dialog.addAction(action)
        self.present(dialog, animated: true, completion: nil)
    }
}
