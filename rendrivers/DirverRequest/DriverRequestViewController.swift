//
//  DriverRequestViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 01/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DriverRequestViewController: BaseViewController {
    private let infoManager : InfoManager = InfoManager.getInstance()
    
    @IBOutlet var pathInfoView: UIView!
    @IBOutlet var pathViaView: UIView!
   
    @IBOutlet var sourcePath: UITextView!
    @IBOutlet var targetPath: UITextView!
   
    @IBOutlet var sourceViaPath: UITextView!
    @IBOutlet var targetViaPath: UITextView!
    @IBOutlet var viaPath: UITextView!
    
    @IBOutlet var confirmBtn: RoundButton!
    
    @IBOutlet var expectAmountLabel: UILabel!
    
    @IBOutlet var confirmWaitBtn: UIButton!
    
    private var confirmStatus = "2" // 1: 수락 , 2:거절
    
    private var curReqState = ""
    
    //timer
    private var mTimer : Timer?
    private var remainTime = 30
    
    override func viewDidLoad() {
        super.viewDidLoad()
        request(API_DRIVER_REQUEST)
        curReqState = API_DRIVER_REQUEST
        
        //탁송 요청 수락 완료
        NotificationCenter.default.addObserver(self, selector: #selector(driverRequestConfirmed), name: NSNotification.Name(rawValue: NOTI_DRIVER_REQUEST_START), object: nil)
        //탁송 요청 수락 취소
        NotificationCenter.default.addObserver(self, selector: #selector(driverRequestCalceled), name: NSNotification.Name(rawValue: NOTI_DRIVER_REQUEST_CANCEL), object: nil)
    }
    
    
    //탁송 요청 수락  Noti
    @objc func driverRequestConfirmed() {
        
        mTimer?.invalidate()
        
        if mainOperationDelegate != nil {
            mainOperationDelegate?.moveToPage(id: "RequestStatusViewController")
            UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: false, completion: nil)
        }
    }
    
    //탁송 요청 수락 취소  Noti
    @objc func driverRequestCalceled() {
        mTimer?.invalidate()
        infoManager.setMenuType(type: MenuType.DRIVER_CANCEL)
        self.dismiss(animated: true, completion: nil)
    }
    
    //거절하기
    @IBAction func rejectDidClicked(_ sender: Any) {
        confirmStatus = "2"
        request(API_DRIVER_DECIDE_REQUEST)
        curReqState = API_DRIVER_DECIDE_REQUEST
        infoManager.setMenuType(type: MenuType.DRIVER_CANCEL)
    }
    
    //임수 수행하기
    @IBAction func confirmMissionDidClicked(_ sender: Any) {
        confirmStatus = "1"
        request(API_DRIVER_DECIDE_REQUEST)
        curReqState = API_DRIVER_DECIDE_REQUEST
    }
    
    
    private func startTimer(){
        confirmBtn.setTitle("임무수행", for: .normal)
        
        remainTime = 30
        if let timer = mTimer {
            //timer 객체가 nil 이 아닌경우에는 invalid 상태에만 시작한다
            if !timer.isValid {
                /** 1초마다 timerCallback함수를 호출하는 타이머 */
                mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
            }
        }else{
            //timer 객체가 nil 인 경우에 객체를 생성하고 타이머를 시작한다
            /** 1초마다 timerCallback함수를 호출하는 타이머 */
            mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
        }
    }
    
    //타이머가 호출하는 콜백함수
    @objc func timerCallback(){
        if remainTime > 0 {
            remainTime -= 1
            
            UIView.setAnimationsEnabled(false)
            confirmBtn.setTitle(String(format: "임무수행 %d", remainTime%60), for: .normal)
            confirmBtn.setNeedsLayout()
            confirmBtn.layoutIfNeeded()
            UIView.setAnimationsEnabled(true)
            
        }else{
            //secureTimeLabel.isHidden = true
            mTimer?.invalidate()
            
            //시간 지나면 자동 거절
//            curReqState = API_DRIVER_DECIDE_REQUEST
//
//            confirmStatus = "2"
//            showLoadingProgressBar(isShow: false)
//            if mainOperationDelegate != nil {
//                self.dismiss(animated: true, completion: nil)
//                return
//            }
        }
        
    }
    
    //MARK: - network
    override func request(_ name: String) {
        showLoadingProgressBar(isShow: true)
        switch name {
        case API_DRIVER_DECIDE_REQUEST:
            let apiToken : String  = infoManager.getApiToken()
            let driverID : String  = infoManager.getAppID()
            let reqID : String = infoManager.getReqID()
            
            let params: Parameters = [
                "api_token" : apiToken
                ,"req_id" : reqID
                ,"confirm_state" : confirmStatus
                ,"driver_id" : driverID
                
            ]
            _ = NetworkManager.sharedInstance.request(API_DRIVER_REQUEST, params, HTTPMethod.put, target: self)
            break
            
        case API_DRIVER_REQUEST://탁송 드라이버 요청 건 검색
            let extraParam : String = "/" + infoManager.getReqID() + "?api_token="+infoManager.getApiToken()
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        showLoadingProgressBar(isShow: false)
        switch requestID {
        case API_DRIVER_DECIDE_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                if confirmStatus == "2" {
                    if mainOperationDelegate != nil {
                        self.dismiss(animated: true, completion: nil)
                        //mainOperationDelegate?.moveToPageByPresent(id: "driverMainViewController")
                        return
                    }
                }
            }else{
                showLoadingProgressBar(isShow: false)
                if mainOperationDelegate != nil {
                    self.dismiss(animated: true, completion: nil)
                    //mainOperationDelegate?.moveToPageByPresent(id: "driverMainViewController")
                    return
                }
            }
            break
            
        case API_DRIVER_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                if curReqState == API_DRIVER_DECIDE_REQUEST && confirmStatus == "1" { //탁송 요청 수락
                    confirmWaitBtn.isHidden = false
                    return
                }else if curReqState == API_DRIVER_DECIDE_REQUEST && confirmStatus == "2" { //탁송 요청 거절
                    if mainOperationDelegate != nil {
                        self.dismiss(animated: true, completion: nil)
                        mainOperationDelegate?.moveToPageByPresent(id: "driverMainViewController")
                    }
                    return
                }
                
                let way_point_addr   = json["addresses"]["waypoint"]["address"].stringValue
                let start_point_addr = json["addresses"]["start_point"]["address"].stringValue
                let end_point_addr   = json["addresses"]["end_point"]["address"].stringValue
                
//                let way_point_time = json["waypoint_info"]["arrive_time"].stringValue
//                let start_point_time = json["start_point_info"]["arrive_time"].stringValue
//                let end_point_time = json["end_point_info"]["arrive_time"].stringValue
                
                
                infoManager.expect_amt = json["expect_amt"].stringValue
                
                expectAmountLabel.text = infoManager.expect_amt.getCurrencyString() + " 원"
                infoManager.mission_id = json["mission"]["mission_id"].stringValue
                infoManager.cost = json["mission"]["cost"].stringValue
                infoManager.cautions = json["cautions"]
                
                if way_point_addr.count != 0 {
                    pathViaView.isHidden = false
                    pathInfoView.isHidden = true
                    targetViaPath.text = end_point_addr
                    sourceViaPath.text = start_point_addr
                    viaPath.text = way_point_addr
                    
//
                }else{
                    pathViaView.isHidden = true
                    pathInfoView.isHidden = false
                    
                    targetPath.text = end_point_addr
                    sourcePath.text = start_point_addr
                    
                }
                

              //정보를 모두 받아 온 후 수락 여부 타이머 동작
              startTimer()
            }else{
                if curReqState == API_DRIVER_DECIDE_REQUEST  { //탁송 요청 거절
                    if mainOperationDelegate != nil {
                        self.dismiss(animated: true, completion: nil)
                        mainOperationDelegate?.moveToPageByPresent(id: "driverMainViewController")
                    }
                    return
                }
            }
            break
            default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        showLoadingProgressBar(isShow: false)
        
        if mainOperationDelegate != nil {
            self.dismiss(animated: true, completion: nil)
            mainOperationDelegate?.moveToPageByPresent(id: "driverMainViewController")
        }
        
        //let json = JSON(data!)
        
        //let dialog = UIAlertController(title: "로그인 오류", message: "오류 번호 : " + json["success"].stringValue, preferredStyle: .alert)
        //let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
        //dialog.addAction(action)
        //self.present(dialog, animated: true, completion: nil)
        //showPopupView("드라이버 호출 오류", "관리자에게 문의해 주세요.")
    }
}
