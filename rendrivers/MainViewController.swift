//
//  MainViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 26/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import NMapsMap
import ChannelIO

@objc protocol MainOperationDelegate{
    func moveToPage(id: String)
    func moveToPage(id: String, imgPath: String)
    func moveToPage(id: String, imgPath: String, thirdInfo: String)
    
    func moveToPageByPresent(id: String)
    func sideMenuShow()
    func setDriverWorkStatus(status: Bool)
    @objc optional func moveToPageByPresent(id: String, title: String, url: String)
    @objc optional func moveToPageByPresent(id: String, reqID: String)
}

class MainViewController:  BaseMapViewController, UITextFieldDelegate, MainOperationDelegate {

    @IBOutlet var upperView: UIView!
    private let locationManager = CLLocationManager()
    private let pathInfoManager : PathInfoManager = PathInfoManager.sharedInstance
    
    @IBOutlet var startWorkingBtn: RoundButton!
    private let startWorkingTime = 9
    private let endWorkingTime = 19
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        upperView.bottomAlphGradient(width: Double(upperView.bounds.width), height: Double(upperView.bounds.height), startPos: 0.0, endPos: 0.75, alph: 0.8)
        
        //메뉴 이동 noti
        NotificationCenter.default.addObserver(self, selector: #selector(self.moveToPageBySideMenu(_:)), name: NSNotification.Name(rawValue: NOTI_MOVE_MENU_CONTROLLER), object: nil)
        
        let bWorkingTime = isWorkingTime()
        if !bWorkingTime{
            startWorkingBtn.isUserInteractionEnabled = false
            startWorkingBtn.setTitle("운영시간: 09시~19시", for: .normal)
        }
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization() //권한 요청
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        request(API_APP_STATUS_REQUEST)
    }
    /*
     * 일반 함수 정의 부부
     */
    @IBAction func menuDidClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
    }

    @IBAction func workInDidClicked(_ sender: Any) {
        request(API_WORK_STATUS)
    }
    
    @objc func moveToPageBySideMenu(_ notification: NSNotification) {
            if let dict = notification.userInfo as NSDictionary? {
                if let id = dict["id"] as? String{
                    
                    if id == "CustomerViewController"{
                        //moveToPageByPresent(id: id)
                        ChannelIO.openChat(animated: true)
                    }else{
                        if id == "driverMainViewController" && InfoManager.getInstance().curDriverWorkStatus { //출근 상태일 경우 퇴근 요청 한다.
                            request(API_WORK_STATUS)
                        }else{
                            if id == "driverMainViewController" {
                                if isWorkingTime(){
                                    moveToPage(id: id)
                                }
                            }else{
                                moveToPage(id: id)
                            }
                        }
                        
                    }
                }
            }
        }
    
    func isWorkingTime() -> Bool {
        let cal = Calendar(identifier: .gregorian)
        let now = Date()
        let coms = cal.dateComponents([.weekday], from: now)
        //일요일 1 - 토요일 7
        if coms.weekday == 1 || coms.weekday == 7 {return false}
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH"
        dateFormatter.locale = NSLocale(localeIdentifier: "ko_KR") as Locale
        let curTime = Int(dateFormatter.string(from: now))
        
        if curTime! < startWorkingTime || endWorkingTime < curTime! { return false }
        return true
    }
    
    //MARK: - network
    override func request(_ name: String) {
        let infoManager = InfoManager.getInstance()
        switch name {
        case API_WORK_STATUS:
            let apiToken : String  = infoManager.getApiToken()
            let driverID : String  = infoManager.getAppID()
            
            let params: Parameters = [
                "api_token" : apiToken
                ,"current_state" : infoManager.curDriverWorkStatus ? "1" : "0"  // 0:출근  1: 퇴근
                ,"driver_id" : driverID
                
            ]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.put, target: self)
            break
        case API_APP_STATUS_REQUEST:
            let extraParam : String = "/" + infoManager.getAppID() + "?api_token="+infoManager.getApiToken() + "&fcm_token="+infoManager.getToken()
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
            
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        let infoManager = InfoManager.getInstance()
        switch requestID {
        case API_WORK_STATUS:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" && !infoManager.curDriverWorkStatus {
                moveToPage(id: "driverMainViewController")
            }else if json["success"].stringValue == "0000" && infoManager.curDriverWorkStatus {
                infoManager.curDriverWorkStatus = false
                self.navigationController?.popViewController(animated: true)
            }
            break
        case API_APP_STATUS_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                if(json["insr_state"].stringValue == "1"){
                    moveToPageByPresent(id: "LoginViewController")
                    return
                }
                
                if json["state"].stringValue == "1" && isWorkingTime() { //출근
                    infoManager.curDriverWorkStatus = false
                    request(API_WORK_STATUS)
                }else if json["state"].stringValue == "2" { //드라이버 접수 중
                    //request(API_WORK_STATUS)
                }else if json["state"].stringValue == "3" { //고객 접수 중
                    //request(API_WORK_STATUS)
                }else if json["state"].stringValue == "4" && isWorkingTime() { //운행 중
                    //request(API_WORK_STATUS)
                    infoManager.curDriverWorkStatus = false
                    moveToPage(id: "RequestStatusViewController")
                }
            }else{
                infoManager.curDriverWorkStatus = false
                moveToPageByPresent(id: "LoginViewController")
            }
            break
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        moveToPageByPresent(id: "LoginViewController")
    }
    
    /// MainOperationDelegate
    func setDriverWorkStatus(status: Bool){
        InfoManager.getInstance().curDriverWorkStatus = status
    }
    
    func sideMenuShow() {
        sideMenuController?.revealMenu()
    }
    
    func moveToPage(id: String) {
        if id == "DriverMainViewController"{
            #if RELEASE
            Mixpanel.mainInstance().track(event: "SET_CHECKOUT_OPTION",
                                          properties:["CHECKOUT_STEP": "1", "CHECKOUT_OPTION": "ADDRESS"])
            #endif
            
            request(API_WORK_STATUS)
            return
        }
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseViewController
        pathVC.mainOperationDelegate = self
        
        self.navigationController?.pushViewController(pathVC, animated: true)
    }
    
    func moveToPage(id: String, imgPath: String){
        
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseViewController
        pathVC.mainOperationDelegate = self
        
        pathVC.pathInfoStr = imgPath
        self.navigationController?.pushViewController(pathVC, animated: true)
    }
    
    func moveToPage(id: String, imgPath: String, thirdInfo: String){
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseViewController
        pathVC.mainOperationDelegate = self
        
        pathVC.pathInfoStr = imgPath
        pathVC.thirdInfo = thirdInfo
        self.navigationController?.pushViewController(pathVC, animated: true)
    }
    
    func moveToPageByPresent(id: String, title: String, url: String) {
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseViewController
        pathVC.titleInfo = title
        pathVC.urlInfo = url
        pathVC.mainOperationDelegate = self
        getTopMostViewController()?.present(pathVC, animated: true, completion: nil)
    }
    
    func moveToPageFromDelegate(_ id: String) {
        moveToPage(id: id)
    }
    
    func moveToPagePresentFromDelegate(_ id: String, reqID: String) {
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseMapViewController
        getTopMostViewController()?.present(pathVC, animated: true, completion: nil)
    }
    
    
    func moveToPageByPresent(id: String) {
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseViewController
        pathVC.mainOperationDelegate = self
        getTopMostViewController()?.present(pathVC, animated: true, completion: nil)
    }
    
    func getTopMostViewController() -> UIViewController? {
        var topMostViewController = UIApplication.shared.keyWindow?.rootViewController
        
        while let presentedViewController = topMostViewController?.presentedViewController {
            topMostViewController = presentedViewController
        }
        
        return topMostViewController
    }
    
    //나의 위치 표시
    func setMyLocationPos(){
        if pathInfoManager.myLocationInfo == nil { return }
        let latitude = pathInfoManager.myLocationInfo!.coordinate.latitude
        let longitude = pathInfoManager.myLocationInfo!.coordinate.longitude
        
        let cameraUpdate = NMFCameraUpdate(scrollTo: NMGLatLng(lat: latitude, lng: longitude))
        mapView!.moveCamera(cameraUpdate)
        
        let locationOverlay = mapView?.locationOverlay
        locationOverlay?.hidden = false
        locationOverlay?.location = NMGLatLng(lat: latitude, lng: longitude)
    }
}


extension MainViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let firstLocation = locations.first else {
            return
        }
                
        if pathInfoManager.myLocationInfo == nil {
            pathInfoManager.myLocationInfo = firstLocation
            println(pathInfoManager.myLocationInfo?.coordinate.latitude)
            println(pathInfoManager.myLocationInfo?.coordinate.longitude)
            setMyLocationPos()
            println("*****************************************************************")
        }
    }
}
