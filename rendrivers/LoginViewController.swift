//
//  LoginViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 26/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var secureTextField: UITextField!
    @IBOutlet var inputViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var loginGuideView: UIView!
    @IBOutlet var loginGuideButton: UIButton!
    
    @IBOutlet var popupView: UIView!
    @IBOutlet var popupTitleLabel: UILabel!
    @IBOutlet var popupContentLabel: UITextView!
    
    //timer
    private var mTimer : Timer?
    private var remainTime = 600
    @IBOutlet var secureTimeLabel: UILabel!
    
    private var isKeyboarShow = false
    override func viewDidLoad() {
        super.viewDidLoad()

        phoneTextField.delegate = self
        phoneTextField.keyboardType = .numberPad
        secureTextField.delegate = self
        secureTextField.keyboardType = .numberPad
        
        // Touch Event 등록 후 함수를 연동한다. (getPosByMap)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dimDidClicked(_:)))
        self.dimView!.addGestureRecognizer(gesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willResignActive), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.inputViewBottomConstraint.constant = 5
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: self.view.window)
    }
    
    //타이머가 호출하는 콜백함수
    @objc func timerCallback(){
        if remainTime > 0 {
            remainTime -= 1
            secureTimeLabel.text = String(format: "%02d:%02d", remainTime/60, remainTime%60)
        }else{
            secureTimeLabel.isHidden = true
            mTimer?.invalidate()
        }
        
    }
    
    //회원 가입 하기 Event
    @IBAction func registDidClicked(_ sender: Any) {
        //이거는 팝업
        showPopupView("회원 가입", "렌톡입니다.\n\n2019/12/9 ~ 2019/12/31 간 회원가입이 잠시 불가합니다.\n2020/1/1 부터 다시 회원가입 진행 예정 입니다.\n많은 관심 부탁드립니다.\n감사합니다.")
        
        //이거는 가입 페이지 이동
        //moveToPageByPresent(id: "RegisterViewController")
    }
    
    func moveToPageByPresent(id: String) {
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseViewController
        self.present(pathVC, animated: true, completion: nil)
    }
    
    //DimView Clicked
    @objc func dimDidClicked(_ sender: UIGestureRecognizer){
        hidePopupView()
    }
    
    @IBAction func sendCallDidClicked(_ sender: Any) {
        sendCall(CS_NUMBER)
    }
    
    @IBAction func telCertiDidClicked(_ sender: Any) {
        let phone : String = phoneTextField.trimmingCharacters()
        if phone.count == 0 {
            showPopupView("로그인 오류", "핸드폰 번호를 입력해주세요.")
            return
        }
        
        remainTime = 180
        secureTimeLabel.isHidden = false
        if let timer = mTimer {
            //timer 객체가 nil 이 아닌경우에는 invalid 상태에만 시작한다
            if !timer.isValid {
                /** 1초마다 timerCallback함수를 호출하는 타이머 */
                mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
            }
        }else{
            //timer 객체가 nil 인 경우에 객체를 생성하고 타이머를 시작한다
            /** 1초마다 timerCallback함수를 호출하는 타이머 */
            mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
        }
        
        request(API_USER_LOGIN_REQ_NUM)
    }
    
    @IBAction func loginDidClicked(_ sender: Any) {
        secureTimeLabel.isHidden = true
        mTimer?.invalidate()
        
        request(API_USER_LOGIN)
    }
    
    @objc func willResignActive(_ notification: Notification) {
        print("Background");
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification:NSNotification) {
        print("Show");
        
        if isKeyboarShow {return}
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        print(keyboardHeight)
        
        adjustingHeight(show: true, notification: notification)
        isKeyboarShow = true
    }
    
    @IBAction func popupConfirmDidClicked(_ sender: Any) {
        hidePopupView()
    }
    
    @IBAction func loginGuideDidClicked(_ sender: Any) {
        loginGuideButton.isSelected = !loginGuideButton.isSelected
        showGuideView(loginGuideButton.isSelected)
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        print("Hide")
        if !isKeyboarShow {return}
        adjustingHeight(show: false, notification: notification)
        isKeyboarShow = false
    }
    
    private func showPopupView(_ title: String, _ content: String ){
        self.view.endEditing(true)
        
        popupTitleLabel.text = title
        popupContentLabel.text = content
        
        self.view.bringSubviewToFront(popupView)
        
        popupView.isHidden = false
        dimView!.isHidden = false
    }
    
    private func hidePopupView(){
        popupView.isHidden = true
        dimView!.isHidden = true
    }
    
    private func showGuideView(_ isShow: Bool){
        if isShow {
            loginGuideButton.setImage(UIImage(named: "ico_phone_guide_p"), for: .normal)
            loginGuideView.isHidden = false
            loginGuideButton.isSelected = true
        }else{
            loginGuideButton.isSelected = false
            loginGuideButton.setImage(UIImage(named: "ico_phone_guid"), for: .normal)
            loginGuideView.isHidden = true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func adjustingHeight(show:Bool, notification:NSNotification) {
        
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let animationDurarion = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        //let changeInHeight = (keyboardFrame.height) * (show ? 1 : -1)
        let changeInHeight = CGFloat(keyboardRectangle.height / 2) * (show ? 1 : -1)
        UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
            self.inputViewBottomConstraint.constant += changeInHeight
        })
        
    }
    
    //MARK: - network
    override func request(_ name: String) {
        switch name {
        case API_USER_LOGIN:
            let phone : String = phoneTextField.trimmingCharacters()
            let secure : String = secureTextField.trimmingCharacters()
            let token : String  = InfoManager.getInstance().getToken()
            
            let params: Parameters = [
                "driver_id" : phone
                ,"auth_no" : secure
                ,"fcm_token" : token
                ,"os_gb" : "1"
            ]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.put, target: self)
            break
        case API_USER_LOGIN_REQ_NUM:
            let phone : String = phoneTextField.trimmingCharacters()
            let params: Parameters = ["tel_no" : phone]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.post, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        switch requestID {
        case API_USER_LOGIN:
            let phone : String = phoneTextField.trimmingCharacters()
            
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                InfoManager.getInstance().setApiToken(token: json["api_token"].stringValue)
                InfoManager.getInstance().setAppID(id: phone)
                
                println(json["apiKeys"]["aws"]["secret"].stringValue)
                println(json["driver_id"].stringValue)
                
                self.performSegue(withIdentifier: "CustomToLeftSegue", sender: self)
            }
            break
        case API_USER_LOGIN_REQ_NUM:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                showPopupView("로그인", "인증번호를 카카오톡에서 확인해 주세요.")
            }
            break;
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        let json = JSON(data!)
//
//        let dialog = UIAlertController(title: "로그인 오류", message: "오류 번호 : " + json["success"].stringValue, preferredStyle: .alert)
//        let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
//        dialog.addAction(action)
//        self.present(dialog, animated: true, completion: nil)
        
        secureTimeLabel.isHidden = true
        mTimer?.invalidate()
        
        showPopupView("로그인 오류", json["tel_no"][0].stringValue)
    }
}
