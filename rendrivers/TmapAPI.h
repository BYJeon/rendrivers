//
//  TmapAPI.h
//  rentalk
//
//  Created by Yuna Daddy on 09/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMapView.h"

NS_ASSUME_NONNULL_BEGIN
#define TMAP_APPKEY @"36ed77a4-99ac-4b6d-95e7-c55123f54ae0"

TMapView*       _mapView;

@interface TmapAPI : NSObject

-(void)createMapView;
-(NSArray*)getROIList:(NSString*) searchWord;

@end

NS_ASSUME_NONNULL_END
