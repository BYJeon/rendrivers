//
//  WebViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 01/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

class WebViewController: BaseViewController {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleInfo
        
        let url = NSURL (string: urlInfo)
        let request = NSURLRequest(url: url! as URL)
        webView.loadRequest(request as URLRequest)
        
    }

    @IBAction func closeDidClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
