//
//  BaseTableViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 20/09/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController, NetManagerDelegate {
    internal var parentDelegate : ParentViewDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override open var shouldAutorotate: Bool{
        return false
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .portrait
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
    
    //MARK: - NetManager delegate
    func request(_ name: String) {
        println(">>> requestID = \(name)")
    }
    
    func requestFinished(_ requestID: String, _ data: Any!) {
    }
    
    func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
    }
    
    func requestFailed(_ requestID: String) {
        println(">>> requestID = \(requestID)")
    }
}

