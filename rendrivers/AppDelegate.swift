//
//  AppDelegate.swift
//  rendrivers
//
//  Created by Yuna Daddy on 17/05/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import FirebaseMessaging
import SideMenuSwift
import SwiftyJSON
import ChannelIO

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var mainViewController: MainViewController?
    
    //fcm
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        /**************************** ChannelIO *****************************/
        ChannelIO.initialize(application)
        
        //Channel Talk Settings
        let settings = ChannelPluginSettings()
        settings.pluginKey = "c36cbe77-56be-4e81-bfb5-a23aeef889ea"
        ChannelIO.boot(with: settings)
        
        /**************************** ChannelIO  *****************************/
        
        /**************************** Firebase *****************************/
        FirebaseApp.configure()
        // Messaging
        // Override point for customization after application launch.
        /**************************** Push service start *****************************/
        
        Messaging.messaging().delegate = self
        
        // iOS 10 support
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // 이미 등록된 토큰이 있는지 확인 없을 경우 nil 호출됨
        let token = Messaging.messaging().fcmToken
        if token != nil && token!.count > 0{
            InfoManager.getInstance().setToken(token: token!)
            print("FCM token: \(token ?? "")")
        }
        /**************************** Push service end *****************************/
        
        
        //사이드 메뉴 init
        var arguments = ProcessInfo.processInfo.arguments
        arguments.removeFirst()
        setupTestingEnvironment(with: arguments)
        
        configureSideMenu()
        
        let infoManager = InfoManager.getInstance()
        
        //API 토큰이 있을 경우 바로 로그인 한다.
        if infoManager.getApiToken() != ""{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let rootController = storyboard.instantiateViewController(withIdentifier: "SideMenuController")
            self.window?.rootViewController = rootController
        }
        
        return true
    }
    
    //APNS Get Token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        
        Messaging.messaging().apnsToken = deviceToken
        
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                if result.token != nil && result.token.count > 0{
                    InfoManager.getInstance().setToken(token: result.token)
                    print("Remote instance ID token: \(result.token)")
                }
            }
        })
        
        print("Token : " + token)
    }
    
    // MARK: - fcm
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            println("Message ID: \(messageID)")
        }
        
        // Print full message.
        println(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            println("Message ID: \(messageID)")
        }
        
        // Print full message.
        println(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    
    //APNS 등록 실패
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNS registration failed: \(error)");
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    private func configureSideMenu() {
        SideMenuController.preferences.basic.menuWidth = 240
        SideMenuController.preferences.basic.defaultCacheKey = "0"
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    // Receive displayed notifications for iOS 10 devices.
    // 앱 실행 중 노티가 왔을 경우
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            println("Message ID: \(messageID)")
        }
        
        // Print full message.
        println(userInfo)
        println(notification)
        
        // Change this to your preferred presentation option
        println("UNUserNotificationCenterDelegate willPresent notification = \(notification)")
        completionHandler([.alert, .sound, .badge])
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        //수신메세지
        receivePushMessgae(userInfo)
        
        /*
         {
         "title": "",
         "message": "",
         "sc_id": "99e9a9322b5a427984d7bc12f8d5798c",
         "push_type": "share_channel_join"
         }
         
         let apsJson = JSON(userInfo)
         let title: String = apsJson["title"].stringValue
         let message: String = apsJson["message"].stringValue
         let sc_id: String = apsJson["sc_id"].stringValue
         let push_type: String = apsJson["sc_id"].stringValue
         println(apsJson)
         */
    }
    
    //Push 메세지 클릭 시 불리는 함수
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            println("Message ID: \(messageID)")
        }
        
        // Print full message.
        println(userInfo)
        
        println("UNUserNotificationCenterDelegate didReceive response = \(response)")
        completionHandler()
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        //수신메세지
        receivePushMessgae(userInfo)
        
        /*
         let apsJson = JSON(userInfo)
         let title: String = apsJson["title"].stringValue
         let message: String = apsJson["message"].stringValue
         let sc_id: String = apsJson["sc_id"].stringValue
         let push_type: String = apsJson["sc_id"].stringValue
         println(apsJson)
         */
    }
    
    func receivePushMessgae(_ userInfo: [AnyHashable : Any]) {
        let apsJson = JSON(userInfo)
        let category_type: String = apsJson["category_type"].stringValue
        
        let infoManager = InfoManager.getInstance()
        
        if category_type == "D01" {//탁송 요청 알림
            infoManager.setReqID(reqID:apsJson["req_id"].stringValue) 
            print(apsJson["req_id"].stringValue)
            //드라이버 Pick 상태 갱신 noti
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_DRIVER_REQUEST), object: nil)
        }else if category_type == "D02" { //탁송 시작 알림
            infoManager.setMenuType(type: MenuType.DRIVER_START)
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_DRIVER_REQUEST_START), object: nil)
        }else if category_type == "D03" || category_type == "D04" || category_type == "D05" { //탁송 취소 알림
            infoManager.setMenuType(type: MenuType.DRIVER_CANCEL)
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_DRIVER_REQUEST_CANCEL), object: nil)
        }
    }
}
extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
    
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        println("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

extension AppDelegate {
    private func setupTestingEnvironment(with arguments: [String]) {
        SideMenuController.preferences.basic.direction = .right
    }
}
#if DEBUG
#endif
