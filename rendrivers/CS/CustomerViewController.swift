//
//  CustomerViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 01/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CustomerViewController: BaseViewController, UITextViewDelegate {

    @IBOutlet var contentTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentTextView.delegate = self
        
        contentTextView.text = "고객센터 문의 내용 입력"
        contentTextView.textColor = UIColor.lightGray
    }

    
    @IBAction func sendQuestionDidClicked(_ sender: Any) {
        request(API_SLACK_CUSTOMER_REQUEST)
    }
    
    @IBAction func closeDidClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func callCSBtnClicked(_ sender: Any) {
        sendCall(CS_NUMBER)
    }
    
    
    //MARK: - network
    override func request(_ name: String) {
        switch name {
        case API_SLACK_CUSTOMER_REQUEST: // Slack 고객 문의 사항
            let infoManager = InfoManager.getInstance()
            
            let params: Parameters = [
                "username" : "렌톡(iOS) : " + infoManager.getAppID() + "님"
                ,"text" : contentTextView.text
            ]
            
            _ = NetworkManager.sharedInstance.requestSlack(name, params, HTTPMethod.post, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        switch requestID {
        case API_SLACK_CUSTOMER_REQUEST:
            dismiss(animated: true, completion: nil)
        default:
            break
        }
        
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        
        let dialog = UIAlertController(title: "오류", message: "관리자에게 문의해 주세요.", preferredStyle: .alert)
        let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
        dialog.addAction(action)
        self.present(dialog, animated: true, completion: nil)
        //showPopupView("드라이버 호출 오류", "관리자에게 문의해 주세요.")
    }
    
    /* Updated for Swift 4 */
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textViewSetupView()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if contentTextView.text == ""{
            textViewSetupView()
        }
    }
    
    func textViewSetupView(){
        if contentTextView.text == "고객센터 문의 내용 입력"{
            contentTextView.text = ""
            contentTextView.textColor = UIColor.black
        }else if contentTextView.text == "" {
            contentTextView.text = "고객센터 문의 내용 입력"
            contentTextView.textColor = UIColor.lightGray
        }
    }
}

