//
//  RequestStatusDetailTableViewController.swift
//  rendrivers
//
//  Created by Yuna on 05/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AWSS3

class RequestStatusDetailTableViewController: BaseTableViewController {
    @IBOutlet var telNoLabel: UILabel!
    @IBOutlet var cpNmLabel: UILabel!
    
    @IBOutlet var pathInfoView: UIView!
    @IBOutlet var pathViaView: UIView!
    
    //경유지가 없는 경로
    @IBOutlet var targetPath: UITextView!
    @IBOutlet var sourcePath: UITextView!
    @IBOutlet var missionCompleteTimeLabel: UILabel!
    @IBOutlet var missionView: UIView!
   
    @IBOutlet var sourceTimeLabel: UILabel!
    @IBOutlet var targetTimeLabel: UILabel!
    
    //경유지가 있는 경로
    @IBOutlet var targetViaPath: UITextView!
    @IBOutlet var viaPath: UITextView!
    @IBOutlet var sourceViaPath: UITextView!
    @IBOutlet var missionViaCompleteTimeLabel: UILabel!
    @IBOutlet var missionViaView: UIView!
    
    @IBOutlet var sourceViaTimeLabel: UILabel!
    @IBOutlet var viaTimeLabel: UILabel!
    @IBOutlet var targetViaTimeLabel: UILabel!
    
    @IBOutlet var serviceFeeLabel: UILabel!
    @IBOutlet var totalFeeLabel: UILabel!
    
    
    @IBOutlet var pathImgView: UIImageView!
    
    open var redID = ""
    open var pathInfoStr = ""
    open var cpTelNo = ""
    
    //AWS S3
    private var downloadProgress : Float = 0
    @objc var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
    
    @objc lazy var transferUtility = {
        AWSS3TransferUtility.default()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if parentDelegate != nil {
            parentDelegate?.showLoadingProgressBarFromDelegate?(true)
        }
        request(API_DRIVER_MISSION_END_REQUEST)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 6
    }
    
    @IBAction func closeDidClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func confirmDidClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "driverMainViewController") as! BaseViewController
        self.navigationController?.pushViewController(pathVC, animated: true)
    }
    
    func downloadDriverPathInfo(_ imagePath : String){
        if imagePath.count == 0 {return}
        let expression = AWSS3TransferUtilityDownloadExpression()
        expression.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                if (self.downloadProgress < Float(progress.fractionCompleted)) {
                    self.downloadProgress = Float(progress.fractionCompleted)
                }
            })
        }
        
        self.completionHandler = { (task, location, data, error) -> Void in
            DispatchQueue.main.async(execute: {
                if let error = error {
                    NSLog("Failed with error: \(error)")
                    //self.statusLabel.text = "Failed"
                }
                else if(self.downloadProgress != 1.0) {
                    //self.statusLabel.text = "Failed"
                }
                else{
                    //self.infoManager.driverInfo!.dirver_image = UIImage(data: data!)
                    self.pathImgView.image = UIImage(data: data!)
                    //print("************************\(reqIndex)*********************")
                }
            })
        }
        
        transferUtility.downloadData(
            forKey: imagePath,
            expression: expression,
            completionHandler: completionHandler).continueWith { (task) -> AnyObject? in
                if let error = task.error {
                    NSLog("Error: %@",error.localizedDescription);
                    DispatchQueue.main.async(execute: {
                        //self.statusLabel.text = "Failed"
                    })
                }
                
                if let _ = task.result {
                    DispatchQueue.main.async(execute: {
                        //self.statusLabel.text = "Downloading..."
                    })
                    NSLog("Download Starting!")
                    // Do something with uploadTask.
                }
                return nil;
        }
    }
    
    //MARK: - network
    override func request(_ name: String) {
        let infoManager = InfoManager.getInstance()
        switch name {
            
        case API_DRIVER_MISSION_END_REQUEST://탁송 드라이버 요청 건 검색
            //let extraParam : String = "/" + infoManager.getReqID() + "?api_token="+infoManager.getApiToken()
            let extraParam : String = "/" + infoManager.requestInfo!.req_id + "?api_token="+infoManager.getApiToken()
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        if parentDelegate != nil {
            parentDelegate?.showLoadingProgressBarFromDelegate?(false)
        }
        
        switch requestID {
        case API_DRIVER_MISSION_END_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                telNoLabel.text = cpTelNo
                let start_tel_no  =  json["start_point_info"]["tel_no"].stringValue
                let start_address =  json["start_point_info"]["address"].stringValue
                let start_arrived_time =  json["start_point_info"]["arrive_time"].stringValue
                
                let waypoint_tel_no  =  json["waypoint_info"]["tel_no"].stringValue
                let waypoint_address =  json["waypoint_info"]["address"].stringValue
                let waypoint_arrived_time =  json["waypoint_info"]["arrive_time"].stringValue
                
                let end_tel_no  =  json["end_point_info"]["tel_no"].stringValue
                let end_address =  json["end_point_info"]["address"].stringValue
                let end_arrived_time =  json["end_point_info"]["arrive_time"].stringValue
                
                let mission_nm = json["mission_info"][0]["mission_nm"].stringValue
                let mission_complete_time = json["mission_info"][0]["completed_time"].stringValue
                
                let service_info = json["fee_info"]["service_info"].stringValue
                let total_dc = json["fee_info"]["total_dc"].stringValue
                let charge_fee = json["fee_info"]["charge_fee"].stringValue
                
                let emp_nm = json["emp_nm"].stringValue
                let emp_company = json["emp_company"].stringValue
                let emp_id = json["emp_id"].stringValue
                
                //경유지가 있을 경우
                if waypoint_address.count != 0 && waypoint_arrived_time.count != 0 {
                    pathInfoView.isHidden = true
                    pathViaView.isHidden = false
                    
                    sourceViaPath.text = start_address
                    sourceViaTimeLabel.text = start_arrived_time
                    
                    viaPath.text = waypoint_address
                    viaTimeLabel.text = waypoint_arrived_time
                    
                    targetViaPath.text = end_address
                    targetViaTimeLabel.text = end_arrived_time
                    
                    if mission_nm != nil && mission_nm.count != 0 {
                        missionViaView.isHidden = false
                        missionViaCompleteTimeLabel.text = mission_complete_time
                    }else{
                        missionViaView.isHidden = true
                    }
                    
                }else{
                    pathInfoView.isHidden = false
                    pathViaView.isHidden = true
                    
                    sourcePath.text = start_address
                    sourceTimeLabel.text = start_arrived_time
                    
                    targetPath.text = end_address
                    targetTimeLabel.text = end_arrived_time
                    
                    if mission_nm != nil && mission_nm.count != 0 {
                        missionView.isHidden = false
                        missionCompleteTimeLabel.text = mission_complete_time
                    }else{
                        missionView.isHidden = true
                    }
                }
                
                serviceFeeLabel.text = charge_fee.getCurrencyString() + "원"
                totalFeeLabel.text = charge_fee.getCurrencyString() + "원"
            }
            
            if pathInfoStr.count > 1 {
                let pre = pathInfoStr.index(pathInfoStr.startIndex, offsetBy: 0)
                let prefix = String(pathInfoStr[pre])
                
                if prefix == "r"{
                    self.downloadDriverPathInfo(pathInfoStr)
                }else{
                    let startIndex = pathInfoStr.index(pathInfoStr.startIndex, offsetBy: 1)
                    self.downloadDriverPathInfo(String(pathInfoStr[startIndex...]))
                }
            }
            break
            
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        if parentDelegate != nil {
            parentDelegate?.showLoadingProgressBarFromDelegate?(false)
        }
        println(">>> requestID = \(requestID)")
        
    }
}
