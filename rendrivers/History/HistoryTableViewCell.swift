//
//  HistoryTableViewCell.swift
//  rendrivers
//
//  Created by Yuna on 05/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {
    
    @IBOutlet var dataLabel: UILabel!
    @IBOutlet var carNumLabel: UILabel!
    @IBOutlet var payInfoLabel: UILabel!
    @IBOutlet var curStatusBtn: RoundButton!
    @IBOutlet var completeStatusBtn: RoundButton!
    @IBOutlet var telNoLabel: UILabel!
    @IBOutlet var cpNameLabel: UILabel!
    
    @IBOutlet var pathImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
