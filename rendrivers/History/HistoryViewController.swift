//
//  HistoryViewController.swift
//  rendrivers
//
//  Created by Yuna on 05/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

class HistoryViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet var numTextField: UITextField!
    private var childTableView: HistoryTableViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        numTextField.keyboardType = .numbersAndPunctuation
        
        numTextField.delegate = self
        childTableView = self.children[0] as? HistoryTableViewController
        childTableView.parentDelegate = self
    }
    
    @IBAction func backDidClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func allViewDidClicked(_ sender: Any) {
        numTextField.text = ""
        childTableView.reloadReuqestInfo("")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        childTableView.reloadReuqestInfo(numTextField.text!)
        return true
    }
    
    ///Parent Delegate

    override func showLoadingProgressBarFromDelegate(_ show: Bool) {
        showLoadingProgressBar(isShow: show)
    }
    
    override func moveToPageFromDelegate(_ id: String, _ subInfo: String, _ thirdInfo: String){
        if mainOperationDelegate != nil {
            mainOperationDelegate?.moveToPage(id: id, imgPath: subInfo, thirdInfo: thirdInfo)
        }
    }
}
