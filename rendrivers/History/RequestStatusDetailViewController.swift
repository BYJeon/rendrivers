//
//  RequestStatusDetailViewController.swift
//  rendrivers
//
//  Created by Yuna on 05/10/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

class RequestStatusDetailViewController: BaseViewController {
    private var childTableView: RequestStatusDetailTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        childTableView = self.children[0] as? RequestStatusDetailTableViewController
        childTableView.cpTelNo = thirdInfo
        childTableView.parentDelegate = self
        childTableView.pathInfoStr = pathInfoStr
    }
    
    
    @IBAction func backDidClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
