//
//  DriverMainViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 26/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AWSS3

class DriverMainViewController: BaseViewController {
    private let infoManager = InfoManager.getInstance()
    private let locationManager = CLLocationManager()
    private let pathInfoManager : PathInfoManager = PathInfoManager.sharedInstance
    
    @IBOutlet var driverImg: UIImageView!
    @IBOutlet var driverNameLabel: UILabel!
    @IBOutlet var driverScoreLabel: UILabel!
    @IBOutlet var driverMissionLabel: UILabel!
    @IBOutlet var driverRewardLabel: UILabel!
    
    private var downloadProgress : Float = 0
    private var driverWorkStatus = "0"
    
    @objc var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
    
    //timer
    private var mTimer : Timer?
    
    @objc lazy var transferUtility = {
           AWSS3TransferUtility.default()
       }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        InfoManager.getInstance().curDriverWorkStatus = true
        //처음은 무조건 출근으로 서버에 다시 요청 한다.
        request(API_WORK_STATUS)
        //탁송 요청
        NotificationCenter.default.addObserver(self, selector: #selector(driverRequested), name: NSNotification.Name(rawValue: NOTI_DRIVER_REQUEST), object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization() //권한 요청
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        request(API_DRIVER_WAIT_INFO_REQUEST)
        
        
        if let timer = mTimer {
            //timer 객체가 nil 이 아닌경우에는 invalid 상태에만 시작한다
            if !timer.isValid {
                /** 1초마다 timerCallback함수를 호출하는 타이머 */
                mTimer = Timer.scheduledTimer(timeInterval: 600, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
            }
        }else{
            //timer 객체가 nil 인 경우에 객체를 생성하고 타이머를 시작한다
            /** 1초마다 timerCallback함수를 호출하는 타이머 */
            mTimer = Timer.scheduledTimer(timeInterval: 600, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
        }
    }
    //탁송 요청 Noti
    @objc func driverRequested() {
        print(infoManager.getMenuType())
        if(mainOperationDelegate != nil && infoManager.getMenuType() != MenuType.DRIVER_REQUEST){
            infoManager.setMenuType(type: MenuType.DRIVER_REQUEST)
            mainOperationDelegate?.moveToPageByPresent(id: "DriverRequestViewController")
        }
    }
    
    @IBAction func workOutDidClicked(_ sender: Any) {
        //퇴근으로 플래그를 바꾸자
        driverWorkStatus = "1"
        request(API_WORK_STATUS)
    }
    
    //타이머가 호출하는 콜백함수
    @objc func timerCallback(){
        request(API_DRIVER_LOCATION_UPLOAD)
    }
    
    private func downloadDriverProfile(_ imgPath: String){
        if imgPath.count < 2 { return }
        
        let expression = AWSS3TransferUtilityDownloadExpression()
        expression.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                if (self.downloadProgress < Float(progress.fractionCompleted)) {
                    self.downloadProgress = Float(progress.fractionCompleted)
                }
            })
        }
        
        self.completionHandler = { (task, location, data, error) -> Void in
            DispatchQueue.main.async(execute: {
                if let error = error {
                    NSLog("Failed with error: \(error)")
                    //self.statusLabel.text = "Failed"
                }
                else if(self.downloadProgress != 1.0) {
                    //self.statusLabel.text = "Failed"
                }
                else{
                    self.driverImg.image = UIImage(data: data!)
                }
            })
        }
        
        transferUtility.downloadData(
            forKey: imgPath,
            expression: expression,
            completionHandler: completionHandler).continueWith { (task) -> AnyObject? in
                if let error = task.error {
                    NSLog("Error: %@",error.localizedDescription);
                    DispatchQueue.main.async(execute: {
                        //self.statusLabel.text = "Failed"
                    })
                }
                
                if let _ = task.result {
                    DispatchQueue.main.async(execute: {
                        //self.statusLabel.text = "Downloading..."
                    })
                    NSLog("Download Starting!")
                    // Do something with uploadTask.
                }
                return nil;
        }
    }
    
    
    @IBAction func menuDidClicked(_ sender: Any) {
        if mainOperationDelegate != nil {
            mainOperationDelegate?.sideMenuShow()
        }
    }
    
    //MARK: - network
    override func request(_ name: String) {
        let infoManager = InfoManager.getInstance()
        
        showLoadingProgressBar(isShow: true)
        switch name {
        case API_APP_STATUS_REQUEST:
            let extraParam : String = "/" + infoManager.getAppID() + "?api_token="+infoManager.getApiToken() + "&fcm_token="+infoManager.getToken()
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
            
        case API_WORK_STATUS:
            let apiToken : String  = infoManager.getApiToken()
            let driverID : String  = infoManager.getAppID()
            
            let params: Parameters = [
                "api_token" : apiToken
                ,"current_state" : driverWorkStatus
                ,"driver_id" : driverID
                
            ]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.put, target: self)
            break
        case API_DRIVER_WAIT_INFO_REQUEST:
            let extraParam : String = "/" + infoManager.getAppID() + "?api_token="+infoManager.getApiToken()
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
            
        case API_DRIVER_LOCATION_UPLOAD:
            if pathInfoManager.myLocationInfo == nil {
                showLoadingProgressBar(isShow: false)
                return
            }
            let la : Double = (pathInfoManager.myLocationInfo?.coordinate.latitude)!
            let lo : Double = (pathInfoManager.myLocationInfo?.coordinate.longitude)!
            let driver_id : String = infoManager.appID!
            let token : String  = infoManager.getApiToken()
            
            let params: Parameters = [
                "la" : String(format: "%f", la)
                ,"lo" : String(format: "%f", lo)
                ,"driver_id" : driver_id
                ,"api_token" : token
            ]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.put, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        showLoadingProgressBar(isShow: false)
        
        switch requestID {
        case API_APP_STATUS_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                
                if json["state"].stringValue == "1" { //출근
                    if mainOperationDelegate != nil {
                        //mainOperationDelegate?.moveToPage(id: "RequestStatusViewController")
                    }
                }else if json["state"].stringValue == "2" { //드라이버 접수 중
                }else if json["state"].stringValue == "3" { //고객 접수 중
                }else if json["state"].stringValue == "4" { //운행 중
                    if mainOperationDelegate != nil {
                        //mainOperationDelegate?.moveToPage(id: "RequestStatusViewController")
                    }
                }
            }
            break
            
        case API_WORK_STATUS:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                if driverWorkStatus == "1" {
                    //self.dismiss(animated: true, completion: nil)
                    if mainOperationDelegate != nil {
                        mainOperationDelegate?.setDriverWorkStatus(status: false)
                    }
                    self.navigationController?.popViewController(animated: true)
                }
            }
            break
            
        case API_DRIVER_WAIT_INFO_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                let driverName    = json["driver_nm"].stringValue
                let driverScore   = json["eval_score"].stringValue
                let driverNum     = json["completed_request_num"].stringValue
                var driverReward  = json["reward"].stringValue
                let driverPhoto   = json["profile_photo"].stringValue
                
                downloadDriverProfile(driverPhoto)
                driverNameLabel.isHidden = false
                driverScoreLabel.isHidden = false
                driverMissionLabel.isHidden = false
                driverRewardLabel.isHidden = false
                
                driverReward = driverReward.count == 0 ? "0" : driverReward
                
                driverNameLabel.text = driverName + " 드라이버"
                driverScoreLabel.text = driverScore.count == 0 ? "0.0" : driverScore
                driverMissionLabel.text = driverNum
                driverRewardLabel.text  = driverReward + " 원"
                
                request(API_DRIVER_LOCATION_UPLOAD)
            }
            break
        case API_DRIVER_LOCATION_UPLOAD:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
            }
            break

        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        showLoadingProgressBar(isShow: false)
        
        println(">>> requestID = \(requestID)")
        
        let json = JSON(data!)
        
        let dialog = UIAlertController(title: "통신 오류", message: "오류 번호 : " + json["error"].stringValue, preferredStyle: .alert)
        let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
        dialog.addAction(action)
        self.present(dialog, animated: true, completion: nil)
    }
}

extension DriverMainViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let firstLocation = locations.first else {
            return
        }
                
        if pathInfoManager.myLocationInfo == nil {
            pathInfoManager.myLocationInfo = firstLocation
            println(pathInfoManager.myLocationInfo?.coordinate.latitude)
            println(pathInfoManager.myLocationInfo?.coordinate.longitude)
            println("*****************************************************************")
        }
    }
}
