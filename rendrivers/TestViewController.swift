//
//  TestViewController.swift
//  rendrivers
//
//  Created by Yuna Daddy on 26/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

class TestViewController: BaseMapViewController {

    @IBOutlet var upperView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        upperView.bottomAlphGradient(width: Double(self.view.bounds.width), height: Double(upperView.bounds.height), startPos: 0.0, endPos: 0.75, alph: 0.8)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
